<?php die(); ?><?xml version="1.0" encoding="UTF-8"?><rss version="2.0"
	xmlns:content="http://purl.org/rss/1.0/modules/content/"
	xmlns:wfw="http://wellformedweb.org/CommentAPI/"
	xmlns:dc="http://purl.org/dc/elements/1.1/"
	xmlns:atom="http://www.w3.org/2005/Atom"
	xmlns:sy="http://purl.org/rss/1.0/modules/syndication/"
	xmlns:slash="http://purl.org/rss/1.0/modules/slash/"
	>

<channel>
	<title>Hospital de la Concepción</title>
	<atom:link href="http://hospitaldelaconcepcion.com/feed/" rel="self" type="application/rss+xml" />
	<link>http://hospitaldelaconcepcion.com</link>
	<description></description>
	<lastBuildDate>Fri, 03 Nov 2017 18:46:47 +0000</lastBuildDate>
	<language>en-US</language>
	<sy:updatePeriod>hourly</sy:updatePeriod>
	<sy:updateFrequency>1</sy:updateFrequency>
	<generator>https://wordpress.org/?v=4.6.8</generator>
	<item>
		<title>EMPLEADOS DE INSTITUCIÓN DE SALUD DEL SUROESTE SE BENEFICIAN  DE SERVICIO DE LAVANDERÍA GRATIS</title>
		<link>http://hospitaldelaconcepcion.com/empleados-de-institucion-de-salud-del-suroeste-se-benefician-de-servicio-de-lavanderia-gratis/</link>
		<comments>http://hospitaldelaconcepcion.com/empleados-de-institucion-de-salud-del-suroeste-se-benefician-de-servicio-de-lavanderia-gratis/#respond</comments>
		<pubDate>Fri, 03 Nov 2017 18:46:47 +0000</pubDate>
		<dc:creator><![CDATA[Alvin Cordero]]></dc:creator>
				<category><![CDATA[Noticias]]></category>

		<guid isPermaLink="false">http://hospitaldelaconcepcion.com/?p=4247</guid>
		<description><![CDATA[San Germán… 3 de noviembre de 2017… La administración del Hospital de la Concepción ubicada en San Germán, establece lavandería gratis en la institución como un esfuerzo más en apoyo a sus empleados. Luego del paso del huracán María, la institución hospitalaria estableció una serie de medidas enfocadas en proveer alivio a sus empleados. Entre [...]]]></description>
				<content:encoded><![CDATA[<p style="text-align: center;">
<p style="text-align: center;">
<p style="text-align: center;">
<p style="text-align: center;">
<p style="text-align: center;">
<p>San Germán… 3 de noviembre de 2017… La administración del Hospital de la Concepción ubicada en San Germán, establece lavandería gratis en la institución como un esfuerzo más en apoyo a sus empleados.</p>
<p>Luego del paso del huracán María, la institución hospitalaria estableció una serie de medidas enfocadas en proveer alivio a sus empleados. Entre ellas, se encontró el adelanto de pago de nómina, acceso a alimentos e incentivos económicos para aquellos empleados que sufrieron daños en sus residencias. No obstante, al día de hoy muchos de ellos no cuentan con los servicios de agua y energía eléctrica. Por tal motivo, el Hospital de la Concepción dio un paso más en sus esfuerzos con la iniciativa de una lavandería gratis en las inmediaciones de la institución.</p>
<p>“Sabemos que la situación que enfrentamos como país ha sido y es difícil para todos como sociedad. Es por esto que tenemos que ser proactivos en los diferentes esfuerzos que realizamos tanto para la comunidad como para nuestros compañeros de trabajo quienes día a día dan lo mejor de sí en pro del bienestar de los pacientes. Aun cuando en el suroeste de Puerto Rico hay sectores que gozan del servicio de energía eléctrica, son muchos los que aún no cuentan con el mismo. Ante la falta de este servicio tan fundamental nos dimos a la tarea de adquirir cinco lavadoras y cinco secadoras para así ofrecer una lavandería gratis a todos nuestros empleados,” expresó el director ejecutivo de la Junta de Síndicos del Hospital de la Concepción, Gustavo Almodóvar, quien a su vez indicó que la institución próximamente estará estableciendo un oasis de agua potable como un beneficio más para sus empleados.</p>
<p>Por su parte, Iván Pérez, técnico de Servicios Ambientales de la institución y quien se convirtió en el primer empleado en hacer uso de los servicios de lavandería expresó que “esta iniciativa es una muy buena, estamos más que agradecidos por la ayuda que nos dan. El hospital nos sorprendió con este servicio y más ahora que nos encontramos ante la necesidad.”</p>
<p>Durante el paso del huracán María, el Hospital de la Concepción, ubicado en San Germán se convirtió en uno de las instituciones de salud principales para las personas adquirir sus servicios de salud. El alto compromiso de esta institución centenaria con la comunidad y con Puerto Rico quedó evidenciado una vez más mediante su innovación clínica, el ofrecimiento de servicios de forma ininterrumpida y los esfuerzos realizados por empleados, facultativos y el grupo de voluntarios que trabajaron durante la emergencia.</p>
<p>&nbsp;</p>
<p style="text-align: center;">###</p>
<p>Contacto para los medios de comunicación:</p>
<p>Neyssa García Toucet</p>
<p>787-543-6708</p>
<p>ngtconsultant@gmail.com</p>
<p><a class="a2a_button_facebook" href="http://www.addtoany.com/add_to/facebook?linkurl=http%3A%2F%2Fhospitaldelaconcepcion.com%2Fempleados-de-institucion-de-salud-del-suroeste-se-benefician-de-servicio-de-lavanderia-gratis%2F&amp;linkname=EMPLEADOS%20DE%20INSTITUCI%C3%93N%20DE%20SALUD%20DEL%20SUROESTE%20SE%20BENEFICIAN%20%20DE%20SERVICIO%20DE%20LAVANDER%C3%8DA%20GRATIS" title="Facebook" rel="nofollow" target="_blank"></a><a class="a2a_button_twitter" href="http://www.addtoany.com/add_to/twitter?linkurl=http%3A%2F%2Fhospitaldelaconcepcion.com%2Fempleados-de-institucion-de-salud-del-suroeste-se-benefician-de-servicio-de-lavanderia-gratis%2F&amp;linkname=EMPLEADOS%20DE%20INSTITUCI%C3%93N%20DE%20SALUD%20DEL%20SUROESTE%20SE%20BENEFICIAN%20%20DE%20SERVICIO%20DE%20LAVANDER%C3%8DA%20GRATIS" title="Twitter" rel="nofollow" target="_blank"></a><a class="a2a_button_google_plus" href="http://www.addtoany.com/add_to/google_plus?linkurl=http%3A%2F%2Fhospitaldelaconcepcion.com%2Fempleados-de-institucion-de-salud-del-suroeste-se-benefician-de-servicio-de-lavanderia-gratis%2F&amp;linkname=EMPLEADOS%20DE%20INSTITUCI%C3%93N%20DE%20SALUD%20DEL%20SUROESTE%20SE%20BENEFICIAN%20%20DE%20SERVICIO%20DE%20LAVANDER%C3%8DA%20GRATIS" title="Google+" rel="nofollow" target="_blank"></a><a class="a2a_dd a2a_target addtoany_share_save" href="https://www.addtoany.com/share#url=http%3A%2F%2Fhospitaldelaconcepcion.com%2Fempleados-de-institucion-de-salud-del-suroeste-se-benefician-de-servicio-de-lavanderia-gratis%2F&amp;title=EMPLEADOS%20DE%20INSTITUCI%C3%93N%20DE%20SALUD%20DEL%20SUROESTE%20SE%20BENEFICIAN%20%20DE%20SERVICIO%20DE%20LAVANDER%C3%8DA%20GRATIS" id="wpa2a_2"></a></p>]]></content:encoded>
			<wfw:commentRss>http://hospitaldelaconcepcion.com/empleados-de-institucion-de-salud-del-suroeste-se-benefician-de-servicio-de-lavanderia-gratis/feed/</wfw:commentRss>
		<slash:comments>0</slash:comments>
		</item>
		<item>
		<title>Comprometidos con Puerto Rico</title>
		<link>http://hospitaldelaconcepcion.com/comprometidos-con-puerto-rico/</link>
		<comments>http://hospitaldelaconcepcion.com/comprometidos-con-puerto-rico/#respond</comments>
		<pubDate>Fri, 20 Oct 2017 15:55:42 +0000</pubDate>
		<dc:creator><![CDATA[Alvin Cordero]]></dc:creator>
				<category><![CDATA[Noticias]]></category>

		<guid isPermaLink="false">http://hospitaldelaconcepcion.com/?p=4232</guid>
		<description><![CDATA[&#160; Hospital del suroeste comprometido con la comunidad antes, durante y después de María Durante el paso del huracán María el Hospital de la Concepción, ubicado en San Germán, se convirtió en una de las instituciones de salud principales para las personas adquirir sus servicios de salud. El alto compromiso de esta institución centenaria con [...]]]></description>
				<content:encoded><![CDATA[<p>&nbsp;</p>
<p><strong><u>Hospital del suroeste comprometido con la comunidad antes, durante y después de María</u></strong></p>
<p>Durante el paso del huracán María el Hospital de la Concepción, ubicado en San Germán, se convirtió en una de las instituciones de salud principales para las personas adquirir sus servicios de salud. El alto compromiso de esta institución centenaria con la comunidad y con Puerto Rico quedó evidenciado una vez más mediante su innovación clínica, el ofrecimiento de servicios de forma ininterrumpida y los esfuerzos realizados por empleados, facultativos y el grupo de voluntarios que trabajaron durante la emergencia.</p>
<p>Desde el año 2015, el Hospital de la Concepción ha invertido en la parte de redundancia de sus sistemas eléctricos para así mantener la institución en completo funcionamiento por medio de un sistema conocido como cogeneración de energía. Este sistema consiste en equipos de generación de electricidad que producen energía de forma independiente y equipos de que a su vez permiten bajar el consumo eléctrico de la institución. Gracias a este sistema, la institución de salud pudo mantenerse durante la emergencia en completo funcionamiento.</p>
<p>Cabe resaltar que personas cercanas al Hospital de la Concepción y hasta de pueblos distantes del sur y norte de Puerto Rico acudieron para recibir sus servicios de salud. Los esfuerzos ofrecidos fueron más allá de atender el gran volumen de pacientes que llegó al hospital debido a las interrupciones en servicio y colapso de otros hospitales a nivel de Puerto Rico. Durante los primeros días de la emergencia y transcurridos los días, la institución ofreció un servicio de calidad a todos los pacientes tanto hospitalizados como en el área de Sala de Emergencias.</p>
<p>Por otro lado, la institución se encontró con la necesidad de muchos pacientes que según eran dados de alta no podían llegar a sus hogares por falta de transportación y comunicación, por lo que ofrecieron el servicio de transportación gratis. De igual forma, identificaron pacientes con otras necesidades y cuando eran dados de alta se les entregaban artículos de primera necesidad.</p>
<p>El Hospital de la Concepción se ha mantenido activo llevando mensajes de prevención para crear conciencia en la población de la importancia de cuidar su salud luego del paso del huracán y se encuentra desarrollando esfuerzos para impactar diferentes comunidades del suroeste. De esta forma, la institución demuestra su compromiso con la salud y el bienestar de la población y con todo Puerto Rico.</p>
<p>&nbsp;</p>
<p><a class="a2a_button_facebook" href="http://www.addtoany.com/add_to/facebook?linkurl=http%3A%2F%2Fhospitaldelaconcepcion.com%2Fcomprometidos-con-puerto-rico%2F&amp;linkname=Comprometidos%20con%20Puerto%20Rico" title="Facebook" rel="nofollow" target="_blank"></a><a class="a2a_button_twitter" href="http://www.addtoany.com/add_to/twitter?linkurl=http%3A%2F%2Fhospitaldelaconcepcion.com%2Fcomprometidos-con-puerto-rico%2F&amp;linkname=Comprometidos%20con%20Puerto%20Rico" title="Twitter" rel="nofollow" target="_blank"></a><a class="a2a_button_google_plus" href="http://www.addtoany.com/add_to/google_plus?linkurl=http%3A%2F%2Fhospitaldelaconcepcion.com%2Fcomprometidos-con-puerto-rico%2F&amp;linkname=Comprometidos%20con%20Puerto%20Rico" title="Google+" rel="nofollow" target="_blank"></a><a class="a2a_dd a2a_target addtoany_share_save" href="https://www.addtoany.com/share#url=http%3A%2F%2Fhospitaldelaconcepcion.com%2Fcomprometidos-con-puerto-rico%2F&amp;title=Comprometidos%20con%20Puerto%20Rico" id="wpa2a_4"></a></p>]]></content:encoded>
			<wfw:commentRss>http://hospitaldelaconcepcion.com/comprometidos-con-puerto-rico/feed/</wfw:commentRss>
		<slash:comments>0</slash:comments>
		</item>
		<item>
		<title>Huracán María</title>
		<link>http://hospitaldelaconcepcion.com/huracan-maria/</link>
		<comments>http://hospitaldelaconcepcion.com/huracan-maria/#respond</comments>
		<pubDate>Tue, 19 Sep 2017 05:13:43 +0000</pubDate>
		<dc:creator><![CDATA[Alvin Cordero]]></dc:creator>
				<category><![CDATA[Noticias]]></category>

		<guid isPermaLink="false">http://hospitaldelaconcepcion.com/?p=4200</guid>
		<description><![CDATA[Expresiones oficiales de la administradora del Hospital de la Concepción, Felícita Bonilla ante el potencial paso del huracán María. &#160; San Germán… 18 de septiembre de 2017 Una vez más nos encontramos ante un aviso de huracán. Nuevamente reiteramos nuestro compromiso con nuestros pacientes, el personal del hospital y toda la comunidad. Pueden estar confiados [...]]]></description>
				<content:encoded><![CDATA[<p><strong>Expresiones oficiales de la administradora del Hospital de la Concepción, Felícita Bonilla </strong><strong>ante el potencial paso del huracán María. </strong></p>
<p>&nbsp;</p>
<p>San Germán… 18 de septiembre de 2017</p>
<p>Una vez más nos encontramos ante un aviso de huracán. Nuevamente reiteramos nuestro compromiso con nuestros pacientes, el personal del hospital y toda la comunidad. Pueden estar confiados en que continuaremos ofreciendo servicios de forma ininterrumpida para así garantizarles un cuidado médico seguro y eficaz a todos los pacientes.</p>
<p>Los exhortamos a tomar las medidas recomendadas para su seguridad y protección en sus hogares de manera que puedan prevenir situaciones de salud. De no tener una necesidad grave, permanezca en un lugar seguro durante el paso del huracán. Ya en nuestro Hospital estamos listos. Contamos con un innovador sistema de Cogeneración de Energía que junto a generadores de emergencia tienen capacidad para operar todo el Hospital. Confiamos en que estaremos ofreciendo todos nuestros servicios en caso de que el sistema eléctrico del país colapse.</p>
<p>###</p>
<p>&nbsp;</p>
<p>Contacto para los medios:</p>
<p>Neyssa García Toucet</p>
<p>Relacionista Profesional</p>
<p>787-543-6708</p>
<p><a href="mailto:ngtconsultant@gmail.com">ngtconsultant@gmail.com</a></p>
<p><a class="a2a_button_facebook" href="http://www.addtoany.com/add_to/facebook?linkurl=http%3A%2F%2Fhospitaldelaconcepcion.com%2Fhuracan-maria%2F&amp;linkname=Hurac%C3%A1n%20Mar%C3%ADa" title="Facebook" rel="nofollow" target="_blank"></a><a class="a2a_button_twitter" href="http://www.addtoany.com/add_to/twitter?linkurl=http%3A%2F%2Fhospitaldelaconcepcion.com%2Fhuracan-maria%2F&amp;linkname=Hurac%C3%A1n%20Mar%C3%ADa" title="Twitter" rel="nofollow" target="_blank"></a><a class="a2a_button_google_plus" href="http://www.addtoany.com/add_to/google_plus?linkurl=http%3A%2F%2Fhospitaldelaconcepcion.com%2Fhuracan-maria%2F&amp;linkname=Hurac%C3%A1n%20Mar%C3%ADa" title="Google+" rel="nofollow" target="_blank"></a><a class="a2a_dd a2a_target addtoany_share_save" href="https://www.addtoany.com/share#url=http%3A%2F%2Fhospitaldelaconcepcion.com%2Fhuracan-maria%2F&amp;title=Hurac%C3%A1n%20Mar%C3%ADa" id="wpa2a_6"></a></p>]]></content:encoded>
			<wfw:commentRss>http://hospitaldelaconcepcion.com/huracan-maria/feed/</wfw:commentRss>
		<slash:comments>0</slash:comments>
		</item>
		<item>
		<title>Nuevo sistema de cogeneración de energía</title>
		<link>http://hospitaldelaconcepcion.com/nuevo-sistema-de-cogeneracion-de-energia/</link>
		<comments>http://hospitaldelaconcepcion.com/nuevo-sistema-de-cogeneracion-de-energia/#respond</comments>
		<pubDate>Mon, 18 Sep 2017 11:20:20 +0000</pubDate>
		<dc:creator><![CDATA[Alvin Cordero]]></dc:creator>
				<category><![CDATA[Artículos]]></category>

		<guid isPermaLink="false">http://hospitaldelaconcepcion.com/?p=4191</guid>
		<description><![CDATA[Hospital del suroeste de Puerto Rico se convierte en la primera industria en utilizar sistema de cogeneración de energía. &#160; Con una inversión de 6.5 millones de dólares, el Hospital de la Concepción ubicado en San Germán, se convierte en la primera industria de Puerto Rico en iniciar un proyecto enfocado en la cogeneración de [...]]]></description>
				<content:encoded><![CDATA[<p><strong>Hospital del suroeste de Puerto Rico se convierte en la primera industria en utilizar sistema de cogeneración de energía.</strong></p>
<p>&nbsp;</p>
<p>Con una inversión de 6.5 millones de dólares, el Hospital de la Concepción ubicado en San Germán, se convierte en la primera industria de Puerto Rico en iniciar un proyecto enfocado en la cogeneración de energía. Este proyecto consiste en la instalación de equipos de generación de electricidad para producir energía a un costo menor y equipos de recuperación de calor que permitirán bajar el consumo eléctrico en la institución.</p>
<p>El Hospital de la Concepción, siendo una Institución centenaria, mantiene el compromiso de ofrecer servicios de alta calidad a su comunidad no tan solo enfocándose en la innovación clínica, sino también en su consistencia a la hora de ofrecer servicios. La Institución procura brindarle a sus Facultativos la tranquilidad que necesitan de saber que el Hospital de la Concepción está preparado para que puedan hacer uso de equipos de alta tecnología sin tener fluctuaciones de voltaje que pueden a su vez causar situaciones de estrés en medio de los procedimientos con sus pacientes. El sistema de cogeneración de energía más allá de bajar costos energéticos, permite atender un asunto bien importante para los hospitales, la redundancia en los sistemas críticos para garantizar la continuidad de servicios.</p>
<p>&nbsp;</p>
<p>“En el Hospital de la Concepción estamos invirtiendo en la parte de redundancia de nuestros sistemas eléctricos para así mantener la institución en completo funcionamiento. El proyecto de cogeneración de energía garantiza además el hacer uso de esa energía recuperada para reutilizarla en los sistemas de <em>comfort</em> de la institución como lo son los aires acondicionados y los sistemas de control de humedad. Este sistema a su vez permitirá generar un impacto favorable al medio ambiente mediante la reducción de emisiones, lo cual es de gran beneficio para la comunidad,” expresó el Lcdo. Edgar Crespo Campos, Administrador de Servicios de Apoyo, quien a su vez es ingeniero y está a cargo del proyecto.</p>
<p>&nbsp;</p>
<p>Esta tecnología lleva más de 25 años utilizándose en Estados Unidos y más de 50 años en Europa. El Hospital de la Concepción se convierte en la primera industria en dar paso a la implementación de la misma. “Estamos abriendo las puertas a que otras instituciones comiencen a mirar estas opciones para poder contener costos. Buscamos una solidez económica que nos va ayudar a mantener y continuar ofreciendo nuestros servicios a la vez que demostramos nuestro compromiso de innovar y transformar la crisis energética que tenemos en Puerto Rico. Tenemos tanto hospitales como industrias de la manufactura que han estado comunicándose interesados en dar el paso a esta innovadora tecnología” puntualizó el director ejecutivo de la Junta de Síndicos del Hospital de la Concepción, Gustavo Almodóvar Almodóvar.</p>
<p>&nbsp;</p>
<p><a class="a2a_button_facebook" href="http://www.addtoany.com/add_to/facebook?linkurl=http%3A%2F%2Fhospitaldelaconcepcion.com%2Fnuevo-sistema-de-cogeneracion-de-energia%2F&amp;linkname=Nuevo%20sistema%20de%20cogeneraci%C3%B3n%20de%20energ%C3%ADa" title="Facebook" rel="nofollow" target="_blank"></a><a class="a2a_button_twitter" href="http://www.addtoany.com/add_to/twitter?linkurl=http%3A%2F%2Fhospitaldelaconcepcion.com%2Fnuevo-sistema-de-cogeneracion-de-energia%2F&amp;linkname=Nuevo%20sistema%20de%20cogeneraci%C3%B3n%20de%20energ%C3%ADa" title="Twitter" rel="nofollow" target="_blank"></a><a class="a2a_button_google_plus" href="http://www.addtoany.com/add_to/google_plus?linkurl=http%3A%2F%2Fhospitaldelaconcepcion.com%2Fnuevo-sistema-de-cogeneracion-de-energia%2F&amp;linkname=Nuevo%20sistema%20de%20cogeneraci%C3%B3n%20de%20energ%C3%ADa" title="Google+" rel="nofollow" target="_blank"></a><a class="a2a_dd a2a_target addtoany_share_save" href="https://www.addtoany.com/share#url=http%3A%2F%2Fhospitaldelaconcepcion.com%2Fnuevo-sistema-de-cogeneracion-de-energia%2F&amp;title=Nuevo%20sistema%20de%20cogeneraci%C3%B3n%20de%20energ%C3%ADa" id="wpa2a_8"></a></p>]]></content:encoded>
			<wfw:commentRss>http://hospitaldelaconcepcion.com/nuevo-sistema-de-cogeneracion-de-energia/feed/</wfw:commentRss>
		<slash:comments>0</slash:comments>
		</item>
		<item>
		<title>Ante el aviso de vigilancia de huracán</title>
		<link>http://hospitaldelaconcepcion.com/ante-el-aviso-de-vigilancia-de-huracan/</link>
		<comments>http://hospitaldelaconcepcion.com/ante-el-aviso-de-vigilancia-de-huracan/#respond</comments>
		<pubDate>Tue, 05 Sep 2017 07:49:06 +0000</pubDate>
		<dc:creator><![CDATA[Alvin Cordero]]></dc:creator>
				<category><![CDATA[Noticias]]></category>

		<guid isPermaLink="false">http://hospitaldelaconcepcion.com/?p=4180</guid>
		<description><![CDATA[Expresiones oficiales de la administradora del Hospital de la Concepción, Felícita Bonilla ante el aviso de vigilancia de huracán emitido por el Servicio Nacional de Meteorología de Puerto Rico. &#160; San Germán… 4 de septiembre de 2017 Nuestros pacientes, el personal del hospital y toda la comunidad, pueden estar confiados en que nuestros servicios se [...]]]></description>
				<content:encoded><![CDATA[<p><strong>Expresiones oficiales de la administradora del Hospital de la Concepción, Felícita Bonilla </strong><strong>ante el aviso de vigilancia de huracán emitido por el Servicio Nacional de Meteorología de Puerto Rico.</strong></p>
<p>&nbsp;</p>
<p>San Germán… 4 de septiembre de 2017</p>
<p>Nuestros pacientes, el personal del hospital y toda la comunidad, pueden estar confiados en que nuestros servicios se continuarán ofreciendo de forma ininterrumpida. En nuestra institución, estaremos ofreciendo como es nuestra costumbre un cuidado médico seguro y eficaz a todos los pacientes. Exhortamos a la ciudadanía, particularmente a los vecinos del suroeste a que tomen las medidas recomendadas para su seguridad y protección en sus hogares de manera que puedan prevenir situaciones de salud. Además, los instamos a no prestar atención a rumores y a mantenerse informados a través de fuentes confiables como lo es el Servicio Nacional de Meteorología de Puerto Rico.</p>
<p>El Hospital de la Concepción cuenta con un innovador sistema de Cogeneración de Energía además de sus generadores de emergencia que tienen capacidad para operar todo el Hospital, poder continuar ofreciendo todos nuestros servicios y a su vez garantizar el que no nos veamos afectados en caso de que el sistema eléctrico del país colapse.</p>
<p style="text-align: center;">###</p>
<p>Contacto para los medios:</p>
<p>Neyssa García Toucet</p>
<p>Relacionista Profesional</p>
<p>787-543-6708</p>
<p><a href="mailto:ngtconsultant@gmail.com">ngtconsultant@gmail.com</a></p>
<p>&nbsp;</p>
<p><a class="a2a_button_facebook" href="http://www.addtoany.com/add_to/facebook?linkurl=http%3A%2F%2Fhospitaldelaconcepcion.com%2Fante-el-aviso-de-vigilancia-de-huracan%2F&amp;linkname=Ante%20el%20aviso%20de%20vigilancia%20de%20hurac%C3%A1n" title="Facebook" rel="nofollow" target="_blank"></a><a class="a2a_button_twitter" href="http://www.addtoany.com/add_to/twitter?linkurl=http%3A%2F%2Fhospitaldelaconcepcion.com%2Fante-el-aviso-de-vigilancia-de-huracan%2F&amp;linkname=Ante%20el%20aviso%20de%20vigilancia%20de%20hurac%C3%A1n" title="Twitter" rel="nofollow" target="_blank"></a><a class="a2a_button_google_plus" href="http://www.addtoany.com/add_to/google_plus?linkurl=http%3A%2F%2Fhospitaldelaconcepcion.com%2Fante-el-aviso-de-vigilancia-de-huracan%2F&amp;linkname=Ante%20el%20aviso%20de%20vigilancia%20de%20hurac%C3%A1n" title="Google+" rel="nofollow" target="_blank"></a><a class="a2a_dd a2a_target addtoany_share_save" href="https://www.addtoany.com/share#url=http%3A%2F%2Fhospitaldelaconcepcion.com%2Fante-el-aviso-de-vigilancia-de-huracan%2F&amp;title=Ante%20el%20aviso%20de%20vigilancia%20de%20hurac%C3%A1n" id="wpa2a_10"></a></p>]]></content:encoded>
			<wfw:commentRss>http://hospitaldelaconcepcion.com/ante-el-aviso-de-vigilancia-de-huracan/feed/</wfw:commentRss>
		<slash:comments>0</slash:comments>
		</item>
		<item>
		<title>Stress Test</title>
		<link>http://hospitaldelaconcepcion.com/stress-test/</link>
		<comments>http://hospitaldelaconcepcion.com/stress-test/#respond</comments>
		<pubDate>Wed, 30 Aug 2017 07:41:27 +0000</pubDate>
		<dc:creator><![CDATA[Alvin Cordero]]></dc:creator>
				<category><![CDATA[Artículos]]></category>

		<guid isPermaLink="false">http://hospitaldelaconcepcion.com/?p=4172</guid>
		<description><![CDATA[Informándolo sobre la prueba de esfuerzo cardíaco o ‘’stress test’’ Para evaluar las arterias del corazón el ‘’stress test’’ o prueba de esfuerzo cardíaco es el mejor estudio, no invasivo, que puede considerarse. El estudio lo puede ordenar su cardiólogo ó médico primario. Este estudio ayuda a evaluar la capacidad de su corazón a adaptarse [...]]]></description>
				<content:encoded><![CDATA[<p>Informándolo sobre la prueba de esfuerzo cardíaco o ‘’stress test’’</p>
<p>Para evaluar las arterias del corazón el ‘’stress test’’ o prueba de esfuerzo cardíaco es el mejor estudio, no invasivo, que puede considerarse. El estudio lo puede ordenar su cardiólogo ó médico primario. Este estudio ayuda a evaluar la capacidad de su corazón a adaptarse al ejercicio. En este estudio se le pedirá que camine en una trotadora. Mientras camina, le será monitoreada su presión, pulso y trazado electrocardiográfico. La intensidad del caminar será aumentada gradualmente hasta llegar a lo máximo que usted tolere de manera segura. Hay personas que no pueden caminar bien y hay alternativas para hacerlo sin caminar. Hay veces que el estudio es ordenado que se realice en el departamento de medicina nuclear y es porque tendrá una parte en que se le injectará material radioactivo para lograr imágenes de su corazón.</p>
<p>Los resultados de este estudio ayudarán a su médico a identificar la posibilidad de que tenga coronarias obstruidas. Las arterias del corazón se llaman las coronarias, son éstas las que transportan oxígeno al músculo cardíaco. Estas arterias se pueden obstruir y como consecuencia provocar síntomas como dolor de pecho o evolucionar a un infarto. Un infarto al corazón sucede cuando el músculo cardíaco no recibe oxígeno suficiente. Un infarto es exáctamente lo que se quiere evitar haciendo diagnósticos antes de que ya la enfermedad esté demasiado avanzada.</p>
<p>Todos queremos un corazón saludable, por lo tanto hay que tomar acción. Hay que ejercitarse , comer saludable y hacerse su ‘’stress test’’ cuando su médico así se lo indique.</p>
<p>&nbsp;</p>
<p>Por: Dra. Alicia Quiñones Acevedo</p>
<p>Especialista en Medicina Interna</p>
<p><a class="a2a_button_facebook" href="http://www.addtoany.com/add_to/facebook?linkurl=http%3A%2F%2Fhospitaldelaconcepcion.com%2Fstress-test%2F&amp;linkname=Stress%20Test" title="Facebook" rel="nofollow" target="_blank"></a><a class="a2a_button_twitter" href="http://www.addtoany.com/add_to/twitter?linkurl=http%3A%2F%2Fhospitaldelaconcepcion.com%2Fstress-test%2F&amp;linkname=Stress%20Test" title="Twitter" rel="nofollow" target="_blank"></a><a class="a2a_button_google_plus" href="http://www.addtoany.com/add_to/google_plus?linkurl=http%3A%2F%2Fhospitaldelaconcepcion.com%2Fstress-test%2F&amp;linkname=Stress%20Test" title="Google+" rel="nofollow" target="_blank"></a><a class="a2a_dd a2a_target addtoany_share_save" href="https://www.addtoany.com/share#url=http%3A%2F%2Fhospitaldelaconcepcion.com%2Fstress-test%2F&amp;title=Stress%20Test" id="wpa2a_12"></a></p>]]></content:encoded>
			<wfw:commentRss>http://hospitaldelaconcepcion.com/stress-test/feed/</wfw:commentRss>
		<slash:comments>0</slash:comments>
		</item>
		<item>
		<title>Nuevo grupo de Radiólogos llega al Hospital de la Concepción</title>
		<link>http://hospitaldelaconcepcion.com/nuevo-grupo-de-radiologos-llega-al-hospital-de-la-concepcion/</link>
		<comments>http://hospitaldelaconcepcion.com/nuevo-grupo-de-radiologos-llega-al-hospital-de-la-concepcion/#respond</comments>
		<pubDate>Sun, 04 Dec 2016 22:02:20 +0000</pubDate>
		<dc:creator><![CDATA[Alvin Cordero]]></dc:creator>
				<category><![CDATA[Artículos]]></category>

		<guid isPermaLink="false">http://hospitaldelaconcepcion.com/?p=3966</guid>
		<description><![CDATA[El Dr. Fernando J. López Rodríguez, neuroradiólogo y su grupo de radiólogos especialistas  han llegado al Hospital de la Concepción en San Germán.  El grupo que está compuesto por médicos radiólogos expertos  y a la vanguardia en múltiples especialidades son “Board Certified” por el American College of Radiology (ACR) y cuentan con sub-especialistas en el [...]]]></description>
				<content:encoded><![CDATA[<p>El Dr. Fernando J. López Rodríguez, neuroradiólogo y su grupo de radiólogos especialistas  han llegado al Hospital de la Concepción en San Germán.  El grupo que está compuesto por médicos radiólogos expertos  y a la vanguardia en múltiples especialidades son “Board Certified” por el American College of Radiology (ACR) y cuentan con sub-especialistas en el área de neuroradiología, imágenes músculo esqueletales, imágenes de la mujer, imágenes de abdomen y radiología intervencional.</p>
<p>Han comenzado a ofrecer un servicio responsable, completo y de calidad;  cualidades que los han distinguido como grupo en las instituciones donde brindan servicios.  Otras características que los describen lo son la rapidez en brindar resultados finales combinado con reportes comprensivos y detallados.</p>
<p>Dicho grupo trae consigo recursos administrativos 24 horas los 7 días a la semana los cuales juntos se adueñan del proceso radiológico total que va desde la orden médica hasta el resultado final al paciente y médico referente.</p>
<p>Su compromiso con la salud de los pacientes,  hace posible  obtener los reportes detallados y certeros en estudios ambulatorios en un periodo  de 24 horas y en algunos casos específicos, en dos horas, de ser solicitados.</p>
<p>Una vez más el Hospital de la Concepción continúa reforzando la calidad de sus servicios en beneficio de todos sus pacientes. Si quiere conocer más sobre los servicios que se ofrecen en el Centro de Imágenes del Hospital de la Concepción o de otros servicios, puede llamar al <a href="tel:(787)%20892-1860">787-892-1860</a>, acceder la página web <a href="http://www.hospitaldelaconcepcion.com/" target="_blank" data-saferedirecturl="https://www.google.com/url?hl=en&amp;q=http://www.hospitaldelaconcepci%C3%B3n.com&amp;source=gmail&amp;ust=1480974379116000&amp;usg=AFQjCNFzJFjr3b6fLjnPPC3ulHU0-cU8ag">www.hospitaldelaconcepcion.com</a> o buscar en Facebook.</p>
<p><a class="a2a_button_facebook" href="http://www.addtoany.com/add_to/facebook?linkurl=http%3A%2F%2Fhospitaldelaconcepcion.com%2Fnuevo-grupo-de-radiologos-llega-al-hospital-de-la-concepcion%2F&amp;linkname=Nuevo%20grupo%20de%20Radi%C3%B3logos%20llega%20al%20Hospital%20de%20la%20Concepci%C3%B3n" title="Facebook" rel="nofollow" target="_blank"></a><a class="a2a_button_twitter" href="http://www.addtoany.com/add_to/twitter?linkurl=http%3A%2F%2Fhospitaldelaconcepcion.com%2Fnuevo-grupo-de-radiologos-llega-al-hospital-de-la-concepcion%2F&amp;linkname=Nuevo%20grupo%20de%20Radi%C3%B3logos%20llega%20al%20Hospital%20de%20la%20Concepci%C3%B3n" title="Twitter" rel="nofollow" target="_blank"></a><a class="a2a_button_google_plus" href="http://www.addtoany.com/add_to/google_plus?linkurl=http%3A%2F%2Fhospitaldelaconcepcion.com%2Fnuevo-grupo-de-radiologos-llega-al-hospital-de-la-concepcion%2F&amp;linkname=Nuevo%20grupo%20de%20Radi%C3%B3logos%20llega%20al%20Hospital%20de%20la%20Concepci%C3%B3n" title="Google+" rel="nofollow" target="_blank"></a><a class="a2a_dd a2a_target addtoany_share_save" href="https://www.addtoany.com/share#url=http%3A%2F%2Fhospitaldelaconcepcion.com%2Fnuevo-grupo-de-radiologos-llega-al-hospital-de-la-concepcion%2F&amp;title=Nuevo%20grupo%20de%20Radi%C3%B3logos%20llega%20al%20Hospital%20de%20la%20Concepci%C3%B3n" id="wpa2a_14"></a></p>]]></content:encoded>
			<wfw:commentRss>http://hospitaldelaconcepcion.com/nuevo-grupo-de-radiologos-llega-al-hospital-de-la-concepcion/feed/</wfw:commentRss>
		<slash:comments>0</slash:comments>
		</item>
		<item>
		<title>Comunicado de prensa Open House CTI</title>
		<link>http://hospitaldelaconcepcion.com/comunicado-de-prensa-open-house-cti/</link>
		<comments>http://hospitaldelaconcepcion.com/comunicado-de-prensa-open-house-cti/#respond</comments>
		<pubDate>Thu, 22 Sep 2016 06:26:00 +0000</pubDate>
		<dc:creator><![CDATA[Alvin Cordero]]></dc:creator>
				<category><![CDATA[Noticias]]></category>

		<guid isPermaLink="false">http://142.4.10.62/?p=3781</guid>
		<description><![CDATA[&#160; &#160; COMUNICADO DE PRENSA   ABRE SUS PUERTAS NUEVO CENTRO DE TERAPIAS INTEGRADAS EN BENEFICIO DE LA COMUNIDAD DEL SUROESTE &#160; San Germán… 12 de noviembre de 2015… Como parte de los esfuerzos para ofrecer más y mejores servicios manteniendo altos estándares de calidad, el Hospital de la Concepción, dio a conocer la apertura [...]]]></description>
				<content:encoded><![CDATA[<p>&nbsp;</p>
<p>&nbsp;</p>
<p><strong>COMUNICADO DE PRENSA</strong></p>
<p><strong><u> </u></strong></p>
<p><strong><u>ABRE SUS PUERTAS NUEVO CENTRO DE TERAPIAS INTEGRADAS EN BENEFICIO DE LA COMUNIDAD DEL SUROESTE </u></strong></p>
<p>&nbsp;</p>
<p>San Germán… 12 de noviembre de 2015… Como parte de los esfuerzos para ofrecer más y mejores servicios manteniendo altos estándares de calidad, el Hospital de la Concepción, dio a conocer la apertura de su Centro de Terapias Integradas. El mismo ofrecerá servicios ambulatorios de Terapia Física, Terapia Ocupacional y Patología del Habla en beneficio de la comunidad del suroeste y en un solo lugar.</p>
<p>“En el Hospital de la Concepción somos fieles a nuestra misión y buscamos en cada momento ofrecer servicios de salud de la más alta calidad que permitan satisfacer las necesidades físicas de nuestros pacientes. No descansamos en tener a la disposición de la comunidad del suroeste todo lo que necesiten para el cuidado de su salud. El Centro de Terapias Integradas permitirá contribuir a una continuidad de cuidado de aquellos pacientes que así lo necesiten,” expresó Felícita Bonilla, administradora del Hospital de la Concepción.</p>
<p>&nbsp;</p>
<p>Este Centro además de ofrecer servicios a pacientes con necesidades de rehabilitación física, ocupacional y del habla, cuenta con fisiatras que realizan la evaluación médica y recomendación de tratamiento. Dentro del área de Terapia Ocupacional, por medio de profesionales capacitados se facilita y mejora el desempeño de los pacientes en sus actividades del diario vivir, control motor y coordinación.</p>
<p>De igual forma, el Centro de Terapias Integradas cuenta con Patólogos del Habla y Lenguaje quienes evalúan, diagnostican y tratan el lenguaje, la discriminación auditiva visual, razonamiento y memoria. El área de Terapia Física cuenta con un área de gimnasio donde se ofrecen programas de ejercicios y actividades terapéuticas, se integra el uso de terapia virtual como herramienta de rehabilitación y también la terapia láser.</p>
<p>“Los profesionales del Centro de Terapia Integrada son altamente calificados. Existe una necesidad tangible de estos servicios integrados tanto en el Oeste como a nivel Isla. Este Centro tiene como norte la pronta recuperación del paciente con la mayor independencia en las actividades cotidianas,” detalló Bonilla. El mismo ubica en la Torre Médica San Vicente de Paul y contará con el siguiente horario de servicios: lunes a viernes de 7:30 a. m. a 5:30 p. m. y sábados de 7:30 a. m.- 12:00 p. m. Para mayor información de los servicios que se ofrecen en el Centro de Terapias Integradas puede llamar al 787-659-0299.</p>
<p>###</p>
<p><a class="a2a_button_facebook" href="http://www.addtoany.com/add_to/facebook?linkurl=http%3A%2F%2Fhospitaldelaconcepcion.com%2Fcomunicado-de-prensa-open-house-cti%2F&amp;linkname=Comunicado%20de%20prensa%20Open%20House%20CTI" title="Facebook" rel="nofollow" target="_blank"></a><a class="a2a_button_twitter" href="http://www.addtoany.com/add_to/twitter?linkurl=http%3A%2F%2Fhospitaldelaconcepcion.com%2Fcomunicado-de-prensa-open-house-cti%2F&amp;linkname=Comunicado%20de%20prensa%20Open%20House%20CTI" title="Twitter" rel="nofollow" target="_blank"></a><a class="a2a_button_google_plus" href="http://www.addtoany.com/add_to/google_plus?linkurl=http%3A%2F%2Fhospitaldelaconcepcion.com%2Fcomunicado-de-prensa-open-house-cti%2F&amp;linkname=Comunicado%20de%20prensa%20Open%20House%20CTI" title="Google+" rel="nofollow" target="_blank"></a><a class="a2a_dd a2a_target addtoany_share_save" href="https://www.addtoany.com/share#url=http%3A%2F%2Fhospitaldelaconcepcion.com%2Fcomunicado-de-prensa-open-house-cti%2F&amp;title=Comunicado%20de%20prensa%20Open%20House%20CTI" id="wpa2a_16"></a></p>]]></content:encoded>
			<wfw:commentRss>http://hospitaldelaconcepcion.com/comunicado-de-prensa-open-house-cti/feed/</wfw:commentRss>
		<slash:comments>0</slash:comments>
		</item>
		<item>
		<title>Comunicado de prensa sobre Canción de Cuna en el Hospital</title>
		<link>http://hospitaldelaconcepcion.com/comunicado-de-prensa-sobre-cancion-de-cuna-en-el-hospital/</link>
		<comments>http://hospitaldelaconcepcion.com/comunicado-de-prensa-sobre-cancion-de-cuna-en-el-hospital/#respond</comments>
		<pubDate>Thu, 22 Sep 2016 06:06:29 +0000</pubDate>
		<dc:creator><![CDATA[Alvin Cordero]]></dc:creator>
				<category><![CDATA[Noticias]]></category>

		<guid isPermaLink="false">http://142.4.10.62/?p=3777</guid>
		<description><![CDATA[&#160; CANCIÓN DE CUNA DA LA BIENVENIDA A BEBÉS EN EL HOSPITAL DE LA CONCEPCIÓN &#160; San Germán…12 de julio de 2016…. El Director Ejecutivo de la Junta de Síndicos del Hospital de la Concepción, Gustavo Almodóvar, dio a conocer la iniciativa de esta Institución hospitalaria enfocada en recibir a todos los bebés recién nacidos [...]]]></description>
				<content:encoded><![CDATA[<p>&nbsp;</p>
<p><strong><u>CANCIÓN DE CUNA DA LA BIENVENIDA A BEBÉS EN EL HOSPITAL DE LA CONCEPCIÓN</u></strong></p>
<p>&nbsp;</p>
<p>San Germán…12 de julio de 2016…. El Director Ejecutivo de la Junta de Síndicos del Hospital de la Concepción, Gustavo Almodóvar, dio a conocer la iniciativa de esta Institución hospitalaria enfocada en recibir a todos los bebés recién nacidos con una canción de cuna.</p>
<p>Por años, el Hospital de la Concepción se ha destacado en el servicio que brinda a las madres y sus bebés al momento del alumbramiento.  Además de cuidar cada detalle durante el proceso de alumbramiento, la Institución Hospitalaria provee las condiciones adecuadas para que esa conexión tan necesaria entre mamá y bebé pueda darse desde el nacimiento, ya sea a través del alojamiento en conjunto, promoviendo la lactancia y proveyendo la seguridad que mamá y bebé necesitan durante su estadía en el Hospital.</p>
<p>“Hemos tomado la iniciativa de brindar un detalle más para ese momento tan especial a través de lo que hemos denominado alerta ‘Ángel de la Guarda’.  Cada vez que nazca un bebé en nuestra Institución, en el sistema de altavoz se escuchará una canción de cuna como indicador de que ha llegado una nueva vida al mundo. Esperamos que junto a los familiares de todos los bebés por nacer en el Hospital de la Concepción, podamos ofrecer nuestra bendición a esta nueva vida cada vez que nuestro sistema de altavoz nos indique la llegada de ese hermoso momento,” describió el Director Ejecutivo.</p>
<p>El Hospital de la Concepción ubicado en San Germán, es una Institución con más 500 años de servicio a la comunidad. Se distingue por sus esfuerzos de labor social dentro de los que se encuentran el programa Milagros de Vida y el programa Llevando Salud a tu Comunidad. De igual forma, cuenta con diferentes centros clínicos destacados entre los que se encuentran el Centro de Terapias Integradas, Centro de Rehabilitación Física y Centro de Infusión. De igual forma cuenta con un Laboratorio de Cateterismo y Salas de Operaciones Inteligentes.</p>
<p>&nbsp;</p>
<p style="text-align: center;">###</p>
<p><a class="a2a_button_facebook" href="http://www.addtoany.com/add_to/facebook?linkurl=http%3A%2F%2Fhospitaldelaconcepcion.com%2Fcomunicado-de-prensa-sobre-cancion-de-cuna-en-el-hospital%2F&amp;linkname=Comunicado%20de%20prensa%20sobre%20Canci%C3%B3n%20de%20Cuna%20en%20el%20Hospital" title="Facebook" rel="nofollow" target="_blank"></a><a class="a2a_button_twitter" href="http://www.addtoany.com/add_to/twitter?linkurl=http%3A%2F%2Fhospitaldelaconcepcion.com%2Fcomunicado-de-prensa-sobre-cancion-de-cuna-en-el-hospital%2F&amp;linkname=Comunicado%20de%20prensa%20sobre%20Canci%C3%B3n%20de%20Cuna%20en%20el%20Hospital" title="Twitter" rel="nofollow" target="_blank"></a><a class="a2a_button_google_plus" href="http://www.addtoany.com/add_to/google_plus?linkurl=http%3A%2F%2Fhospitaldelaconcepcion.com%2Fcomunicado-de-prensa-sobre-cancion-de-cuna-en-el-hospital%2F&amp;linkname=Comunicado%20de%20prensa%20sobre%20Canci%C3%B3n%20de%20Cuna%20en%20el%20Hospital" title="Google+" rel="nofollow" target="_blank"></a><a class="a2a_dd a2a_target addtoany_share_save" href="https://www.addtoany.com/share#url=http%3A%2F%2Fhospitaldelaconcepcion.com%2Fcomunicado-de-prensa-sobre-cancion-de-cuna-en-el-hospital%2F&amp;title=Comunicado%20de%20prensa%20sobre%20Canci%C3%B3n%20de%20Cuna%20en%20el%20Hospital" id="wpa2a_18"></a></p>]]></content:encoded>
			<wfw:commentRss>http://hospitaldelaconcepcion.com/comunicado-de-prensa-sobre-cancion-de-cuna-en-el-hospital/feed/</wfw:commentRss>
		<slash:comments>0</slash:comments>
		</item>
		<item>
		<title>COMUNICADO DE PRENSA &#8211; Experiencia Médica</title>
		<link>http://hospitaldelaconcepcion.com/comunicado-de-prensa/</link>
		<comments>http://hospitaldelaconcepcion.com/comunicado-de-prensa/#respond</comments>
		<pubDate>Thu, 22 Sep 2016 05:59:12 +0000</pubDate>
		<dc:creator><![CDATA[Alvin Cordero]]></dc:creator>
				<category><![CDATA[Noticias]]></category>

		<guid isPermaLink="false">http://142.4.10.62/?p=3773</guid>
		<description><![CDATA[  Estudiantes destacados participan de programa educativo Experiencia Médica Contacto: Neyssa García Toucet 787-543-6708   San Germán &#124; 23 de junio de 2016…El director ejecutivo de la Junta de Síndicos del Hospital de la Concepción, Gustavo Almodóvar, dio a conocer el inicio del 2do programa de Experiencia Médica. Esta iniciativa busca adentrar en el mundo [...]]]></description>
				<content:encoded><![CDATA[<p><strong> </strong></p>
<p><strong>Estudiantes destacados participan de programa educativo Experiencia Médica</strong></p>
<p>Contacto: Neyssa García Toucet</p>
<p>787-543-6708</p>
<p><strong> </strong></p>
<p>San Germán | 23 de junio de 2016…El director ejecutivo de la Junta de Síndicos del Hospital de la Concepción, Gustavo Almodóvar, dio a conocer el inicio del 2do programa de Experiencia Médica. Esta iniciativa busca adentrar en el mundo de la salud y la medicina a estudiantes de escuela superior dando la oportunidad de experimentar la vida de un médico en un Hospital.</p>
<p>&nbsp;</p>
<p>“El programa Experiencia Médica da la oportunidad a 20 estudiantes de excelencia académica del suroeste de Puerto Rico de pasar un mes dentro del Hospital siendo acompañantes de los facultativos de la Institución y conociendo de primera mano las diferentes ramas de la medicina. Por medio de Experiencia Médica, los estudiantes se relacionan con áreas de la medicina tales como pediatría, cirugía, radiología, medicina nuclear, medicina hiperbárica y ginecología,” expresó el Director Ejecutivo del Hospital.</p>
<p>&nbsp;</p>
<p>El creador del programa, doctor José Almodóvar, explicó que este que el segundo grupo del Programa de Experiencia Médica que estará en el Hospital realizando rotaciones con los facultativos durante cuatro semanas. De igual forma expresó, “siento satisfacción como profesional de la medicina de poder aportar con esta iniciativa a este segundo grupo de jóvenes. Este programa, permite conocer de primera mano cómo transcurre la vida de un médico en una institución hospitalaria en el proceso de proteger y promover la salud de sus pacientes. Confío que el mismo sea de enriquecimiento para transformar su futuro.”</p>
<p>&nbsp;</p>
<p>Por su parte, el presidente de la Junta de Síndicos del Hospital de la Concepción, Monseñor Alvaro Corrada del Río, destacó que el programa Experiencia Médica además de ser sufragado en su totalidad por la institución hospitalaria, permite continuar diciendo presente en el futuro de la salud de Puerto Rico y de los servicios médicos. “Hay que seguir reinventándose, y proyectando la salud de Puerto Rico para que sea de primera clase,” destacó.</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p style="text-align: center;"><strong>###</strong></p>
<p><a class="a2a_button_facebook" href="http://www.addtoany.com/add_to/facebook?linkurl=http%3A%2F%2Fhospitaldelaconcepcion.com%2Fcomunicado-de-prensa%2F&amp;linkname=COMUNICADO%20DE%20PRENSA%20%E2%80%93%20Experiencia%20M%C3%A9dica" title="Facebook" rel="nofollow" target="_blank"></a><a class="a2a_button_twitter" href="http://www.addtoany.com/add_to/twitter?linkurl=http%3A%2F%2Fhospitaldelaconcepcion.com%2Fcomunicado-de-prensa%2F&amp;linkname=COMUNICADO%20DE%20PRENSA%20%E2%80%93%20Experiencia%20M%C3%A9dica" title="Twitter" rel="nofollow" target="_blank"></a><a class="a2a_button_google_plus" href="http://www.addtoany.com/add_to/google_plus?linkurl=http%3A%2F%2Fhospitaldelaconcepcion.com%2Fcomunicado-de-prensa%2F&amp;linkname=COMUNICADO%20DE%20PRENSA%20%E2%80%93%20Experiencia%20M%C3%A9dica" title="Google+" rel="nofollow" target="_blank"></a><a class="a2a_dd a2a_target addtoany_share_save" href="https://www.addtoany.com/share#url=http%3A%2F%2Fhospitaldelaconcepcion.com%2Fcomunicado-de-prensa%2F&amp;title=COMUNICADO%20DE%20PRENSA%20%E2%80%93%20Experiencia%20M%C3%A9dica" id="wpa2a_20"></a></p>]]></content:encoded>
			<wfw:commentRss>http://hospitaldelaconcepcion.com/comunicado-de-prensa/feed/</wfw:commentRss>
		<slash:comments>0</slash:comments>
		</item>
	</channel>
</rss>

<!-- Dynamic page generated in 0.655 seconds. -->
<!-- Cached page generated by WP-Super-Cache on 2017-11-17 07:01:28 -->
