<?php if (has_term('', W2DC_CATEGORIES_TAX, $listing->post->ID)): ?>
<div class="w2dc_field w2dc_field_output_block w2dc_field_output_block_<?php echo $content_field->id; ?>">
	<?php if ($content_field->icon_image): ?>
		<img class="w2dc_field_icon" src="<?php echo W2DC_FIELDS_ICONS_URL; ?><?php echo $content_field->icon_image; ?>" />
	<?php endif; ?>
	<?php if (!$content_field->is_hide_name): ?>
		<span class="w2dc_field_name"><?php echo $content_field->name?>:</span>
	<?php endif; ?>
	<span class="w2dc_field_content">
	<?php echo get_the_term_list($listing->post->ID, W2DC_CATEGORIES_TAX, '', ', ', ''); ?>
	</span>
</div>
<?php endif; ?>