		<div class="w2dc_content">
			<?php w2dc_renderMessages(); ?>

			<?php if ($frontend_controller->listings): ?>
			<?php while ($frontend_controller->query->have_posts()): ?>
				<?php $frontend_controller->query->the_post(); ?>
				
				<?php w2dc_renderTemplate('frontend/frontpanel_buttons.tpl.php', array('listing' => $frontend_controller->listings[get_the_ID()])); ?>

				<div>
					<?php if ($frontend_controller->listings[get_the_ID()]->title()): ?>
					<header>
						<h2><?php echo $frontend_controller->listings[get_the_ID()]->title(); ?></h2>
	
						<?php if ($frontend_controller->breadcrumbs): ?>
						<div class="breadcrumbs">
							<?php echo $frontend_controller->getBreadCrumbs(); ?>
						</div>
						<?php endif; ?>
					</header>
					<?php endif; ?>
	
					<article id="post-<?php the_ID(); ?>">
						<?php $frontend_controller->listings[get_the_ID()]->display(true); ?>
					</article>
				</div>
			<?php endwhile; endif; ?>
		</div>