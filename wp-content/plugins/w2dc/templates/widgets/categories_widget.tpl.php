<?php echo $args['before_widget']; ?>
<?php if (!empty($title))
echo $args['before_title'] . $title . $args['after_title'];
?>
<div class="w2dc_content w2dc_widget w2dc_categories_widget">
	<?php w2dc_renderAllCategories(0, $depth, 1, $counter, $subcats); ?>
</div>
<?php echo $args['after_widget']; ?>