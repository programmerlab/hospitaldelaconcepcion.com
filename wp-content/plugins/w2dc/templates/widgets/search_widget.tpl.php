<?php echo $args['before_widget']; ?>
<?php if (!empty($title))
echo $args['before_title'] . $title . $args['after_title'];
?>
<div class="w2dc_content w2dc_widget w2dc_search_widget">
	<?php
	$search_form = new search_form();
	$search_form->display();
	?>
</div>
<?php echo $args['after_widget']; ?>