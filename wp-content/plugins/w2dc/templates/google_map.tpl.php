<div class="w2dc_content">
<?php if (!$static_image): ?>
	<script>
		map_markers_attrs_array.push(new map_markers_attrs(<?php echo $unique_map_id; ?>, eval(<?php echo $locations_options; ?>), <?php echo ($enable_radius_cycle) ? 1 : 0; ?>, <?php echo ($enable_clusters) ? 1 : 0; ?>, <?php echo ($show_summary_button) ? 1 : 0; ?>, <?php echo ($show_readmore_button) ? 1 : 0; ?>, <?php echo $map_args; ?>));
	</script>

	<div id="maps_canvas_<?php echo $unique_map_id; ?>" class="maps_canvas" style="width: <?php if ($width) echo $width . 'px'; else echo 'auto'; ?>; height: <?php if ($height) echo $height; else echo '300'; ?>px"></div>
	<?php if ($show_directions): ?>
	<div id="maps_direction_from">
		<?php _e('Get direction from:', 'W2DC'); ?> <input type="text" size="60" id="from_direction_<?php echo $unique_map_id; ?>" class="form-control" />
	</div>
	<div id="maps_direction_to">
		<?php _e('direction to:', 'W2DC'); ?><br />
		<?php $i = 1; ?>
		<?php foreach ($locations_array AS $location): ?>
		<input type="radio" name="select_direction" class="select_direction_<?php echo $unique_map_id; ?>" <?php if (count($locations_array) == 1) echo "style='display:none'"; ?> <?php checked($i, 1); ?> value="<?php echo $location->getWholeAddress(); ?>" /> <b><?php echo $location->getWholeAddress(); ?></b><br />
		<?php endforeach; ?>
	</div>

	<input type="button" class="direction_button front-btn btn btn-info" id="get_direction_button_<?php echo $unique_map_id; ?>" value="<?php _e('Get direction', 'W2DC'); ?>">

	<div id="route_<?php echo $unique_map_id; ?>" class="maps_direction_route"></div>
	<?php endif; ?>
<?php else: ?>
	<img src="http://maps.googleapis.com/maps/api/staticmap?size=795x350&<?php foreach ($locations_array  AS $location) { if ($location->map_coords_1 != 0 && $location->map_coords_2 != 0) { ?>markers=<?php if ($w2dc_instance->map_markers_url && $location->map_icon_file) { ?>icon:<?php echo $w2dc_instance->map_markers_url . 'icons/' . urlencode($location->map_icon_file) . '%7C'; }?><?php echo $location->map_coords_1 . ',' . $location->map_coords_2 . '&'; }} ?><?php if ($map_zoom) echo 'zoom=' . $map_zoom; ?>&sensor=true" />
<?php endif; ?>
</div>