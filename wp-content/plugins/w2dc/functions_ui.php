<?php



function w2dc_tax_dropdowns_init($tax = 'category', $field_name = null, $term_id = null, $count = true, $labels = array(), $titles = array(), $uID = null) {

	// unique ID need when we place some dropdowns groups on one page

	if (!$uID)

		$uID = rand(1, 10000);



	wp_enqueue_script('w2dc_tax_dropdowns_handle');

	

	$localized_data[$uID] = array(

			'labels' => $labels,

			'titles' => $titles

	);

	echo "<script>js_objects['tax_dropdowns_" . $uID . "'] = " . json_encode($localized_data) . "</script>";



	if (!is_null($term_id) && $term_id != 0) {

		$chain = array();

		$parent_id = $term_id;

		while ($parent_id != 0) {

			if ($term = get_term($parent_id, $tax)) {

				$chain[] = $term->term_id;

				$parent_id = $term->parent;

			} else

				break;

		}

	}

	$chain[] = 0;

	$chain = array_reverse($chain);



	if (!$field_name)

		$field_name = 'selected_tax[' . $uID . ']';



	echo '<div id="tax_dropdowns_wrap_' . $uID . '" class="' . $tax . ' cs_count_' . (int)$count . ' tax_dropdowns_wrap">';

	echo '<input type="hidden" name="' . $field_name . '" id="selected_tax[' . $uID . ']" class="selected_tax_' . $tax . '" value="' . $term_id . '" />';

	foreach ($chain AS $key=>$term_id) {

		// there is a wp bug with pad_counts in get_terms function - so we use this construction

		if ($terms = wp_list_filter(get_categories(array('taxonomy' => $tax, 'pad_counts' => true, 'hide_empty' => false)), array('parent' => $term_id))) {

			$level_num = $key + 1;

			echo '<div id="wrap_chainlist_' . $level_num . '_' .$uID . '" class="wrap_chainlist">';



			if (isset($labels[$key]))

				echo '<label for="chainlist_' . $level_num . '_' . $uID . '">' . $labels[$key] . '</label>';



			echo '<select id="chainlist_' . $level_num . '_' . $uID . '" class="form-control form-group">';

			//echo '<option value="">- ' . ((isset($titles[$key])) ? $titles[$key] : __('Select term', 'W2DC')) . ' -</option>';
			echo '<option value="">- ' . "Especialidad" . ' -</option>';


			foreach ($terms as $term) {

				if ($count)

					$term_count = " ($term->count)";

				else

					 $term_count = '';

				if (isset($chain[$key+1]) && $term->term_id == $chain[$key+1]) $selected = 'selected'; else $selected = '';

				echo '<option id="' . $term->slug . '" value="' . $term->term_id . '" ' . $selected . '>' . $term->name . $term_count . '</option>';

			}

			echo '</select>';

			echo '<div class="clear"></div>';

			echo '</div>';

		}

	}

	echo '</div>';

}



function w2dc_tax_dropdowns_updateterms() {

	$parentid = w2dc_getValue($_POST, 'parentid');

	$next_level = w2dc_getValue($_POST, 'next_level');

	$tax = w2dc_getValue($_POST, 'tax');

	$count = w2dc_getValue($_POST, 'count');

	if (!$label = w2dc_getValue($_POST, 'label'))

		$label = '';

	if (!$title = w2dc_getValue($_POST, 'title'))

		$title = __('Select term', 'W2DC');

	$uID = w2dc_getValue($_POST, 'uID');



	// there is a wp bug with pad_counts in get_terms function - so we use this construction

	$terms = wp_list_filter(get_categories(array('taxonomy' => $tax, 'pad_counts' => true, 'hide_empty' => false)), array('parent' => $parentid));



	if (!empty($terms)) {

		echo '<div id="wrap_chainlist_' . $next_level . '_' . $uID . '" class="wrap_chainlist">';



		if ($label)

			echo '<label for="chainlist_' . $next_level . '_' . $uID . '">' . $label . '</label>';



		echo '<select id="chainlist_' . $next_level . '_' . $uID . '" class="form-control form-group">';



		echo '<option value="">- ' . $title . ' -</option>';



		foreach ($terms as $term) {

			if ($count == 'cs_count_1') {

				$term_count = " ($term->count)";

			} else { $term_count = '';

			}

			echo '<option id="' . $term->slug . '" value="' . $term->term_id . '">' . $term->name . $term_count . '</option>';

		}



		echo '</select>';

		echo '</div>';



	}

	die();

}



function w2dc_renderOptionsTerms($tax, $parent, $selected_terms, $level = 0) {

	$terms = get_terms($tax, array('parent' => $parent, 'hide_empty' => false));



	foreach ($terms AS $term) {

		echo '<option value="' . $term->term_id . '" ' . (($selected_terms && in_array($term->term_id, $selected_terms)) ? 'selected' : '') . '>' . (str_repeat('&nbsp;&nbsp;&nbsp;', $level)) . $term->name . '</option>';

		w2dc_renderOptionsTerms($tax, $term->term_id, $selected_terms, $level+1);

	}

	return $terms;

}

function w2dc_termsSelectList($name, $tax = 'category', $selected_terms = array()) {

	echo '<select multiple="multiple" name="' . $name . '[]" class="selected_terms_list form-control form-group" style="height: 300px">';

	echo '<option value="" ' . ((!$selected_terms) ? 'selected' : '') . '>' . __('- Select All -', 'W2DC') . '</option>';



	w2dc_renderOptionsTerms($tax, 0, $selected_terms);



	echo '</select>';

}



function w2dc_recaptcha() {

	if (get_option('w2dc_enable_recaptcha') && get_option('w2dc_recaptcha_public_key') && get_option('w2dc_recaptcha_private_key')) {

		if (!function_exists('_recaptcha_qsencode'))

			require_once(W2DC_PATH . 'recaptcha/recaptchalib.php');

		return '<p>' . recaptcha_get_html(get_option('w2dc_recaptcha_public_key')) . '</p>';

	}

}



function w2dc_is_recaptcha_passed() {

	if (get_option('w2dc_enable_recaptcha') && get_option('w2dc_recaptcha_public_key') && get_option('w2dc_recaptcha_private_key')) {

		if (isset($_POST["recaptcha_challenge_field"]) && isset($_POST["recaptcha_response_field"])) {

			if (!function_exists('_recaptcha_qsencode'))

				require_once(W2DC_PATH . 'recaptcha/recaptchalib.php');

			$responce = recaptcha_check_answer(get_option('w2dc_recaptcha_private_key'),

					$_SERVER["REMOTE_ADDR"],

					$_POST["recaptcha_challenge_field"],

					$_POST["recaptcha_response_field"]);

			return $responce->is_valid;

		} else {

			return false;

		}

	} else

		return true;

}



function w2dc_orderLinks($base_url, $defaults = array()) {

	global $w2dc_instance;



	if (isset($_GET['order_by']) && $_GET['order_by']) {

		$order_by = $_GET['order_by'];

		$order = w2dc_getValue($_GET, 'order', 'ASC');

	} else {

		if (isset($defaults['order_by']) && $defaults['order_by']) {

			$order_by = $defaults['order_by'];

			$order = w2dc_getValue($defaults, 'order', 'ASC');

		} else {

			$order_by = 'post_date';

			$order = 'DESC';

		}

	}



	$ordering['array'] = array('post_date' => __('Date', 'W2DC'), 'title' => __('Title', 'W2DC'));



	$content_fields = $w2dc_instance->content_fields->getOrderingContentFields();

	foreach ($content_fields AS $content_field)

		$ordering['array'][$content_field->slug] = $content_field->name;

	

	$ordering['links'] = array();

	foreach ($ordering['array'] AS $field_slug=>$field_name) {

		$class = '';

		if ($order_by == $field_slug) {

			if ($order == 'ASC') {

				$class = 'ascending';

				$url = add_query_arg('order', 'DESC', add_query_arg('order_by', $field_slug, $base_url));

			} elseif ($order == 'DESC') {

				$class = 'descending';

				$url = add_query_arg('order_by', $field_slug, $base_url);

			}

		} else

			$url = add_query_arg('order_by', $field_slug, $base_url);



		$ordering['links'][$field_slug] = '<a class="' . $class . '" href="' . $url . '">' .$field_name . '</a>';

	}



	$ordering = apply_filters('w2dc_ordering_options', $ordering, $base_url, $defaults);



	echo __('Order by: ', 'W2DC') . implode(' | ', $ordering['links']);

}



function w2dc_renderSubCategories($parent_category_slug = '', $columns = 2, $count = false) {

	if ($parent_category_slug) {

		$parent_category = get_term_by('slug', $parent_category_slug, W2DC_CATEGORIES_TAX);

		$parent_category_id = $parent_category->term_id;

	} else

		$parent_category_id = 0;

	

	w2dc_renderAllCategories($parent_category_id, 1, $columns, $count);

}



function w2dc_terms_checklist($post_id) {

	if ($terms = wp_list_filter(get_categories(array('taxonomy' => W2DC_CATEGORIES_TAX, 'pad_counts' => true, 'hide_empty' => false)), array('parent' => 0))) {

		$checked_categories_ids = array();

		$checked_categories = wp_get_object_terms($post_id, W2DC_CATEGORIES_TAX);

		foreach ($checked_categories AS $term)

			$checked_categories_ids[] = $term->term_id;



		echo '<ul class="categorychecklist">';

		foreach ($terms AS $term) {

			$checked = '';

			if (in_array($term->term_id, $checked_categories_ids))

				$checked = 'checked';

				

			echo '

<li id="' . W2DC_CATEGORIES_TAX . '-' . $term->term_id . '">';

			echo '<label class="selectit"><input type="checkbox" ' . $checked . ' id="in-' . W2DC_CATEGORIES_TAX . '-' . $term->term_id . '" name="tax_input[' . W2DC_CATEGORIES_TAX . '][]" value="' . $term->term_id . '"> ' . $term->name . '</label>';

			echo _w2dc_terms_checklist($term->term_id, $checked_categories_ids);

			echo '</li>';

		}

		echo '</ul>';

	}

}

function _w2dc_terms_checklist($parent = 0, $checked_categories_ids = array()) {

	$html = '';

	if ($terms = wp_list_filter(get_categories(array('taxonomy' => W2DC_CATEGORIES_TAX, 'pad_counts' => true, 'hide_empty' => false)), array('parent' => $parent))) {

		$html .= '<ul class="children">';

		foreach ($terms AS $term) {

			$checked = '';

			if (in_array($term->term_id, $checked_categories_ids))

				$checked = 'checked';



			$html .= '

<li id="' . W2DC_CATEGORIES_TAX . '-' . $term->term_id . '">';

			$html .= '<label class="selectit"><input type="checkbox" ' . $checked . ' id="in-' . W2DC_CATEGORIES_TAX . '-' . $term->term_id . '" name="tax_input[' . W2DC_CATEGORIES_TAX . '][]" value="' . $term->term_id . '"> ' . $term->name . '</label>';

			$html .= _w2dc_terms_checklist($term->term_id);

			$html .= '</li>';

		}

		$html .= '</ul>';

	}

	return $html;

}



function w2dc_categoriesOfLevels($allowed_levels) {

	global $w2dc_instance;

	

	$allowed_categories = array();

	foreach ($allowed_levels AS $level_id) {

		$level = $w2dc_instance->levels->levels_array[$level_id];

		$allowed_categories = array_merge($allowed_categories, $level->categories);

	}

	

	return $allowed_categories;

}



function w2dc_renderAllCategories($parent = 0, $depth = 2, $columns = 2, $count = false, $max_subcategories = 0, $allowed_levels = array()) {

	if ($depth > 2)

		$depth = 2;

	if ($depth == 0 || !is_numeric($depth))

		$depth = 1;

	if ($columns > 4)

		$columns = 4;

	if ($columns == 0 || !is_numeric($columns))

		$columns = 1;

	

	$allowed_categories = implode(',', w2dc_categoriesOfLevels($allowed_levels));



	// we use array_merge with empty array because we need to flush keys in terms array

	$terms = array_merge(

			wp_list_filter(

					get_categories(array(

							'taxonomy' => W2DC_CATEGORIES_TAX,

							'pad_counts' => true,

							'hide_empty' => false,

							// filter terms by listings levels

							'include' => $allowed_categories,

					)),

					array('parent' => $parent)

			), array());



	if ($terms) {

		echo '<div class="w2dc_content categories-columns categories-columns-' . $columns . '">';

		$terms_count = count($terms);

		$counter = 0;

		$tcounter = 0;



		foreach ($terms AS $key=>$term) {

			$tcounter++;

			if ($counter == 0)

				echo '<div class="categories-row">';



			echo '<div class="categories-column categories-column-' . $columns . '">';

			if ($count)

				$term_count = " ($term->count)";

			else

				$term_count = '';



			if ($icon_file = w2dc_getCategoryIcon($term->term_id))

				$icon_image = '<img class="w2dc_field_icon" src="' . W2DC_CATEGORIES_ICONS_URL . $icon_file . '" />';

			else

				$icon_image = '';



			echo '<div class="categories-column-wrapper">';

			echo '<div class="categories-root"><a href="' . get_term_link($term) . '" title="' . $term->name .$term_count . '">' . $icon_image . $term->name .$term_count . '</a></div>';

			echo _w2dc_renderAllCategories($term->term_id, $depth, 1, $count, $max_subcategories, $allowed_categories);

			echo '</div>';



			echo '</div>';



			$counter++;

			if ($counter == $columns)

				echo '</div>';

			elseif ($tcounter == $terms_count && $counter != $columns) {

				while ($counter != $columns) {

					echo '<div class="categories-column categories-column-' . $columns . ' categories-column-hidden"></div>';

					$counter++;

				}

				echo '</div>';

			}

			if ($counter == $columns) $counter = 0;

		}

		echo '</div>';

	}

}

function _w2dc_renderAllCategories($parent = 0, $depth = 2, $level = 0, $count = false, $max_subcategories = 0, $allowed_categories = array()) {

	$terms = wp_list_filter(

			get_categories(array(

					'taxonomy' => W2DC_CATEGORIES_TAX,

					'pad_counts' => true,

					'hide_empty' => false,

					// filter terms by listings levels

					'include' => $allowed_categories,

			)), array('parent' => $parent));

	

	$html = '';

	if (($depth == 0 || !is_numeric($depth) || $depth > $level)) {

		$level++;

		$counter = 0;

		$html .= '<div class="subcategories">';

		$html .= '<ul>';

		foreach ($terms AS $term) {

			if ($count)

				$term_count = " ($term->count)";

			else

				$term_count = '';



			if ($icon_file = w2dc_getCategoryIcon($term->term_id))

				$icon_image = '<img class="w2dc_field_icon" src="' . W2DC_CATEGORIES_ICONS_URL . $icon_file . '" />';

			else

				$icon_image = '';



			$counter++;

			if ($max_subcategories != 0 && $counter > $max_subcategories) {

				$html .= '<li><a href="' . get_term_link(intval($parent), W2DC_CATEGORIES_TAX) . '">' . __('View all subcategories ->', 'W2DC') . '</a></li>';

				break;

			} else

				$html .= '<li><a href="' . get_term_link($term) . '" title="' . $term->name .$term_count . '">' . $icon_image . $term->name . $term_count . '</a></li>';

		}

		$html .= '</ul>';

		$html .= '</div>';

	}

	return $html;

}



function w2dc_getCategoryIcon($term_id) {

	global $w2dc_instance;

	

	if ($icon_file = $w2dc_instance->categories_manager->getCategoryIconFile($term_id))

		return $icon_file;

}



function w2dc_show_404() {

	status_header(404);

	nocache_headers();

	include(get_404_template());

	exit;

}



function w2dc_login_form($args = array()) {

	$defaults = array(

			'redirect' => ( is_ssl() ? 'https://' : 'http://' ) . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'], // Default redirect is back to the current page

			'form_id' => 'loginform',

			'label_username' => __( 'Username' ),

			'label_password' => __( 'Password' ),

			'label_remember' => __( 'Remember Me' ),

			'label_log_in' => __( 'Log In' ),

			'id_username' => 'user_login',

			'id_password' => 'user_pass',

			'id_remember' => 'rememberme',

			'id_submit' => 'wp-submit',

			'remember' => true,

			'value_username' => '',

			'value_remember' => false, // Set this to true to default the "Remember me" checkbox to checked

	);

	$args = wp_parse_args($args, apply_filters( 'login_form_defaults', $defaults));

	

	echo '<div class="w2dc_content">';

	

	echo '

		<form name="' . $args['form_id'] . '" id="' . $args['form_id'] . '" action="' . esc_url( site_url( 'wp-login.php', 'login_post' ) ) . '" method="post" class="w2dc_login_form" role="form">

			' . apply_filters( 'login_form_top', '', $args ) . '

			<p class="form-group">

				<label for="' . esc_attr( $args['id_username'] ) . '">' . esc_html( $args['label_username'] ) . '</label>

				<input type="text" name="log" id="' . esc_attr( $args['id_username'] ) . '" class="form-control" value="' . esc_attr( $args['value_username'] ) . '" />

			</p>

			<p class="login-password">

				<label for="' . esc_attr( $args['id_password'] ) . '">' . esc_html( $args['label_password'] ) . '</label>

				<input type="password" name="pwd" id="' . esc_attr( $args['id_password'] ) . '" class="form-control" value="" />

			</p>

			' . apply_filters( 'login_form_middle', '', $args ) . '

			' . ( $args['remember'] ? '<p class="checkbox"><label><input name="rememberme" type="checkbox" id="' . esc_attr( $args['id_remember'] ) . '" value="forever"' . ( $args['value_remember'] ? ' checked="checked"' : '' ) . ' /> ' . esc_html( $args['label_remember'] ) . '</label></p>' : '' ) . '

			<p class="login-submit">

				<input type="submit" name="wp-submit" id="' . esc_attr( $args['id_submit'] ) . '" class="btn btn-primary" value="' . esc_attr( $args['label_log_in'] ) . '" />

				<input type="hidden" name="redirect_to" value="' . esc_url( $args['redirect'] ) . '" />

			</p>

			' . apply_filters( 'login_form_bottom', '', $args ) . '

		</form>';



	do_action('login_form');

	do_action('login_footer');

	echo '<p id="nav">';

	if (get_option('users_can_register'))

		echo '<a href="' . add_query_arg('action', 'register', home_url('wp-login.php')) . '" rel="nofollow">' . __('Register', 'W2DC') . '</a> | ';

 

	echo '<a title="' . esc_attr__('Password Lost and Found', 'W2DC') . '" href="' . add_query_arg('action', 'lostpassword', home_url('wp-login.php')) . '">' . __('Lost your password?', 'W2DC') . '</a>';

	echo '</p>';

	

	echo '</div>';

}





if (!function_exists('renderPaginator')) {

	function renderPaginator($query) {

		if (get_class($query) == 'WP_Query') {

			if (get_query_var('page'))

				$paged = get_query_var('page');

			elseif (get_query_var('paged'))

				$paged = get_query_var('paged');

			else

				$paged = 1;

		

			$total_pages = $query->max_num_pages;

			$total_lines = ceil($total_pages/10);

		

			if ($total_pages > 1){

				$current_page = max(1, $paged);

				$current_line = floor(($current_page-1)/10) + 1;

		

				$previous_page = $current_page - 1;

				$next_page = $current_page + 1;

				$previous_line_page = floor(($current_page-1)/10)*10;

				$next_line_page = ceil($current_page/10)*10 + 1;

		

				echo '<div class="w2dc_pagination_wrapper">';

				echo '<ul class="pagination">';

				if ($total_pages > 10 && $current_page > 10)

					echo '<li class="previous_line"><a href="' . get_pagenum_link($previous_line_page) . '" title="' . esc_attr__('Previous Line', 'W2DC') . '"><<</a></li>' ;

		

				if ($total_pages > 3 && $current_page > 1)

					echo '<li class="previous"><a href="' . get_pagenum_link($previous_page) . '" title="' . esc_attr__('Previous Page', 'W2DC') . '"><</i></a></li>' ;

		

				$count = ($current_line-1)*10;

				$end = ($total_pages < $current_line*10) ? $total_pages : $current_line*10;

				while ($count < $end) {

					$count = $count + 1;

					if ($count == $current_page)

						echo '<li class="active"><a href="' . get_pagenum_link($count) . '">' . $count . '</a></li>' ;

					else

						echo '<li class="inactive"><a href="' . get_pagenum_link($count) . '">' . $count . '</a></li>' ;

		

				}

		

				if ($total_pages > 3 && $current_page < $total_pages)

					echo '<li class="next"><a href="' . get_pagenum_link($next_page) . '" title="' . esc_attr__('Next Page', 'W2DC') . '">></i></a></li>' ;

		

				if ($total_pages > 10 && $current_line < $total_lines)

					echo '<li class="next_line"><a href="' . get_pagenum_link($next_line_page) . '" title="' . esc_attr__('Next Line', 'W2DC') . '">>></a></li>' ;

		

				echo '</ul>';

				echo '</div>';

			}

		}

	}

}



?>