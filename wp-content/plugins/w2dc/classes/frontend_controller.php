<?php 

class w2dc_frontend_controller {
	public $args;
	public $query;
	public $page_title;
	public $template;
	public $listings = array();
	public $search_form;
	public $google_map;
	public $paginator;
	public $breadcrumbs = array();
	public $base_url;
	public $messages = array();

	public function __construct($args = array()) {
		apply_filters('w2dc_frontend_controller_construct', $this);
	}

	public function processQuery($load_map = true, $map_args = array()) {
		// this is special construction,
		// this needs when we order by any postmeta field, this adds listings to the list with "empty" fields
		if (isset($this->query->query_vars['orderby']) && ($this->query->query_vars['orderby'] == 'meta_value_num' || $this->query->query_vars['orderby'] == 'meta_value') && (!isset($this->query->query_vars['meta_key']) || $this->query->query_vars['meta_key'] != '_order_date')) {
			$args = $this->query->query_vars;

			// there is strange thing - WP adds `taxonomy` and `term_id` args to the root of query vars array
			// this may cause problems
			unset($args['taxonomy']);
			unset($args['term_id']);
			
			$original_posts_per_page = $args['posts_per_page'];

			$ordered_posts_ids = get_posts(array_merge($args, array('fields' => 'ids', 'nopaging' => true)));
			//var_dump($ordered_posts_ids);
			$ordered_max_num_pages = ceil(count($ordered_posts_ids)/$original_posts_per_page) - (int) $ordered_posts_ids;

			$args['paged'] = $args['paged'] - $ordered_max_num_pages;
			$args['orderby'] = 'meta_value_num';
			$args['meta_key'] = '_order_date';
			$args['order'] = 'DESC';
			$args['posts_per_page'] = $original_posts_per_page - $this->query->post_count;
			$all_posts_ids = get_posts(array_merge($args, array('fields' => 'ids', 'nopaging' => true)));
			$all_posts_count = count($all_posts_ids);
			//var_dump($all_posts_count);

			if ($this->query->found_posts) {
				$args['post__not_in'] = array_map('intval', $ordered_posts_ids);
				if (isset($args['post__in']) && is_array($args['post__in']) && $args['post__in']) {
					$args['post__in'] = array_diff($args['post__in'], $args['post__not_in']);
					if (!$args['post__in'])
						$args['posts_per_page'] = 0;
				}
			}

			$unordered_query = new WP_Query($args);

			//var_dump($unordered_query->request);

			if ($args['posts_per_page'])
				$this->query->posts = array_merge($this->query->posts, $unordered_query->posts);

			$this->query->post_count = count($this->query->posts);
			$this->query->found_posts = $all_posts_count;
			$this->query->max_num_pages = ceil($all_posts_count/$original_posts_per_page);
		}

		if ($load_map)
			$this->google_map = new google_maps($map_args);

		while ($this->query->have_posts()) {
			$this->query->the_post();

			$listing = new w2dc_listing;
			$listing->loadListingFromPost(get_post());

			if ($load_map && !$this->google_map->is_ajax_markers_management())
				$this->google_map->collectLocations($listing);
			
			$this->listings[get_the_ID()] = $listing;
		}

		// this is reset is really required after the loop ends 
		wp_reset_postdata();
		
		remove_filter('posts_join', 'join_levels');
		remove_filter('posts_orderby', 'orderby_levels', 1);
	}
	
	public function getQuery() {
		return $this->query;
	}
	
	public function getPageTitle() {
		return $this->page_title;
	}

	public function getBreadCrumbs() {
		return implode(' » ', $this->breadcrumbs);
	}

	public function getBaseUrl() {
		return $this->base_url;
	}
}

/**
 * join levels_relationships and levels tables into the query
 * 
 * */
function join_levels($join = '') {
	global $wpdb;

	$join .= " LEFT JOIN {$wpdb->levels_relationships} AS w2dc_lr ON w2dc_lr.post_id = {$wpdb->posts}.ID ";
	$join .= " LEFT JOIN {$wpdb->levels} AS w2dc_levels ON w2dc_levels.id = w2dc_lr.level_id ";

	return $join;
}

/**
 * sticky and featured listings in the first order
 * 
 */
function orderby_levels($orderby = '') {
	$orderby = " w2dc_levels.sticky DESC, w2dc_levels.featured DESC, " . $orderby;
	return $orderby;
}

/**
 * sticky and featured listings in the first order
 * 
 */
function where_sticky_featured($where = '') {
	$where .= " AND (w2dc_levels.sticky=1 OR w2dc_levels.featured=1)";
	return $where;
}


add_filter('w2dc_order_args', 'w2dc_order_listings', 10, 2);
function w2dc_order_listings($order_args = array(), $defaults = array()) {
	global $w2dc_instance;

	if (isset($_GET['order_by']) && $_GET['order_by']) {
		$order_by = $_GET['order_by'];
		$order = w2dc_getValue($_GET, 'order', 'ASC');
	} else {
		if (isset($defaults['order_by']) && $defaults['order_by']) {
			$order_by = $defaults['order_by'];
			$order = w2dc_getValue($defaults, 'order', 'ASC');
		} else {
			$order_by = 'post_date';
			$order = 'DESC';
		}
	}

	$order_args['order'] = $order;

	if ($order_by == 'post_date') {
		// First of all order by _order_date parameter
		$order_args['orderby'] = 'meta_value_num';
		$order_args['meta_key'] = '_order_date';

		// Do not affect levels weights when already ordering by posts IDs
		if (!isset($order_args['orderby']) || $order_args['orderby'] != 'post__in') {
			add_filter('posts_join', 'join_levels');
			add_filter('posts_orderby', 'orderby_levels', 1);
		}
		
		$w2dc_instance->order_by_date = true;
	} elseif ($order_by == 'title') {
		$order_args['orderby'] = 'title';
	} else
		$order_args = array_merge($order_args, $w2dc_instance->content_fields->getOrderParams($defaults));

	return $order_args;
}

function w2dc_what_search($args) {
	if (isset($_GET['what_search']) && $_GET['what_search'])
		$args['s'] = $_GET['what_search'];

	return $args;
}
add_filter('w2dc_search_args', 'w2dc_what_search');

function w2dc_where_search($args) {
	if (isset($_GET['where_search']) && $_GET['where_search']) {
		$where_search = $_GET['where_search'];

		global $wpdb;
		$results = $wpdb->get_results($wpdb->prepare("SELECT post_id FROM {$wpdb->locations_relationships} WHERE address_line_1 LIKE '%%%s%%' OR address_line_2 LIKE '%%%s%%' OR zip_or_postal_index LIKE '%%%s%%'", $where_search, $where_search, $where_search), ARRAY_A);
		$post_ids = array();
		foreach ($results AS $row)
			$post_ids[] = $row['post_id'];
		if ($post_ids)
			$args['post__in'] = $post_ids;
	}

	return $args;
}
add_filter('w2dc_search_args', 'w2dc_where_search');

function w2dc_locations_search($args, $defaults = array()) {
	$search_location = w2dc_getValue($_GET, 'search_location', w2dc_getValue($defaults, 'location_id'));

	if ($search_location && is_numeric($search_location)) {
		$term_ids = get_terms(W2DC_LOCATIONS_TAX, array('child_of' => $search_location, 'fields' => 'ids', 'hide_empty' => false));
		$term_ids[] = $search_location;

		global $wpdb;
		$results = $wpdb->get_results("SELECT post_id FROM {$wpdb->locations_relationships} WHERE location_id IN (" . implode(', ', $term_ids) . ")", ARRAY_A);
		$post_ids = array();
		foreach ($results AS $row)
			$post_ids[] = $row['post_id'];

		if ($post_ids)
			$args['post__in'] = $post_ids;
		else
			// Do not show any listings
			$args['post__in'] = array(0);
	}
	return $args;
}
add_filter('w2dc_search_args', 'w2dc_locations_search', 10, 2);

function w2dc_base_url_args($args) {
	if (isset($_GET['what_search']) && $_GET['what_search'])
		$args['what_search'] = $_GET['what_search'];
	if (isset($_GET['where_search']) && $_GET['where_search'])
		$args['where_search'] = $_GET['where_search'];
	if (isset($_GET['search_location']) && $_GET['search_location'] && is_numeric($_GET['search_location']))
		$args['search_location'] = $_GET['search_location'];

	return $args;
}
add_filter('w2dc_base_url_args', 'w2dc_base_url_args');

?>