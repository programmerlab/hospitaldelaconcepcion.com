<?php 

class w2dc_directory_controller extends w2dc_frontend_controller {
	public $is_home = false;
	public $is_search = false;
	public $is_single = false;
	public $is_category = false;
	public $is_tag = false;

	public function __construct() {
		global $w2dc_instance;

		if (get_query_var('page'))
			$paged = get_query_var('page');
		elseif (get_query_var('paged'))
			$paged = get_query_var('paged');
		else
			$paged = 1;

		if (get_query_var('listing')) {
			$args = array(
					'post_type' => W2DC_POST_TYPE,
					'post_status' => 'publish',
					'name' => get_query_var('listing'),
					'posts_per_page' => 1,
			);
			$this->query = new WP_Query($args);
			$this->processQuery();

			if (count($this->listings) == 1) {
				$this->is_single = true;
				$this->template = 'frontend/listing_single.tpl.php';
				
				$listings_array = $this->listings;
				$listing = array_shift($listings_array);
				$this->listing = $listing;
				$this->page_title = $listing->title();
				$this->breadcrumbs = array(
						'<a href="' . w2dc_directoryUrl() . '">' . __('Home', 'W2DC') . '</a>',
						$listing->title()
				);
				
				if (get_option('w2dc_listing_contact_form') && $w2dc_instance->action == 'contact')
					$this->contactOwnerAction($listing->post);

				if (get_option('w2dc_listing_contact_form') && defined('WPCF7_VERSION') && get_option('w2dc_listing_contact_form_7'))
					add_filter('wpcf7_form_action_url', array($this, 'w2dc_add_listing_id_to_wpcf7'));
				
				add_filter('language_attributes', array($this, 'add_opengraph_doctype'));
				add_action('wp_head', array($this, 'insert_fb_in_head'), 5);
				if (function_exists('rel_canonical'))
					remove_action('wp_head', 'rel_canonical');
				// replace the default WordPress canonical URL function with your own
				add_action('wp_head', array($this, 'rel_canonical_with_custom_tag_override'));
			} else {
				if ($template = get_404_template()) {
					status_header(404);
					nocache_headers();
				} else
					$template = get_index_template();

				if ($template = apply_filters('template_include', $template))
					include($template);
				exit;
			}
		} elseif ($w2dc_instance->action == 'search') {
			$this->is_search = true;
			$this->template = 'frontend/search.tpl.php';

			if (get_option('w2dc_main_search'))
				$this->search_form = new search_form();

			$default_orderby_args = array('order_by' => get_option('w2dc_default_orderby'), 'order' => get_option('w2dc_default_order'));
			$this->args = $default_orderby_args;
			$order_args = apply_filters('w2dc_order_args', array(), $default_orderby_args);

			$args = array(
					'post_type' => W2DC_POST_TYPE,
					'post_status' => 'publish',
					'meta_query' => array(array('key' => '_listing_status', 'value' => 'active')),
					'posts_per_page' => get_option('w2dc_listings_number_excerpt'),
					'paged' => $paged,
			);
			$args = array_merge($args, $order_args);
			$args = apply_filters('w2dc_search_args', $args);
			$base_url_args = apply_filters('w2dc_base_url_args', array('w2dc_action' => 'search'));
			
			$this->query = new WP_Query($args);
			$this->processQuery(get_option('w2dc_map_on_excerpt'));

			$this->page_title = __('Search results', 'W2DC');
			$this->breadcrumbs = array(
					'<a href="' . w2dc_directoryUrl() . '">' . __('Home', 'W2DC') . '</a>',
					__('Search results', 'W2DC')
			);
			$this->base_url = w2dc_directoryUrl($base_url_args);
		} elseif (get_query_var('category')) {
			if ($category_object = get_term_by('slug', get_query_var('category'), W2DC_CATEGORIES_TAX)) {
				$this->is_category = true;
				$this->category = $category_object;
				
				if (get_option('w2dc_main_search'))
					$this->search_form = new search_form();

				$default_orderby_args = array('order_by' => get_option('w2dc_default_orderby'), 'order' => get_option('w2dc_default_order'));
				$this->args = $default_orderby_args;
				$order_args = apply_filters('w2dc_order_args', array(), $default_orderby_args);

				$args = array(
						'tax_query' => array(
								array(
									'taxonomy' => W2DC_CATEGORIES_TAX,
									'field' => 'slug',
									'terms' => $category_object->slug,
								)
						),
						'post_type' => W2DC_POST_TYPE,
						'post_status' => 'publish',
						'meta_query' => array(array('key' => '_listing_status', 'value' => 'active')),
						'posts_per_page' => get_option('w2dc_listings_number_excerpt'),
						'paged' => $paged
				);
				$args = array_merge($args, $order_args);
	
				$this->query = new WP_Query($args);
				$this->processQuery(get_option('w2dc_map_on_excerpt'));

				$this->template = 'frontend/category.tpl.php';

				$this->page_title = $category_object->name;
				$this->breadcrumbs = array_merge(
						array('<a href="' . w2dc_directoryUrl() . '">' . __('Home', 'W2DC') . '</a>'),
						w2dc_get_term_parents($category_object, W2DC_CATEGORIES_TAX, true, true)
				);
				$this->base_url = get_term_link($category_object, W2DC_CATEGORIES_TAX);
			} else {
				if ($template = get_404_template()) {
					status_header(404);
					nocache_headers();
				} else
					$template = get_index_template();

				if ($template = apply_filters('template_include', $template))
					include($template);
				exit;
			}
		} elseif (get_query_var('tag')) {
			if ($tag_object = get_term_by('slug', get_query_var('tag'), W2DC_TAGS_TAX)) {
				$this->is_tag = true;
				$this->tag = $tag_object;

				if (get_option('w2dc_main_search'))
					$this->search_form = new search_form();

				$default_orderby_args = array('order_by' => get_option('w2dc_default_orderby'), 'order' => get_option('w2dc_default_order'));
				$this->args = $default_orderby_args;
				$order_args = apply_filters('w2dc_order_args', array(), $default_orderby_args);

				$args = array(
						'tax_query' => array(
								array(
										'taxonomy' => W2DC_TAGS_TAX,
										'field' => 'slug',
										'terms' => $tag_object->slug,
								)
						),
						'post_type' => W2DC_POST_TYPE,
						'post_status' => 'publish',
						'meta_query' => array(array('key' => '_listing_status', 'value' => 'active')),
						'posts_per_page' => get_option('w2dc_listings_number_excerpt'),
						'paged' => $paged,
				);
				$args = array_merge($args, $order_args);
	
				$this->query = new WP_Query($args);
				$this->processQuery(get_option('w2dc_map_on_excerpt'));

				$this->template = 'frontend/tag.tpl.php';
	
				$this->page_title = $tag_object->name;
				$this->breadcrumbs = array_merge(
						array('<a href="' . w2dc_directoryUrl() . '">' . __('Home', 'W2DC') . '</a>'),
						w2dc_get_term_parents($tag_object, W2DC_TAGS_TAX, true, true)
				);
				$this->base_url = get_term_link($tag_object, W2DC_TAGS_TAX);
			} else {
				if ($template = get_404_template()) {
					status_header(404);
					nocache_headers();
				} else
					$template = get_index_template();

				if ($template = apply_filters('template_include', $template))
					include($template);
				exit;
			}
		} elseif ($w2dc_instance->action == 'myfavourites') {
			if (!$favourites = checkQuickList())
				$favourites = array(0);
			$args = array(
					'post__in' => $favourites,
					'post_type' => W2DC_POST_TYPE,
					'post_status' => 'publish',
					'meta_query' => array(array('key' => '_listing_status', 'value' => 'active')),
					'posts_per_page' => get_option('w2dc_listings_number_excerpt'),
					'paged' => $paged,
			);
			$this->query = new WP_Query($args);
			$this->processQuery(get_option('w2dc_map_on_excerpt'));

			$this->template = 'frontend/favourites.tpl.php';
			$this->page_title = __('My favourites', 'W2DC');
			$this->breadcrumbs = array('<a href="' . w2dc_directoryUrl() . '">' . __('Home', 'W2DC') . '</a>', __('My favourites', 'W2DC'));
		} elseif (!$w2dc_instance->action) {
			$this->is_home = true;

			if (get_option('w2dc_main_search'))
				$this->search_form = new search_form();

			$default_orderby_args = array('order_by' => get_option('w2dc_default_orderby'), 'order' => get_option('w2dc_default_order'));
			$this->args = $default_orderby_args;
			$order_args = apply_filters('w2dc_order_args', array(), $default_orderby_args);

			$args = array(
					'post_type' => W2DC_POST_TYPE,
					'post_status' => 'publish',
					'meta_query' => array(array('key' => '_listing_status', 'value' => 'active')),
					'posts_per_page' => get_option('w2dc_listings_number_index'),
					'paged' => $paged,
			);
			$args = array_merge($args, $order_args);
			
			$this->template = 'frontend/index.tpl.php';

			$this->query = new WP_Query($args);
			$this->processQuery(get_option('w2dc_map_on_index'));
			$this->base_url = w2dc_directoryUrl();
		}
		
		$this->hide_order = !(get_option('w2dc_show_orderby_links'));

		add_action('get_header', array($this, 'configure_seo_filters'), 2);
		
		apply_filters('w2dc_frontend_controller_construct', $this);
	}

	// Add listing ID to query string while rendering Contact Form 7
	public function w2dc_add_listing_id_to_wpcf7($url) {
		if ($this->is_single)
			$url = add_query_arg('listing_id', $this->listing->post->ID, $url);
		
		return $url;
	}

	public function contactOwnerAction($post) {
		$validation = new form_validation;
		if (!is_user_logged_in()) {
			$validation->set_rules('contact_name', __('Contact name', 'W2DC'), 'required');
			$validation->set_rules('contact_email', __('Contact email', 'W2DC'), 'required|valid_email');
		}
		$validation->set_rules('contact_message', __('Your message', 'W2DC'), 'required|max_length[1500]');
		if ($validation->run()) {
			if (!is_user_logged_in()) {
				$contact_name = $validation->result_array('contact_name');
				$contact_email = $validation->result_array('contact_email');
			} else {
				$current_user = wp_get_current_user();
				$contact_name = $current_user->display_name;
				$contact_email = $current_user->user_email;
			}
			$contact_message = $validation->result_array('contact_message');

			if (w2dc_is_recaptcha_passed()) {
				$listing_owner = get_userdata($post->post_author);

				/* $headers =  "MIME-Version: 1.0\r\n" .
						"From: $contact_name <$contact_email>\r\n" .
						"Reply-To: $contact_email\r\n" .
						"Content-Type: text/plain; charset=\"" . get_option('blog_charset') . "\"\r\n"; */
				$headers[] = "From: $contact_name <$contact_email>";
				$headers[] = "Reply-To: $contact_email";

				global $w2dc_instance;
				$subject = "[" . get_option('blogname') . "] " . sprintf(__('%s contacted you about your listing', 'W2DC'), $contact_name);

				$body = w2dc_renderTemplate('emails/contact_form.tpl.php',
						array(
								'contact_name' => $contact_name,
								'contact_email' => $contact_email,
								'contact_message' => $contact_message,
								'listing_title' => get_the_title($post),
								'listing_url' => get_permalink($post->ID)
						), true);

				if (wp_mail($listing_owner->user_email, $subject, $body, $headers))
					w2dc_addMessage(__('You message was sent successfully!', 'W2DC'));
				else
					w2dc_addMessage(__('An error occurred and your message wasn\'t sent!', 'W2DC'), 'error');
			} else {
				w2dc_addMessage(__('Verification code wasn\'t entered correctly!', 'W2DC'), 'error');
			}
		} else {
			w2dc_addMessage($validation->error_string(), 'error');
		}
	}
	
	public function configure_seo_filters() {
		if ($this->query) {
			add_filter('wp_title', array($this, 'page_title'), 10, 2);
			if (defined('WPSEO_VERSION')) {
				global $wpseo_front;

				// real number of page for WP SEO plugin
				if ($this->query) {
					global $wp_query;
					$wp_query->max_num_pages = $this->query->max_num_pages;
				}

				// remove force_rewrite option of WP SEO plugin
				remove_action('get_header', array(&$wpseo_front, 'force_rewrite_output_buffer'));
				remove_action('wp_footer', array(&$wpseo_front, 'flush_cache'));
				
				remove_filter('wp_title', array(&$wpseo_front, 'title'), 15, 3);
				remove_action('wp_head', array(&$wpseo_front, 'head'), 1, 1);
	
				add_action('wp_head', array( $this, 'page_meta'));
			}
		}
	}
	
	public function page_meta() {
		global $wpseo_front;
		if ($this->is_single) {
			global $post;
			$saved_page = $post;
			$post = get_post($this->listing->post->ID);
	
			$wpseo_front->metadesc();
			$wpseo_front->metakeywords();
	
			$post = $saved_page;
		} elseif ($this->is_category) {
			if (version_compare(WPSEO_VERSION, '1.5.0', '<'))
				$metadesc = wpseo_get_term_meta($this->category, $this->category->taxonomy, 'desc');
			else
				$metadesc = WPSEO_Taxonomy_Meta::get_term_meta($this->category, $this->category->taxonomy, 'desc');

			if (!$metadesc && isset($wpseo_front->options['metadesc-' . $this->category->taxonomy]))
				$metadesc = wpseo_replace_vars($wpseo_front->options['metadesc-' . $this->category->taxonomy], (array) $this->category );
			$metadesc = apply_filters('wpseo_metadesc', trim($metadesc));
			echo '<meta name="description" content="' . esc_attr(strip_tags(stripslashes($metadesc))) . '"/>' . "\n";
		} elseif ($this->is_tag) {
			if (version_compare(WPSEO_VERSION, '1.5.0', '<'))
				$metadesc = wpseo_get_term_meta($this->tag, $this->tag->taxonomy, 'desc');
			else
				$metadesc = WPSEO_Taxonomy_Meta::get_term_meta($this->tag, $this->tag->taxonomy, 'desc');

			if (!$metadesc && isset($wpseo_front->options['metadesc-' . $this->tag->taxonomy]))
				$metadesc = wpseo_replace_vars($wpseo_front->options['metadesc-' . $this->tag->taxonomy], (array) $this->tag );
			$metadesc = apply_filters('wpseo_metadesc', trim($metadesc));
			echo '<meta name="description" content="' . esc_attr(strip_tags(stripslashes($metadesc))) . '"/>' . "\n";
		} elseif ($this->is_home) {
			$wpseo_front->metadesc();
			$wpseo_front->metakeywords();
		}
	}
	
	public function page_title($title, $separator = '|') {
		if (defined('WPSEO_VERSION')) {
			global $wpseo_front, $w2dc_instance;
			if ($this->is_single) {
				$title = $wpseo_front->get_content_title(get_post($this->listing->post->ID));
				return esc_html(strip_tags(stripslashes(apply_filters('wpseo_title', $title))));
			} elseif ($this->is_category) {
				if (version_compare(WPSEO_VERSION, '1.5.0', '<'))
					$title = trim(wpseo_get_term_meta($this->category, $this->category->taxonomy, 'title'));
				else
					$title = trim(WPSEO_Taxonomy_Meta::get_term_meta($this->category, $this->category->taxonomy, 'title'));

				if (!empty($title))
					return wpseo_replace_vars($title, (array)$this->category);
				return $wpseo_front->get_title_from_options('title-tax-' . $this->category->taxonomy, $this->category);
			} elseif ($this->is_tag) {
				if (version_compare(WPSEO_VERSION, '1.5.0', '<'))
					$title = trim(wpseo_get_term_meta($this->tag, $this->tag->taxonomy, 'title'));
				else
					$title = trim(WPSEO_Taxonomy_Meta::get_term_meta($this->tag, $this->tag->taxonomy, 'title'));

				if (!empty($title))
					return wpseo_replace_vars($title, (array)$this->tag);
				return $wpseo_front->get_title_from_options('title-' . $this->tag->taxonomy, $this->tag);
			} elseif ($this->is_home) {
				//$page = get_post($w2dc_instance->index_page_id);
				//return $wpseo_front->get_title_from_options('title-' . W2DC_POST_TYPE, (array) $page);
				return $wpseo_front->get_content_title();
			}

			if ($this->getPageTitle())
				$title = esc_html(strip_tags(stripslashes($this->getPageTitle()))) . ' ';
			return $title . wpseo_replace_vars('%%sep%% %%sitename%%', array());
		} else {
			if ($this->getPageTitle()) {
				if (get_option('w2dc_directory_title'))
					if ($this->getPageTitle() == get_option('w2dc_directory_title'))
					$directory_title = '';
				else
					$directory_title = get_option('w2dc_directory_title');
				else
					$directory_title = '';
	
				if ($title != __('Directory listings', 'W2DC') . ' ' . $separator . ' ')
					return $this->getPageTitle() . ' ' . $separator . ' ' . $directory_title;
				else
					return $directory_title;
			}
		}
	
		return $title;
	}

	// rewrite canonical URL
	public function rel_canonical_with_custom_tag_override() {
		echo '<link rel="canonical" href="' . get_permalink($this->listing->post->ID) . '" />
';
	}
	
	// Adding the Open Graph in the Language Attributes
	public function add_opengraph_doctype($output) {
		return $output . ' xmlns:og="http://opengraphprotocol.org/schema/" xmlns:fb="http://www.facebook.com/2008/fbml"';
	}
	
	// Lets add Open Graph Meta Info
	public function insert_fb_in_head() {
		echo '<meta property="og:type" content="article" />
';
		echo '<meta property="og:locale" content="en_US" />
';
		echo '<meta property="og:title" content="' . esc_attr($this->listing->title()) . '" />
';
		if ($this->listing->post->post_excerpt)
			$excerpt = $this->listing->post->post_excerpt;
		else
			$excerpt = $this->listing->getExcerptFromContent();
		echo '<meta property="og:description" content="' . esc_attr($excerpt) . '" />
';		
		echo '<meta property="og:url" content="' . get_permalink($this->listing->post->ID) . '" />
';
		echo '<meta property="og:site_name" content="' . get_option('w2dc_directory_title') . '" />
';
		if ($this->listing->logo_image) {
			$thumbnail_src = $src_full = wp_get_attachment_image_src($this->listing->logo_image, 'medium');
			echo '<meta property="og:image" content="' . esc_attr($thumbnail_src[0]) . '" />
';
		}
	}

	public function display() {
		$output =  w2dc_renderTemplate($this->template, array('frontend_controller' => $this), true);
		wp_reset_postdata();

		return $output;
	}
}

if (defined('WPCF7_VERSION')) {
	if (version_compare(WPCF7_VERSION, '3.9', '<')) {
		if (get_option('w2dc_listing_contact_form') && defined('WPCF7_VERSION') && get_option('w2dc_listing_contact_form_7'))
			add_action('wpcf7_before_send_mail', 'w2dc_wpcf7_handle_email');
		
		function w2dc_wpcf7_handle_email(&$WPCF7_ContactForm = null) {
			if (isset($_GET['listing_id'])) {
				$post = get_post($_GET['listing_id']);
		
				if ($post && isset($_POST['_wpcf7']) && preg_match_all('/'.get_shortcode_regex().'/s', get_option('w2dc_listing_contact_form_7'), $matches))
				foreach ($matches[2] AS $key=>$shortcode) {
					if ($shortcode == 'contact-form-7') {
						if ($attrs = shortcode_parse_atts($matches[3][$key]))
							if (isset($attrs['id']) && $attrs['id'] == $_POST['_wpcf7']) {
								if (($listing_owner = get_userdata($post->post_author)) && $listing_owner->user_email)
									$WPCF7_ContactForm->mail['recipient'] = $listing_owner->user_email;
							}
					}
				}
			}
		}
	} else {
		if (get_option('w2dc_listing_contact_form') && defined('WPCF7_VERSION') && get_option('w2dc_listing_contact_form_7'))
			add_filter('wpcf7_mail_components', 'w2dc_wpcf7_handle_email');
		
		function w2dc_wpcf7_handle_email($WPCF7_ContactForm) {
			if (isset($_GET['listing_id'])) {
				$post = get_post($_GET['listing_id']);
		
				if ($post && isset($_POST['_wpcf7']) && preg_match_all('/'.get_shortcode_regex().'/s', get_option('w2dc_listing_contact_form_7'), $matches))
				foreach ($matches[2] AS $key=>$shortcode) {
					if ($shortcode == 'contact-form-7') {
						if ($attrs = shortcode_parse_atts($matches[3][$key]))
							if (isset($attrs['id']) && $attrs['id'] == $_POST['_wpcf7']) {
								if (($listing_owner = get_userdata($post->post_author)) && $listing_owner->user_email)
									$WPCF7_ContactForm['recipient'] = $listing_owner->user_email;
							}
					}
				}
			}
			return $WPCF7_ContactForm;
		}
	}
}

?>