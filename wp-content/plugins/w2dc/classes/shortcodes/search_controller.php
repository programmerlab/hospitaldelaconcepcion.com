<?php 

class w2dc_search_controller extends w2dc_frontend_controller {

	public function __construct($args) {
		$this->search_form = new search_form();
		
		apply_filters('w2dc_frontend_controller_construct', $this);
	}

	public function display() {
		ob_start();
		$this->search_form->display();
		$output = ob_get_clean();

		return $output;
	}
}

?>