<?php 

class w2dc_categories_controller extends w2dc_frontend_controller {

	public function __construct($args) {
		global $w2dc_instance;

		$shortcode_atts = array_merge(array(
				'parent' => 0,
				'depth' => 1,
				'columns' => 2,
				'count' => 1,
				'subcats' => 0,
				'levels' => array(),
		), $args);
		$this->args = $shortcode_atts;
		
		if (isset($this->args['levels']) && !is_array($this->args['levels']))
			if ($levels = array_filter(explode(',', $this->args['levels']), 'trim'))
				$this->args['levels'] = $levels;

		apply_filters('w2dc_frontend_controller_construct', $this);
	}

	public function display() {
		ob_start();
		w2dc_renderAllCategories($this->args['parent'], $this->args['depth'], $this->args['columns'], $this->args['count'], $this->args['subcats'], $this->args['levels']);
		$output = ob_get_clean();

		return $output;
	}
}

?>