<?php 

class w2dc_listing {
	public $post;
	public $level;
	public $expiration_date;
	public $order_date;
	public $listing_created = false;
	public $status; // active, expired, unpaid, stopped
	public $categories = array();
	public $locations = array();
	public $content_fields = array();
	public $logo_file;
	public $map_zoom;
	public $logo_image;
	public $images = array();
	public $videos = array();
	public $map;

	public function __construct($level_id = null) {
		if ($level_id) {
			// New listing
			$this->setLevelByID($level_id);
		}
	}
	
	// Load existed listing
	public function loadListingFromPost($post) {
		if (is_object($post))
			$this->post = $post;
		elseif (is_numeric($post))
			if (!($this->post = get_post($post)))
				return false;

		if ($this->setLevelByPostId()) {
			$this->setMetaInformation();
			$this->setLocations();
			$this->setContentFields();
			$this->setMapZoom();
			$this->setMedia();
			
			apply_filters('w2dc_listing_loading', $this);
		}
		return true;
	}

	public function setLevelByID($level_id) {
		global $w2dc_instance;

		$levels = $w2dc_instance->levels;
		$this->level = $levels->getLevelById($level_id);
	}
	
	public function setMetaInformation() {
		if (!$this->level->eternal_active_period)
			$this->expiration_date = get_post_meta($this->post->ID, '_expiration_date', true);

		$this->order_date = get_post_meta($this->post->ID, '_order_date', true);

		$this->status = get_post_meta($this->post->ID, '_listing_status', true);

		$this->listing_created = get_post_meta($this->post->ID, '_listing_created', true);

		return $this->expiration_date;
	}

	public function setLevelByPostId($post_id = null) {
		global $w2dc_instance, $wpdb;

		if (!$post_id)
			$post_id = $this->post->ID;

		$levels = $w2dc_instance->levels;

		if ($level_id = $wpdb->get_var("SELECT level_id FROM {$wpdb->levels_relationships} WHERE post_id=" . $post_id))
			return $this->level = $levels->levels_array[$level_id];
		return $this->level;
	}

	public function setLocations() {
		global $wpdb;

		$results = $wpdb->get_results("SELECT * FROM {$wpdb->locations_relationships} WHERE post_id=".$this->post->ID, ARRAY_A);
		
		foreach ($results AS $row) {
			if ($row['location_id'] || $row['map_coords_1'] != '0.000000' || $row['map_coords_2'] != '0.000000' || $row['address_line_1'] || $row['zip_or_postal_index']) {
				$location = new w2dc_location($this->post->ID);
				$location_settings = array(
						'id' => $row['id'],
						'selected_location' => $row['location_id'],
						'address_line_1' => $row['address_line_1'],
						'address_line_2' => $row['address_line_2'],
						'zip_or_postal_index' => $row['zip_or_postal_index'],
				);
				if ($this->level->google_map) {
					$location_settings['manual_coords'] = w2dc_getValue($row, 'manual_coords');
					$location_settings['map_coords_1'] = w2dc_getValue($row, 'map_coords_1');
					$location_settings['map_coords_2'] = w2dc_getValue($row, 'map_coords_2');
					if ($this->level->google_map_markers)
						$location_settings['map_icon_file'] = w2dc_getValue($row, 'map_icon_file');
				}
				$location->createLocationFromArray($location_settings);
				
				$this->locations[] = $location;
			}
		}
	}

	public function setMapZoom() {
		if (!$this->map_zoom = get_post_meta($this->post->ID, '_map_zoom', true))
			$this->map_zoom = get_option('w2dc_default_map_zoom');
	}

	public function setContentFields() {
		global $w2dc_instance;

		$post_categories_ids = wp_get_post_terms($this->post->ID, W2DC_CATEGORIES_TAX, array('fields' => 'ids'));
		$this->content_fields = $w2dc_instance->content_fields->loadValues($this->post->ID, $post_categories_ids);
	}
	
	public function setMedia() {
		if ($this->level->images_number) {
			if ($images = get_post_meta($this->post->ID, '_attached_image')) {
				foreach ($images AS $image_id)
					$this->images[$image_id] = get_post($image_id, ARRAY_A);

				if (($logo_id = (int)get_post_meta($this->post->ID, '_attached_image_as_logo', true)) && in_array($logo_id, array_keys($this->images)))
					$this->logo_image = $logo_id;
				else
					$this->logo_image = array_shift(array_keys($this->images));
			} else
				$this->images = array();
		}
		
		if ($this->level->videos_number) {
			if ($videos = get_post_meta($this->post->ID, '_attached_video_id')) {
				$videos_captions = get_post_meta($this->post->ID, '_attached_video_caption');
				foreach ($videos AS $key=>$video) {
					if (isset($videos_captions[$key]))
						$caption = $videos_captions[$key];
					else
						$caption = '';
					$this->videos[] = array('caption' => $caption, 'id' => $video);
				}
			}
		}
	}
	
	public function getContentField($field_id) {
		if (isset($this->content_fields[$field_id]))
			return $this->content_fields[$field_id];
	}

	public function display($is_single = false) {
		w2dc_renderTemplate('frontend/listing.tpl.php', array('listing' => $this, 'is_single' => $is_single));
	}
	
	public function renderContentFields($is_single = true) {
		foreach ($this->content_fields AS $content_field) {
			if ((!$is_single && $content_field->on_exerpt_page) || ($is_single && $content_field->on_listing_page))
				$content_field->renderOutput($this);
		}
	}
	
	public function isMap() {
		$this->map = new google_maps;
		return $this->map->collectLocations($this);
	}
	
	public function renderMap($show_directions = true, $static_image = false, $enable_radius_cycle = false, $enable_clusters = false, $show_summary_button = false, $show_readmore_button = false) {
		$this->map->display($show_directions, $static_image, $enable_radius_cycle, $enable_clusters, $show_summary_button, $show_readmore_button);
	}
	
	public function title() {
		return get_the_title($this->post);
	}

	public function processRaiseUp($invoke_hooks = true) {
		if ($this->level->raiseup_enabled) {
			$continue = true;
			if ($invoke_hooks)
				$continue = apply_filters('w2dc_listing_raiseup', $continue, $this);

			if ($continue) {
				$listings_ids = array();

				// adapted for WPML
				if (function_exists('icl_object_id')) {
					global $sitepress;
					$trid = $sitepress->get_element_trid($this->post->ID, 'post_' . W2DC_POST_TYPE);
					$translations = $sitepress->get_element_translations($trid);
					foreach ($translations AS $lang=>$translation)
						$listings_ids[] = $translation->element_id;
				} else
					$listings_ids[] = $this->post->ID;

				foreach ($listings_ids AS $listing_id)
					update_post_meta($listing_id, '_order_date', time());

				return true;
			}
		}
	}

	public function processActivate($invoke_hooks = true) {
		$continue = true;
		if ($invoke_hooks)
			$continue = apply_filters('w2dc_listing_renew', $continue, $this);
		
		if ($continue) {
			$listings = array();

			// adapted for WPML
			if (function_exists('icl_object_id')) {
				global $sitepress;
				$trid = $sitepress->get_element_trid($this->post->ID, 'post_' . W2DC_POST_TYPE);
				$translations = $sitepress->get_element_translations($trid);
				foreach ($translations AS $lang=>$translation) {
					$listing = new w2dc_listing();
					$listing->loadListingFromPost($translation->element_id);
					$listings[] = $listing;
				}
			} else
				$listings[] = $this;

			foreach ($listings AS $listing) {
				if (!$listing->level->eternal_active_period) {
					$expiration_date = w2dc_sumDates(time(), $listing->level->active_days, $listing->level->active_months, $listing->level->active_years);
					update_post_meta($listing->post->ID, '_expiration_date', $expiration_date);
				}
				update_post_meta($listing->post->ID, '_order_date', time());
				update_post_meta($listing->post->ID, '_listing_status', 'active');
				
				delete_post_meta($listing->post->ID, '_expiration_notification_sent');
				delete_post_meta($listing->post->ID, '_preexpiration_notification_sent');
		
				wp_update_post(array('ID' => $listing->post->ID, 'post_status' => 'publish'));
			}
			return true;
		}
	}
	
	public function saveExpirationDate($date_array) {
		$new_tmstmp = $date_array['expiration_date_tmstmp'] + $date_array['expiration_date_hour']*3600 + $date_array['expiration_date_minute']*60;
		
		$listings_ids = array();
		
		// adapted for WPML
		if (function_exists('icl_object_id')) {
			global $sitepress;
			$trid = $sitepress->get_element_trid($this->post->ID, 'post_' . W2DC_POST_TYPE);
			$translations = $sitepress->get_element_translations($trid);
			foreach ($translations AS $lang=>$translation)
				$listings_ids[] = $translation->element_id;
		} else
			$listings_ids[] = $this->post->ID;
		
		foreach ($listings_ids AS $listing_id)
			if ($new_tmstmp != get_post_meta($listing_id, '_expiration_date', true))
				update_post_meta($listing_id, '_expiration_date', $new_tmstmp);

		return true;
	}
	
	public function changeLevel($new_level_id, $invoke_hooks = true) {
		global $w2dc_instance, $wpdb;
		
		if (isset($w2dc_instance->levels->levels_array[$new_level_id]) && !$this->level->upgrade_meta[$new_level_id]['disabled']) {
			$listings = array();
			
			// adapted for WPML
			if (function_exists('icl_object_id')) {
				global $sitepress;
				$trid = $sitepress->get_element_trid($this->post->ID, 'post_' . W2DC_POST_TYPE);
				$translations = $sitepress->get_element_translations($trid);
				foreach ($translations AS $lang=>$translation) {
					$listing = new w2dc_listing();
					$listing->loadListingFromPost($translation->element_id);
					$listings[] = $listing;
				}
			} else
				$listings[] = $this;

			foreach ($listings AS $listing) {
				update_post_meta($listing->post->ID, '_old_level_id', $listing->level->id);
				update_post_meta($listing->post->ID, '_new_level_id', $new_level_id);
			}

			$continue = true;
			if ($invoke_hooks)
				$continue = apply_filters('w2dc_listing_upgrade', $continue, $this);
			
			if ($continue) {
				foreach ($listings AS $listing) {
					if ($wpdb->query("UPDATE {$wpdb->levels_relationships} SET level_id=" . $new_level_id . "  WHERE post_id=" . $listing->post->ID)) {
						if ($this->level->upgrade_meta[$new_level_id]['raiseup'])
							update_post_meta($listing->post->ID, '_order_date', time());
	
						$listing->setLevelByPostId($listing->post->ID);
	
						//  If new level has an option of limited active period - expiration date of listing will be reassigned automatically
						if (!$listing->level->eternal_active_period) {
							$expiration_date = w2dc_sumDates(time(), $listing->level->active_days, $listing->level->active_months, $listing->level->active_years);
							update_post_meta($listing->post->ID, '_expiration_date', $expiration_date);
						}
					}
				}
				return true;
			}
		}
	}

	/**
	 * Load existed listing especially for map info window
	 * 
	 * @param $post is required and must be object
	 */
	public function loadListingForMap($post) {
		$this->post = $post;
	
		if ($this->setLevelByPostId()) {
			$this->setLocations();
			$this->setMapZoom();
			$this->setLogoImage();
				
			apply_filters('w2dc_listing_map_loading', $this);
		}
		return true;
	}

	/**
	 * Load existed listing especially for AJAX map - set only locations
	 * 
	 * @param $post is required and must be object
	 */
	public function loadListingForAjaxMap($post) {
		$this->post = $post;
	
		if ($this->setLevelByPostId())
			$this->setLocations();

		return true;
	}
	
	public function setLogoImage() {
		if ($this->level->images_number) {
			if ($logo_id = (int)get_post_meta($this->post->ID, '_attached_image_as_logo', true))
				$this->logo_image = $logo_id;
			else {
				$images = get_post_meta($this->post->ID, '_attached_image');
				$this->logo_image = array_shift($images);
			}
		}
	}
	
	public function setMapContentFields($map_content_fields, $location) {
		global $w2dc_instance;
	
		$post_categories_ids = wp_get_post_terms($this->post->ID, W2DC_CATEGORIES_TAX, array('fields' => 'ids'));
		$content_fields_output = array();
		
		foreach($map_content_fields AS $field_slug=>$content_field) {
			// is it native content field
			if (is_a($content_field, 'w2dc_content_field')) {
				if (!$content_field->isCategories() || $content_field->categories === array() || array_intersect($content_field->categories, $post_categories_ids)) {
					$content_field->loadValue($this->post->ID);
					$content_fields_output[] = $content_field->renderOutputForMap($location, $this);
				} else 
					$content_fields_output[] = null;
			} else
				$content_fields_output[] = apply_filters('w2dc_map_info_window_fields_values', $content_field, $field_slug, $this);
		}

		return $content_fields_output;
	}
	
	public function getExcerptFromContent($words_length = 35) {
		$the_excerpt = strip_tags(strip_shortcodes($this->post->post_content));
		$words = explode(' ', $the_excerpt, $words_length + 1);
		if (count($words) > $words_length) {
			array_pop($words);
			array_push($words, '…');
			$the_excerpt = implode(' ', $words);
		}
		return $the_excerpt;
	}
}

?>