<?php

class search_form {
	
	public function __construct() {
	}

	public function display() {
		global $w2dc_instance;

		// random ID needed because there may be more than 1 search form on one page
		$random_id = generateRandomVal();
		
		$search_url = ($w2dc_instance->index_page_url) ? w2dc_directoryUrl() : home_url();

		w2dc_renderTemplate('search_form.tpl.php', array('random_id' => $random_id, 'search_url' => $search_url));
	}
}
?>