<?php

function loadMarkersSizes() {
	if (!defined('W2DC_THEME_MODE')) {
		define('W2DC_MARKER_IMAGE_WIDTH', 32);
		define('W2DC_MARKER_IMAGE_HEIGHT', 37);
		define('W2DC_MARKER_ANCHOR_X', 16);
		define('W2DC_MARKER_ANCHOR_Y', 37);
		define('W2DC_INFOWINDOW_WIDTH', 300);
		define('W2DC_INFOWINDOW_OFFSET', -50);

		define('W2DC_INFOWINDOW_LOGO_WIDTH', 80);
		define('W2DC_INFOWINDOW_LOGO_HEIGHT', 80);
	}
}
add_action('init', 'loadMarkersSizes', 0);

class google_maps {
	public $args;
	public $unique_map_id;
	
	public $map_zoom;
	public $locations_array = array();
	public $locations_option_array = array();

	public static $map_content_fields;

	public function __construct($args = array()) {
		global $w2dc_instance;

		$this->args = $args;

		$this->unique_map_id = rand(1000, 100000);

		if (is_null(self::$map_content_fields)) {
			self::$map_content_fields = $w2dc_instance->content_fields->getMapContentFields();
		}
	}

	public function collectLocations($listing) {
		if (count($listing->locations) == 1)
			$this->map_zoom = $listing->map_zoom;
		
		foreach ($listing->locations AS $location) {
			if ($location->map_coords_1 != '0.000000' || $location->map_coords_2 != '0.000000') {
				$logo_image = '';
				if ($listing->logo_image) {
					$src = wp_get_attachment_image_src($listing->logo_image, array(W2DC_INFOWINDOW_LOGO_WIDTH, W2DC_INFOWINDOW_LOGO_HEIGHT));
					$logo_image = $src[0];
				}

				$listing_link = '';
				if (get_option('w2dc_listings_own_page'))
					$listing_link = get_permalink($listing->post->ID);

				if (self::$map_content_fields)
					$content_fields_output = $listing->setMapContentFields(self::$map_content_fields, $location);
				else 
					$content_fields_output = '';

				$this->locations_array[] = $location;
				$this->locations_option_array[] = array(
						$location->id,
						$location->map_coords_1,
						$location->map_coords_2,
						$location->map_icon_file,
						$listing->map_zoom,
						get_the_title(),
						$logo_image,
						$listing_link,
						$content_fields_output,
						'post-' . $listing->post->ID,
				);
			}
		}
		if ($this->locations_option_array)
			return true;
		else
			return false;
	}
	
	public function collectLocationsForAjax($listing) {	
		foreach ($listing->locations AS $location) {
			if ($location->map_coords_1 != '0.000000' || $location->map_coords_2 != '0.000000') {
				$this->locations_array[] = $location;
				$this->locations_option_array[] = array(
						$location->id,
						$location->map_coords_1,
						$location->map_coords_2,
						$location->map_icon_file,
						null,
						null,
						null,
						null,
						null,
						null,
				);
			}
		}
		if ($this->locations_option_array)
			return true;
		else
			return false;
	}

	public function display($show_directions = true, $static_image = false, $enable_radius_cycle = true, $enable_clusters = true, $show_summary_button = true, $show_readmore_button = true, $width = false, $height = false) {
		if ($this->locations_option_array || $this->is_ajax_markers_management()) {
			$locations_options = json_encode($this->locations_option_array);
			$map_args = json_encode($this->args);
			w2dc_renderTemplate('google_map.tpl.php',
					array(
							'locations_options' => $locations_options,
							'locations_array' => $this->locations_array,
							'show_directions' => $show_directions,
							'static_image' => $static_image,
							'enable_radius_cycle' => $enable_radius_cycle,
							'enable_clusters' => $enable_clusters,
							'map_zoom' => $this->map_zoom,
							'show_summary_button' => $show_summary_button,
							'show_readmore_button' => $show_readmore_button,
							'width' => $width,
							'height' => $height,
							'unique_map_id' => $this->unique_map_id,
							'map_args' => $map_args
					));
		}
	}
	
	public function is_ajax_markers_management() {
		if (isset($this->args['ajax_loading']) && $this->args['ajax_loading'] && ((isset($this->args['start_address']) && $this->args['start_address']) || ((isset($this->args['start_latitude']) && $this->args['start_latitude']) && (isset($this->args['start_longitude']) && $this->args['start_longitude']))))
			return true;
	}
}

?>