<script>
	jQuery(document).ready(function($) {
		var locations_number = <?php echo $listing->level->locations_number; ?>;

		<?php if ($listing->level->google_map && $listing->level->google_map_markers): ?>
		var map_icon_file_input;
		jQuery(document).on("click", ".select_map_icon", function() {
			map_icon_file_input = $(this).parent().find('.map_icon_file');

			var dialog = $('<div id="select_map_icon_dialog"></div>').dialog({
				width: 750,
				height: 620,
				modal: true,
				resizable: false,
				draggable: false,
				title: '<?php echo esc_js(__('Select map marker icon', 'W2DC-ELOCATIONS')); ?>',
				open: function() {
					ajax_loader_show();
					$.ajax({
						type: "POST",
						url: js_objects.ajaxurl,
						data: {'action': 'select_map_icon'},
						dataType: 'html',
						success: function(response_from_the_action_function){
							if (response_from_the_action_function != 0) {
								$('#select_map_icon_dialog').html(response_from_the_action_function);
								if (map_icon_file_input.val())
									$(".w2dc_icon[icon_file='"+map_icon_file_input.val()+"']").addClass("selected_icon");
							}
						},
						complete: function() {
							ajax_loader_hide();
						}
					});
					jQuery(document).on("click", ".ui-widget-overlay", function() { $('#select_map_icon_dialog').remove(); });
				},
				close: function() {
					$('#select_map_icon_dialog').remove();
				}
			});
		});
		jQuery(document).on("click", ".w2dc_icon", function() {
			$(".selected_icon").removeClass("selected_icon");
			if (map_icon_file_input) {
				map_icon_file_input.val($(this).attr('icon_file'));
				map_icon_file_input = false;
				$(this).addClass("selected_icon");
				$('#select_map_icon_dialog').remove();
				generateMap();
			}
		});
		jQuery(document).on("click", "#reset_icon", function() {
			if (map_icon_file_input) {
				$(".selected_icon").removeClass("selected_icon");
				map_icon_file_input.val('');
				map_icon_file_input = false;
				$('#select_map_icon_dialog').remove();
				generateMap();
			}
		});
		<?php endif; ?>
		
		$(".add_address").click(function() {
			ajax_loader_show();
			$.ajax({
				type: "POST",
				url: js_objects.ajaxurl,
				data: {'action': 'add_location_in_metabox', 'post_id': <?php echo $listing->post->ID; ?>},
				success: function(response_from_the_action_function){
					if (response_from_the_action_function != 0) {
						$("#locations_wrapper").append(response_from_the_action_function);
						$(".delete_location").show();
						if (locations_number == $(".location_in_metabox").length)
							$(".add_address").hide();
					}
				},
				complete: function() {
					ajax_loader_hide();
				}
			});
		});
		jQuery(document).on("click", ".delete_location", function() {
			$(this).parent().remove();
			if ($(".location_in_metabox").length == 1)
				$(".delete_location").hide();

			<?php if ($listing->level->google_map): ?>
			generateMap();
			<?php endif; ?>

			if (locations_number > $(".location_in_metabox").length)
				$(".add_address").show();
		});

		jQuery(document).on("click", ".manual_coords", function() {
        	if ($(this).is(":checked"))
        		$(this).parent().find(".manual_coords_block").show(200);
        	else
        		$(this).parent().find(".manual_coords_block").hide(200);
        });

        if (locations_number > $(".location_in_metabox").length)
			$(".add_address").show();
	});
</script>

<div class="locations_metabox">
	<div id="locations_wrapper">
		<?php
		if ($listing->locations)
			foreach ($listing->locations AS $location)
				w2dc_renderTemplate(array(W2DC_ELOCATIONS_TEMPLATES_PATH, 'locations_in_metabox.tpl.php'), array('listing' => $listing, 'location' => $location, 'locations_levels' => $locations_levels, 'delete_location_link' => (count($listing->locations) > 1) ? true : false));
		else
			w2dc_renderTemplate(array(W2DC_ELOCATIONS_TEMPLATES_PATH, 'locations_in_metabox.tpl.php'), array('listing' => $listing, 'location' => new w2dc_location, 'locations_levels' => $locations_levels, 'delete_location_link' => false));
		?>
	</div>
	
	<?php if ($listing->level->locations_number > 1): ?>
	<a class="add_address" style="display: none;" href="javascript: void(0);"><img src="<?php echo W2DC_RESOURCES_URL; ?>images/map_add.png" /></a>&nbsp;<?php echo sprintf('<a class="add_address" style="display: none;" href="javascript:void(0);">%s</a>', __('Add address', 'W2DC-ELOCATIONS')); ?>
	<?php endif; ?>

	<?php if ($listing->level->google_map): ?>
	<br />
	<br />
	<input type="hidden" name="map_zoom" class="map_zoom" value="<?php echo $listing->map_zoom; ?>" />
	<input type="button" class="button button-primary btn btn-info" onClick="generateMap(); return false;" value="<?php _e('Generate on google map', 'W2DC-ELOCATIONS'); ?>" />
	<br />
	<br />
	<div class="maps_canvas" id="maps_canvas" style="width: auto; height: 350px;"></div>
	<?php endif;?>
</div>