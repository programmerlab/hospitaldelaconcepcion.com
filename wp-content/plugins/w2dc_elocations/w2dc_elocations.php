<?php
/*
Plugin Name: Web 2.0 Directory enhanced locations
Plugin URI: http://www.salephpscripts.com/wordpress_directory/
Description: Enhanced locations functionality.
Version: 1.2.2
Author: Mihail Chepovskiy
Author URI: http://www.salephpscripts.com
License: commercial
*/

define('W2DC_ELOCATIONS_PATH', plugin_dir_path(__FILE__));

function w2dc_elocations_loadPaths() {
	define('W2DC_ELOCATIONS_TEMPLATES_PATH', W2DC_ELOCATIONS_PATH . 'templates/');

	if (!defined('W2DC_THEME_MODE')) {
		define('W2DC_MAP_ICONS_PATH', plugin_dir_path(__FILE__) . '/map_icons/');
		define('W2DC_MAP_ICONS_URL', plugins_url('/', __FILE__) . 'map_icons/');
	}

	global $w2dc_instance;

	$w2dc_instance->map_markers_url = W2DC_MAP_ICONS_URL;
}
add_action('init', 'w2dc_elocations_loadPaths', 0);

class w2dc_elocations_plugin {

	public function __construct() {
		register_activation_hook(__FILE__, array($this, 'activation'));
		
		add_action('plugins_loaded', array($this, 'load_textdomain'), 0);
	}

	public function activation() {
		include_once(ABSPATH . 'wp-admin/includes/plugin.php');
		if (!defined('W2DC_VERSION')) {
			deactivate_plugins(basename(__FILE__)); // Deactivate ourself
			wp_die("Web 2.0 Web 2.0 Directory plugin required.");
		}
	}
	
	public function load_textdomain() {
		load_plugin_textdomain('W2DC-ELOCATIONS', '', dirname(plugin_basename( __FILE__ )) . '/languages');
	}
}

if (isset($w2dc_instance)) {

	class w2dc_elocations_manager extends w2dc_locations_manager {
		
		public function __construct() {
			global $w2dc_instance, $pagenow;

			if ($pagenow == 'post-new.php' || $pagenow == 'post.php' || $pagenow == 'admin-ajax.php') {				
				remove_action('add_meta_boxes', array($w2dc_instance->locations_manager, 'addLocationsMetabox'), 300);
				add_action('add_meta_boxes', array($this, 'addLocationsMetabox'), 300);

				add_action('wp_ajax_add_location_in_metabox', array($this, 'add_location_in_metabox'));
				add_action('wp_ajax_nopriv_add_location_in_metabox', array($this, 'add_location_in_metabox'));
				
				add_action('wp_ajax_select_map_icon', array($this, 'select_map_icon'));
				add_action('wp_ajax_nopriv_select_map_icon', array($this, 'select_map_icon'));
				
				remove_action('admin_enqueue_scripts', array($w2dc_instance->locations_manager, 'admin_enqueue_scripts_styles'));
				add_action('admin_enqueue_scripts', array($this, 'admin_enqueue_scripts_styles'));
			}

			add_filter('w2dc_level_html', array($this, 'locations_options_in_level_html'));
			add_filter('w2dc_level_validation', array($this, 'locations_options_in_level_validation'));
			add_filter('w2dc_level_create_edit_args', array($this, 'locations_options_in_level_create_add'), 1, 2);
			add_filter('w2dc_level_table_header', array($this, 'level_table_header'), 10, 2);
			add_filter('w2dc_level_table_row', array($this, 'level_table_row'), 1, 2);

			add_filter('w2dc_csv_collation_fields_list', array($this, 'csv_collation_fields'));
			add_filter('w2dc_csv_process_fields', array($this, 'csv_process_fields'), 10, 3);
			add_filter('w2dc_csv_save_location_args', array($this, 'csv_save_location_args'), 10, 3);
		}

		public function listingLocationsMetabox($post) {
			global $w2dc_instance;
			
			$listing = w2dc_getCurrentListingInAdmin();
			$locations_levels = $w2dc_instance->locations_levels;
			w2dc_renderTemplate(array(W2DC_ELOCATIONS_TEMPLATES_PATH, 'locations_metabox.tpl.php'), array('listing' => $listing, 'locations_levels' => $locations_levels));
		}
		
		public function add_location_in_metabox() {
			global $w2dc_instance;
			
			if (isset($_POST['post_id']) && is_numeric($_POST['post_id'])) {
				$listing = new w2dc_listing();
				$listing->loadListingFromPost($_POST['post_id']);
				
				$locations_levels = $w2dc_instance->locations_levels;
				w2dc_renderTemplate(array(W2DC_ELOCATIONS_TEMPLATES_PATH, 'locations_in_metabox.tpl.php'), array('listing' => $listing, 'location' => new w2dc_location, 'locations_levels' => $locations_levels, 'delete_location_link' => true));
			}
			die();
		}
		
		public function select_map_icon() {
			$custom_map_icons = array();
		
			$custom_map_icons_themes = scandir(W2DC_MAP_ICONS_PATH . 'icons/');
			foreach ($custom_map_icons_themes AS $dir) {
				if (is_dir(W2DC_MAP_ICONS_PATH . 'icons/' . $dir) && $dir != '.' && $dir != '..') {
					$custom_map_icons_theme_files = scandir(W2DC_MAP_ICONS_PATH . 'icons/' . $dir);
					foreach ($custom_map_icons_theme_files AS $file)
						if (is_file(W2DC_MAP_ICONS_PATH . 'icons/' . $dir . '/' . $file) && $file != '.' && $file != '..')
							$custom_map_icons[$dir][] = $file;
				}
			}
		
			w2dc_renderTemplate(array(W2DC_ELOCATIONS_TEMPLATES_PATH, 'select_icons.tpl.php'), array('custom_map_icons' => $custom_map_icons));
			die();
		}
		
		public function locations_options_in_level_html($level) {
			w2dc_renderTemplate(array(W2DC_ELOCATIONS_TEMPLATES_PATH, 'locations_options_in_level.tpl.php'), array('level' => $level));
		}
		
		public function locations_options_in_level_validation($validation) {
			$validation->set_rules('locations_number', __('Locations number', 'W2DC-ELOCATIONS'), 'is_natural');
			$validation->set_rules('google_map_markers', __('Custom markers on google map', 'W2DC-ELOCATIONS'), 'is_checked');
			
			return $validation;
		}
		
		public function locations_options_in_level_create_add($insert_update_args, $array) {
			$insert_update_args['locations_number'] = w2dc_getValue($array, 'locations_number', 1);
			$insert_update_args['google_map_markers'] = w2dc_getValue($array, 'google_map_markers', 1);
			return $insert_update_args;
		}
		
		public function level_table_header($columns, $levels) {
			return array_merge($columns, array('locations_number' => __('Locations number', 'W2DC-ELOCATIONS')));
		}
		
		public function level_table_row($items_array, $level) {
			return array_merge($items_array, array('locations_number' => $level->locations_number));
		}
	
		public function validateLocations(&$errors) {
			$validation = new form_validation();
			$validation->set_rules('selected_tax[]', __('Selected location', 'W2DC-ELOCATIONS'), 'is_natural');
			$validation->set_rules('address_line_1[]', __('Address line 1', 'W2DC-ELOCATIONS'));
			$validation->set_rules('address_line_2[]', __('Address line 2', 'W2DC-ELOCATIONS'));
			$validation->set_rules('zip_or_postal_index[]', __('Zip or postal index', 'W2DC-ELOCATIONS'));
			$validation->set_rules('manual_coords[]', __('Use manual coordinates', 'W2DC-ELOCATIONS'), 'is_checked');
			$validation->set_rules('map_coords_1[]', __('Latitude', 'W2DC-ELOCATIONS'), 'numeric');
			$validation->set_rules('map_coords_2[]', __('Longitude', 'W2DC-ELOCATIONS'), 'numeric');
			$validation->set_rules('map_icon_file[]', __('Map icon file', 'W2DC-ELOCATIONS'));
			$validation->set_rules('map_zoom', __('Map zoom', 'W2DC-ELOCATIONS'), 'is_natural');
	
			if (!$validation->run())
				$errors[] = $validation->error_string();
	
			return $validation->result_array();
		}
		
		public function saveLocations($level, $post_id, $validation_results) {
			global $wpdb;

			$wpdb->delete($wpdb->locations_relationships, array('post_id' => $post_id));
			wp_delete_object_term_relationships($post_id, W2DC_LOCATIONS_TAX);
			delete_post_meta($post_id, '_map_zoom');

			if ($validation_results['selected_tax[]']) {
				// remove unauthorized locations
				$validation_results['selected_tax[]'] = array_slice($validation_results['selected_tax[]'], 0, $level->locations_number, true);
		
				foreach ($validation_results['selected_tax[]'] AS $key=>$value) {
					if ($value || $validation_results['address_line_1[]'][$key] || $validation_results['address_line_2[]'][$key] || $validation_results['zip_or_postal_index[]'][$key]) {
						$insert_values = array(
								'post_id' => $post_id,
								'location_id' => $value,
								'address_line_1' => $validation_results['address_line_1[]'][$key],
								'address_line_2' => $validation_results['address_line_2[]'][$key],
								'zip_or_postal_index' => $validation_results['zip_or_postal_index[]'][$key],
						);
						if ($level->google_map) {
							if (is_array($validation_results['manual_coords[]'])) {
								if (in_array($key, array_keys($validation_results['manual_coords[]'])))
									$insert_values['manual_coords'] = 1;
								else
									$insert_values['manual_coords'] = 0;
							} else
								$insert_values['manual_coords'] = 0;
							$insert_values['map_coords_1'] = $validation_results['map_coords_1[]'][$key];
							$insert_values['map_coords_2'] = $validation_results['map_coords_2[]'][$key];
							if ($level->google_map_markers)
								$insert_values['map_icon_file'] = $validation_results['map_icon_file[]'][$key];
						}
						$keys = array_keys($insert_values);
						array_walk($keys, create_function('&$val', '$val = "`".$val."`";'));
						array_walk($insert_values, create_function('&$val', '$val = "\'".$val."\'";'));
						$wpdb->query("INSERT INTO {$wpdb->locations_relationships} (" . implode(', ', $keys) . ") VALUES (" . implode(', ', $insert_values) . ")");
					}
				}
		
				array_walk($validation_results['selected_tax[]'], create_function('&$val', '$val = intval($val);'));
				wp_set_object_terms($post_id, $validation_results['selected_tax[]'], W2DC_LOCATIONS_TAX);
				
				add_post_meta($post_id, '_map_zoom', $validation_results['map_zoom']);
			}
		}
		
		public function csv_collation_fields($field_list) {
			$field_list['map_icon_file'] = __('Map icon file', 'W2DC-ELOCATIONS');

			return $field_list;
		}
		
		public function csv_process_fields($new_listing, $field, $value) {
			if ($field == 'map_icon_file')
				$new_listing['map_icon_file'] = $value;
			
			return $new_listing;
		}

		public function csv_save_location_args($args, $new_post_id, $new_listing) {
			$args['map_icon_file[]'] = array(isset($new_listing['map_icon_file']) ? $new_listing['map_icon_file'] : '');
			
			return $args;
		}
	}

	$w2dc_instance->locations_manager = new w2dc_elocations_manager();
}

global $w2dc_elocations_instance;

$w2dc_elocations_instance = new w2dc_elocations_plugin();

?>
