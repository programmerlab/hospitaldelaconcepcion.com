		<div class="w2dc_directory_frontpanel">
			<script>
				var window_width = 860;
				var window_height = 800;
				var leftPosition, topPosition;
			   	leftPosition = (window.screen.width / 2) - ((window_width / 2) + 10);
			   	topPosition = (window.screen.height / 2) - ((window_height / 2) + 50);
			</script>
			<input type="button" class="w2dc_print_listing_link btn btn-info" onclick="window.open('<?php echo add_query_arg('invoice_id', $frontend_controller->invoice->post->ID, w2dc_directoryUrl(array('w2dc_action' => 'w2dc_print_invoice'))); ?>', 'print_window', 'height='+window_height+',width='+window_width+',left='+leftPosition+',top='+topPosition+',menubar=yes,scrollbars=yes');" value="<?php _e('Print invoice', 'W2DC-PAYMENTS'); ?>" />
			
			<?php if ($frontend_controller->invoice->gateway): ?>
			<input type="button" class="w2dc_reset_link btn btn-info" onclick="window.location='<?php echo add_query_arg('invoice_action', 'reset_gateway', get_edit_post_link($frontend_controller->invoice->post->ID)); ?>';" value="<?php _e('Reset gateway', 'W2DC-PAYMENTS'); ?>" />
			<?php endif; ?>
		</div>
		
		<div class="submit_section">
			<h3 class="submit_section_label"><?php _e('Invoice Info', 'W2DC-FSUBMIT'); ?></h3>
			<div class="submit_section_inside">
				<?php w2dc_renderTemplate(array(W2DC_PAYMENTS_TEMPLATES_PATH, 'info_metabox.tpl.php'), array('invoice' => $frontend_controller->invoice)); ?>
			</div>
		</div>

		<?php if ((get_option('w2dc_paypal_email') || get_option('w2dc_allow_bank')) && $frontend_controller->invoice->status == 'unpaid' && !$frontend_controller->invoice->gateway): ?>
		<div class="submit_section">
			<h3 class="submit_section_label"><?php _e('Invoice Payment - choose payment gateway', 'W2DC-FSUBMIT'); ?></h3>
			<div class="submit_section_inside">
				<?php w2dc_renderTemplate(array(W2DC_PAYMENTS_TEMPLATES_PATH, 'payment_metabox.tpl.php'), array('invoice' => $frontend_controller->invoice, 'paypal' => $frontend_controller->paypal, 'paypal_subscription' => $frontend_controller->paypal_subscription, 'bank_transfer' => $frontend_controller->bank_transfer, 'stripe' => $frontend_controller->stripe)); ?>
			</div>
		</div>
		<?php endif; ?>

		<div class="submit_section">
			<h3 class="submit_section_label"><?php _e('Invoice Log', 'W2DC-FSUBMIT'); ?></h3>
			<div class="submit_section_inside">
				<?php w2dc_renderTemplate(array(W2DC_PAYMENTS_TEMPLATES_PATH, 'log_metabox.tpl.php'), array('invoice' => $frontend_controller->invoice)); ?>
			</div>
		</div>

		<a href="<?php echo w2dc_dashboardUrl(array('w2dc_action' => 'invoices')); ?>" class="btn btn-info"><?php _e('View all invoices', 'W2DC'); ?></a>