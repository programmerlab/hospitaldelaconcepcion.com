msgid ""
msgstr ""
"Project-Id-Version: w2dc payments v1.0.0\n"
"POT-Creation-Date: 2014-08-21 10:00+0600\n"
"PO-Revision-Date: 2014-08-21 10:00+0600\n"
"Last-Translator: \n"
"Language-Team: \n"
"Language: en_US\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.6.8\n"
"X-Poedit-KeywordsList: __;_e;_n:1,2\n"
"X-Poedit-Basepath: ..\n"
"X-Poedit-SourceCharset: UTF-8\n"
"X-Poedit-SearchPath-0: .\n"

#: classes/gateways/bank_transfer.php:13 classes/invoice.php:109
msgid "Bank transfer"
msgstr ""

#: classes/gateways/bank_transfer.php:17
msgid ""
"Print invoice and transfer the payment (bank transfer information included)"
msgstr ""

#: classes/gateways/bank_transfer.php:26
msgid ""
"You chose bank transfer payment gateway, now print invoice and transfer the "
"payment"
msgstr ""

#: classes/gateways/paypal.php:35 classes/invoice.php:103
msgid "PayPal"
msgstr ""

#: classes/gateways/paypal.php:39
msgid ""
"One time payment by PayPal. After successful transaction listing will become "
"active and raised up."
msgstr ""

#: classes/gateways/paypal.php:160 classes/gateways/stripe.php:105
msgid "Payment successfully completed. Transaction data: "
msgstr ""

#: classes/gateways/paypal.php:162
#: classes/gateways/paypal_subscription.php:211
msgid "Transaction failed, Error: "
msgstr ""

#: classes/gateways/paypal_subscription.php:44 classes/invoice.php:106
msgid "PayPal subscription"
msgstr ""

#: classes/gateways/paypal_subscription.php:48
msgid "Regular automatic payments from your PayPal account."
msgstr ""

#: classes/gateways/paypal_subscription.php:85
msgid ""
"This item is not allowed to be paid as subscription or it is not possible to "
"pay for recurring cycle of this item using selected payment gateway."
msgstr ""

#: classes/gateways/paypal_subscription.php:209
msgid "Recurring payment was successfully completed. Transaction data: "
msgstr ""

#: classes/gateways/paypal_subscription.php:219
msgid "Subscription canceled"
msgstr ""

#: classes/gateways/stripe.php:36
msgid "Stripe"
msgstr ""

#: classes/gateways/stripe.php:40
msgid ""
"One time payment by Stripe. After successful transaction listing will become "
"active and raised up."
msgstr ""

#: classes/invoice.php:122
msgid "Invoice created"
msgstr ""

#: classes/items/item_listing.php:22 classes/items/item_listing.php:29
#: classes/items/item_listing.php:36 classes/items/item_listing_upgrade.php:17
#: classes/items/item_listing_upgrade.php:32
msgid "N/A"
msgstr ""

#: classes/items/item_listing.php:34
msgid "Active period - "
msgstr ""

#: classes/items/item_listing.php:75 templates/invoices_dashboard.tpl.php:29
#: w2dc_payments.php:142
msgid "pay invoice"
msgstr ""

#: classes/items/item_listing.php:85
#, php-format
msgid "Invoice for activation of listing: %s"
msgstr ""

#: classes/items/item_listing.php:92 classes/items/item_listing.php:116
msgid ""
"New invoice was created successfully, listing become active after payment"
msgstr ""

#: classes/items/item_listing.php:109
#, php-format
msgid "Invoice for renewal of listing: %s"
msgstr ""

#: classes/items/item_listing_raiseup.php:21
#, php-format
msgid "Invoice for raise up of listing: %s"
msgstr ""

#: classes/items/item_listing_raiseup.php:28
msgid ""
"New invoice was created successfully, listing will be raised up after payment"
msgstr ""

#: classes/items/item_listing_upgrade.php:15
#: classes/items/item_listing_upgrade.php:30
#, php-format
msgid "From '%s' to '%s' level"
msgstr ""

#: classes/items/item_listing_upgrade.php:50
#, php-format
msgid "Invoice for upgrade of listing: %s"
msgstr ""

#: classes/items/item_listing_upgrade.php:57
#, php-format
msgid ""
"New invoice was created successfully, listing \"%s\" will be upgraded after "
"payment"
msgstr ""

#: templates/actions_metabox.tpl.php:10 templates/invoice_print.tpl.php:40
#: templates/invoice_print.tpl.php:61
#: templates/view_invoice_dashboard.tpl.php:9
msgid "Print invoice"
msgstr ""

#: templates/actions_metabox.tpl.php:15
#: templates/view_invoice_dashboard.tpl.php:12
msgid "Reset gateway"
msgstr ""

#: templates/actions_metabox.tpl.php:21
msgid "Set as paid"
msgstr ""

#: templates/info_metabox.tpl.php:8
msgid "Invoice ID"
msgstr ""

#: templates/info_metabox.tpl.php:15
msgid "Author"
msgstr ""

#: templates/info_metabox.tpl.php:22 templates/invoices_dashboard.tpl.php:5
#: w2dc_payments.php:116
msgid "Item"
msgstr ""

#: templates/info_metabox.tpl.php:29
msgid "Item options"
msgstr ""

#: templates/info_metabox.tpl.php:36 templates/invoices_dashboard.tpl.php:6
#: w2dc_payments.php:117 w2dc_payments.php:616 w2dc_payments.php:628
msgid "Price"
msgstr ""

#: templates/info_metabox.tpl.php:42
msgid "Status"
msgstr ""

#: templates/info_metabox.tpl.php:45 templates/invoices_dashboard.tpl.php:27
#: w2dc_payments.php:140
msgid "unpaid"
msgstr ""

#: templates/info_metabox.tpl.php:47 templates/invoices_dashboard.tpl.php:31
#: w2dc_payments.php:144
msgid "paid"
msgstr ""

#: templates/info_metabox.tpl.php:49 templates/invoices_dashboard.tpl.php:35
#: w2dc_payments.php:148
msgid "pending"
msgstr ""

#: templates/info_metabox.tpl.php:55
msgid "Gateway"
msgstr ""

#: templates/invoice_print.tpl.php:40 templates/invoice_print.tpl.php:61
msgid "Close window"
msgstr ""

#: templates/invoice_print.tpl.php:44 w2dc_payments.php:333
msgid "Bank transfer information"
msgstr ""

#: templates/invoice_print.tpl.php:51
#: templates/view_invoice_dashboard.tpl.php:17 w2dc_payments.php:180
msgid "Invoice Info"
msgstr ""

#: templates/invoice_print.tpl.php:57
#: templates/view_invoice_dashboard.tpl.php:33 w2dc_payments.php:199
msgid "Invoice Log"
msgstr ""

#: templates/invoices_dashboard.tpl.php:4 w2dc_payments.php:120
msgid "Invoice"
msgstr ""

#: templates/invoices_dashboard.tpl.php:7 w2dc_payments.php:118
msgid "Payment"
msgstr ""

#: templates/invoices_dashboard.tpl.php:8
msgid "Creation date"
msgstr ""

#: templates/levels_price_in_level.tpl.php:3 w2dc_payments.php:603
msgid "Listings price"
msgstr ""

#: templates/levels_price_in_level.tpl.php:15 w2dc_payments.php:604
msgid "Listings raise up price"
msgstr ""

#: templates/log_metabox.tpl.php:3
msgid "Logged Date"
msgstr ""

#: templates/log_metabox.tpl.php:4
msgid "Message"
msgstr ""

#: templates/stripe_button.tpl.php:41 w2dc_payments.php:512
#: w2dc_payments.php:519 w2dc_payments.php:526 w2dc_payments.php:533
msgid "Payment by Stripe requires installed OpenSSL extension!"
msgstr ""

#: templates/view_invoice_dashboard.tpl.php:25 w2dc_payments.php:220
msgid "Invoice Payment - choose payment gateway"
msgstr ""

#: templates/view_invoice_dashboard.tpl.php:39
msgid "View all invoices"
msgstr ""

#: w2dc_payments.php:244
msgid "Invoice actions"
msgstr ""

#: w2dc_payments.php:258
msgid "Payments"
msgstr ""

#: w2dc_payments.php:264
msgid "General payments settings"
msgstr ""

#: w2dc_payments.php:270
msgid "Any services are Free for administrators"
msgstr ""

#: w2dc_payments.php:278
msgid "Currency"
msgstr ""

#: w2dc_payments.php:286
msgid "Currency symbol or code"
msgstr ""

#: w2dc_payments.php:294
msgid "Currency symbol or code position"
msgstr ""

#: w2dc_payments.php:302
msgid "Decimal separator"
msgstr ""

#: w2dc_payments.php:310
msgid "Thousands separator"
msgstr ""

#: w2dc_payments.php:319
msgid "Bank transfer settings"
msgstr ""

#: w2dc_payments.php:325
msgid "Allow bank transfer"
msgstr ""

#: w2dc_payments.php:342
msgid "PayPal settings"
msgstr ""

#: w2dc_payments.php:348
msgid "Business email"
msgstr ""

#: w2dc_payments.php:356
msgid "Allow subscriptions"
msgstr ""

#: w2dc_payments.php:364 w2dc_payments.php:411
msgid "Test Sandbox mode"
msgstr ""

#: w2dc_payments.php:373
msgid "Stripe settings"
msgstr ""

#: w2dc_payments.php:379
msgid "Test secret key"
msgstr ""

#: w2dc_payments.php:387
msgid "Test publishable key"
msgstr ""

#: w2dc_payments.php:395
msgid "Live secret key"
msgstr ""

#: w2dc_payments.php:403
msgid "Live publishable key"
msgstr ""

#: w2dc_payments.php:425
msgid "US Dollars ($)"
msgstr ""

#: w2dc_payments.php:426
msgid "Euros (€)"
msgstr ""

#: w2dc_payments.php:427
msgid "Pounds Sterling (£)"
msgstr ""

#: w2dc_payments.php:428
msgid "Australian Dollars ($)"
msgstr ""

#: w2dc_payments.php:429
msgid "Brazilian Real (R$)"
msgstr ""

#: w2dc_payments.php:430
msgid "Canadian Dollars ($)"
msgstr ""

#: w2dc_payments.php:431
msgid "Czech Koruna (Kč)"
msgstr ""

#: w2dc_payments.php:432
msgid "Danish Krone (kr)"
msgstr ""

#: w2dc_payments.php:433
msgid "Hong Kong Dollar ($)"
msgstr ""

#: w2dc_payments.php:434
msgid "Hungarian Forint (Ft)"
msgstr ""

#: w2dc_payments.php:435
msgid "Israeli Shekel (₪)"
msgstr ""

#: w2dc_payments.php:436
msgid "Japanese Yen (¥)"
msgstr ""

#: w2dc_payments.php:437
msgid "Malaysian Ringgits (RM)"
msgstr ""

#: w2dc_payments.php:438
msgid "Mexican Peso ($)"
msgstr ""

#: w2dc_payments.php:439
msgid "New Zealand Dollar ($)"
msgstr ""

#: w2dc_payments.php:440
msgid "Norwegian Krone (kr)"
msgstr ""

#: w2dc_payments.php:441
msgid "Philippine Pesos (P)"
msgstr ""

#: w2dc_payments.php:442
msgid "Polish Zloty (zł)"
msgstr ""

#: w2dc_payments.php:443
msgid "Singapore Dollar ($)"
msgstr ""

#: w2dc_payments.php:444
msgid "Swedish Krona (kr)"
msgstr ""

#: w2dc_payments.php:445
msgid "Swiss Franc (Fr)"
msgstr ""

#: w2dc_payments.php:446
msgid "Taiwan New Dollar ($)"
msgstr ""

#: w2dc_payments.php:447
msgid "Thai Baht (฿)"
msgstr ""

#: w2dc_payments.php:448
msgid "Turkish Lira (₤)"
msgstr ""

#: w2dc_payments.php:468 w2dc_payments.php:476
msgid "dot"
msgstr ""

#: w2dc_payments.php:469 w2dc_payments.php:477
msgid "comma"
msgstr ""

#: w2dc_payments.php:475
msgid "no separator"
msgstr ""

#: w2dc_payments.php:478
msgid "space"
msgstr ""

#: w2dc_payments.php:495
msgid ""
"It is required to use permalinks for PayPal payment gateway! Enable "
"permalinks first!"
msgstr ""

#: w2dc_payments.php:500
msgid ""
"You must have a <a href=\"http://developer.paypal.com/\" target=\"_blank"
"\">PayPal Sandbox</a> account setup before using this feature"
msgstr ""

#: w2dc_payments.php:505
msgid "Only for listings with limited active period"
msgstr ""

#: w2dc_payments.php:538
msgid ""
"You can only use <a href=\"http://stripe.com/\" target=\"_blank\">Stripe</a> "
"in test mode until you activate your account."
msgstr ""

#: w2dc_payments.php:544 w2dc_payments.php:552
msgid "Directory invoices"
msgstr ""

#: w2dc_payments.php:545
msgid "Directory invoice"
msgstr ""

#: w2dc_payments.php:546
msgid "View Invoice"
msgstr ""

#: w2dc_payments.php:547
msgid "Search invoices"
msgstr ""

#: w2dc_payments.php:548
msgid "No invoices found"
msgstr ""

#: w2dc_payments.php:549
msgid "No invoices found in trash"
msgstr ""

#: w2dc_payments.php:633 w2dc_payments.php:914
msgid "FREE"
msgstr ""

#: w2dc_payments.php:694
msgid "Step"
msgstr ""

#: w2dc_payments.php:695
msgid "Pay Invoice"
msgstr ""

#: w2dc_payments.php:761
#, php-format
msgid "Payment gateway was selected: %s"
msgstr ""

#: w2dc_payments.php:762
msgid "Payment gateway was selected!"
msgstr ""

#: w2dc_payments.php:771
msgid "Payment gateway was reset"
msgstr ""

#: w2dc_payments.php:772
msgid "Payment gateway was reset!"
msgstr ""

#: w2dc_payments.php:779
msgid "Invoice was manually set as paid"
msgstr ""

#: w2dc_payments.php:780
msgid "Invoice was manually set as paid!"
msgstr ""

#: w2dc_payments.php:782
msgid "An error has occured!"
msgstr ""

#: w2dc_payments.php:843
msgid "Invoices"
msgstr ""
