<?php
/*
Plugin Name: Web 2.0 Directory enhanced search
Plugin URI: http://www.salephpscripts.com/wordpress_directory/
Description: Enhanced search form with categories selection, content fields, those depend on selected categories. Also locations search in radius available.
Version: 1.2.1
Author: Mihail Chepovskiy
Author URI: http://www.salephpscripts.com
License: commercial
*/

define('W2DC_ESEARCH_PATH', plugin_dir_path(__FILE__));

function w2dc_esearch_loadPaths() {
	define('W2DC_ESEARCH_TEMPLATES_PATH', W2DC_ESEARCH_PATH . 'templates/');

	if (!defined('W2DC_THEME_MODE'))
		define('W2DC_ESEARCH_RESOURCES_URL', plugins_url('/', __FILE__) . 'resources/');
}
add_action('init', 'w2dc_esearch_loadPaths', 0);

include_once W2DC_ESEARCH_PATH . 'classes/search_fields/content_field_search.php';
include_once W2DC_ESEARCH_PATH . 'classes/search_fields/fields/content_field_string_search.php';
include_once W2DC_ESEARCH_PATH . 'classes/search_fields/fields/content_field_select_search.php';
include_once W2DC_ESEARCH_PATH . 'classes/search_fields/fields/content_field_checkbox_search.php';
include_once W2DC_ESEARCH_PATH . 'classes/search_fields/fields/content_field_radio_search.php';
include_once W2DC_ESEARCH_PATH . 'classes/search_fields/fields/content_field_number_search.php';
include_once W2DC_ESEARCH_PATH . 'classes/search_fields/fields/content_field_price_search.php';
include_once W2DC_ESEARCH_PATH . 'classes/search_fields/fields/content_field_datetime_search.php';
include_once W2DC_ESEARCH_PATH . 'classes/search_fields/fields/content_field_textarea_search.php';

class w2dc_esearch_plugin {
	public $search_fields = array();
	public $radius_value;
	public $x_coord;
	public $y_coord;
	
	public $random_id;
	
	public function __construct() {
		register_activation_hook(__FILE__, array($this, 'activation'));
	}
	
	public function activation() {
		include_once(ABSPATH . 'wp-admin/includes/plugin.php');
		if (!defined('W2DC_VERSION')) {
			deactivate_plugins(basename(__FILE__)); // Deactivate ourself
			wp_die("Web 2.0 Web 2.0 Directory plugin required.");
		}
	}

	public function init() {
		global $w2dc_instance;
		
		if (!get_option('w2dc_installed_esearch'))
			w2dc_install_esearch();
		
		add_action('plugins_loaded', array($this, 'load_textdomain'), 0);

		/* add_action('wp', array($this, 'load_search_fields'));
		add_action('admin_init', array($this, 'load_search_fields')); */
		if (isset($w2dc_instance))
			$this->load_search_fields();
		add_action('w2dc_version_upgrade', 'w2dc_upgrade_esearch');

		add_action('admin_init', array($this, 'register_settings'));
		
		add_action('pre_search_what_form_html', array($this,  'render_categories_dropboxes'));
		add_action('post_search_what_form_html', array($this,  'render_content_fields'));

		add_action('buttons_search_form_html', array($this,  'render_advanced_link'));

		add_action('post_search_where_form_html', array($this,  'render_radius_search'));

		add_filter('w2dc_search_args', array($this,  'retrieve_search_args'), 100, 2);
		//add_filter('w2dc_order_args', array($this, 'order_by_distance_args'), 100);

		add_filter('w2dc_base_url_args', array($this, 'base_url_args'));
		
		add_filter('w2dc_content_fields_column_options', array($this, 'search_configuration_link'), 10, 2);
		add_filter('w2dc_content_fields_in_pages_options', array($this, 'on_search_form_label'), 10, 2);
		add_filter('w2dc_content_field_html', array($this, 'search_content_field_html'));
		add_filter('w2dc_content_field_validation', array($this, 'search_content_field_validation'), 10, 2);
		add_filter('w2dc_content_field_create_edit_args', array($this, 'search_content_field_save'), 10, 3);

		// handle search configuration page
		add_action('admin_menu', array($this, 'menu'));
		
		add_action('wp_enqueue_scripts', array($this, 'enqueue_scripts_styles'));
	}

	public function load_textdomain() {
		load_plugin_textdomain('W2DC-ESEARCH', '', dirname(plugin_basename( __FILE__ )) . '/languages');
	}
	
	public function menu() {
		global $w2dc_instance;

		add_action($w2dc_instance->content_fields_manager->menu_page_hook, array($this, 'search_configuration_page'));
	}
	
	public function register_settings() {
		add_settings_field(
				'w2dc_show_categories_search',
				__('Show categories search?', 'W2DC'),
				array($this, 'w2dc_show_categories_search_callback'),
				'w2dc_pagesviews_settings_page',
				'w2dc_search_section'
		);
		register_setting('w2dc_pagesviews_settings_page', 'w2dc_show_categories_search');
		add_settings_field(
				'w2dc_show_radius_search',
				__('Show locations radius search?', 'W2DC'),
				array($this, 'w2dc_show_radius_search_callback'),
				'w2dc_pagesviews_settings_page',
				'w2dc_search_section'
		);
		register_setting('w2dc_pagesviews_settings_page', 'w2dc_show_radius_search');

		add_settings_field(
				'w2dc_show_category_count_in_search',
				__('Show listings counts in categories search dropboxes?', 'W2DC-ESEARCH'),
				array($this, 'w2dc_show_category_count_in_search_callback'),
				'w2dc_pagesviews_settings_page',
				'w2dc_search_section'
		);
		register_setting('w2dc_pagesviews_settings_page', 'w2dc_show_category_count_in_search');

		add_settings_field(
				'w2dc_miles_kilometers_in_search',
				__('Dimension in radius search', 'W2DC-ESEARCH'),
				array($this, 'w2dc_miles_kilometers_in_search_callback'),
				'w2dc_pagesviews_settings_page',
				'w2dc_search_section'
		);
		register_setting('w2dc_pagesviews_settings_page', 'w2dc_miles_kilometers_in_search');

		add_settings_field(
				'w2dc_radius_search_min',
				__('Minimum radius search', 'W2DC-ESEARCH'),
				array($this, 'w2dc_radius_search_min_callback'),
				'w2dc_pagesviews_settings_page',
				'w2dc_search_section'
		);
		register_setting('w2dc_pagesviews_settings_page', 'w2dc_radius_search_min');
		add_settings_field(
				'w2dc_radius_search_max',
				__('Maximum radius search', 'W2DC-ESEARCH'),
				array($this, 'w2dc_radius_search_max_callback'),
				'w2dc_pagesviews_settings_page',
				'w2dc_search_section'
		);
		register_setting('w2dc_pagesviews_settings_page', 'w2dc_radius_search_max');
		add_settings_field(
				'w2dc_radius_search_default',
				__('Default radius search', 'W2DC-ESEARCH'),
				array($this, 'w2dc_radius_search_default_callback'),
				'w2dc_pagesviews_settings_page',
				'w2dc_search_section'
		);
		register_setting('w2dc_pagesviews_settings_page', 'w2dc_radius_search_default');

		add_settings_field(
				'w2dc_orderby_distance',
				__('Allow sorting by distance when search by radius', 'W2DC-ESEARCH'),
				array($this, 'w2dc_orderby_distance_callback'),
				'w2dc_pagesviews_settings_page',
				'w2dc_sorting_section'
		);
		register_setting('w2dc_pagesviews_settings_page', 'w2dc_orderby_distance');
	}
	
	public function w2dc_show_categories_search_callback() {
		echo '<input type="checkbox" id="w2dc_show_categories_search" name="w2dc_show_categories_search" value="1" ' . checked(get_option('w2dc_show_categories_search'), 1, false) .' />';
		echo '<p class="description">' . __('This setting is actual for both: main search block and widget', 'W2DC') . '</p>';
	}
	public function w2dc_show_radius_search_callback() {
		echo '<input type="checkbox" id="w2dc_show_radius_search" name="w2dc_show_radius_search" value="1" ' . checked(get_option('w2dc_show_radius_search'), 1, false) .' />';
		echo '<p class="description">' . __('This setting is actual for both: main search block and widget', 'W2DC') . '</p>';
	}

	public function w2dc_show_category_count_in_search_callback() {
		echo '<input type="checkbox" id="w2dc_show_category_count_in_search" name="w2dc_show_category_count_in_search" value="1" ' . checked(get_option('w2dc_show_category_count_in_search'), 1, false) .' />';
	}

	public function w2dc_miles_kilometers_in_search_callback() {
		echo '<label><input type="radio" id="w2dc_miles_kilometers_in_search" name="w2dc_miles_kilometers_in_search" value="miles" ' . checked(get_option('w2dc_miles_kilometers_in_search'), 'miles', false) .' /> ' . __('miles', 'W2DC-ESEARCH') . '</label>&nbsp;&nbsp;<label><input type="radio" id="w2dc_miles_kilometers_in_search" name="w2dc_miles_kilometers_in_search" value="kilometers" ' . checked(get_option('w2dc_miles_kilometers_in_search'), 'kilometers', false) .' /> ' . __('kilometers', 'W2DC-ESEARCH') . '</label>';
	}
	
	public function w2dc_radius_search_min_callback() {
		echo '<input type="text" id="w2dc_radius_search_min" name="w2dc_radius_search_min" value="' . esc_attr(get_option('w2dc_radius_search_min')) .'" size="1" />';
	}
	public function w2dc_radius_search_max_callback() {
		echo '<input type="text" id="w2dc_radius_search_max" name="w2dc_radius_search_max" value="' . esc_attr(get_option('w2dc_radius_search_max')) .'" size="1" />';
	}
	public function w2dc_radius_search_default_callback() {
		echo '<input type="text" id="w2dc_radius_search_default" name="w2dc_radius_search_default" value="' . esc_attr(get_option('w2dc_radius_search_default')) .'" size="1" />';
	}

	public function w2dc_orderby_distance_callback() {
		echo '<input type="checkbox" id="w2dc_orderby_distance" name="w2dc_orderby_distance" value="1" ' . checked(get_option('w2dc_orderby_distance'), 1, false) .' />';
	}
	
	public function load_search_fields() {
		global $w2dc_instance;

		// load only for our page
		//if (!$this->search_fields && ((isset($w2dc_instance->frontend_controller) && $w2dc_instance->frontend_controller->query) || is_admin())) {
		if (!$this->search_fields) {
			$content_fields = $w2dc_instance->content_fields->content_fields_array;

			foreach ($content_fields AS $content_field) {
				if ($content_field->canBeSearched() && (is_admin() || $content_field->on_search_form)) {
					$field_search_class = get_class($content_field) . '_search';
					if (class_exists($field_search_class)) {
						$search_field = new $field_search_class;
						$search_field->assignContentField($content_field);
						$search_field->convertSearchOptions();
						$this->search_fields[$content_field->id] = $search_field;
					}
				}
			}
		}
		//var_dump($this->search_fields);
	}
	
	public function search_configuration_link($actions, $item) {
		if ($item['is_search_configuration_page'])
			$actions['search_configure'] = sprintf('<a href="?page=%s&action=%s&field_id=%d">' . __('Configure search', 'W2DC-ESEARCH') . '</a>', $_GET['page'], 'configure_search', $item['id']);
		
		return $actions;
	}
	
	public function on_search_form_label($html_array, $item) {
		if ($item['on_search_form'])
			$html_array[] = __('On search form', 'W2DC-ESEARCH');
		
		return $html_array;
	}
	
	public function search_content_field_html($content_field) {
		global $w2dc_instance;

		w2dc_renderTemplate(array(W2DC_ESEARCH_TEMPLATES_PATH, 'search_fields/search_content_field_option.tpl.php'), array('content_field' => $content_field, 'content_fields' => $w2dc_instance->content_fields));
	}
	
	public function search_content_field_validation($validation, $content_field) {
		if ($content_field->canBeSearched()) {
			$validation->set_rules('on_search_form', __('On search form', 'W2DC-ESEARCH'), 'is_checked');
			$validation->set_rules('advanced_search_form', __('On advanced search panel', 'W2DC-ESEARCH'), 'is_checked');
		}

		return $validation;
	}
	
	public function search_content_field_save($insert_update_args, $content_field, $array) {
		if ($content_field->canBeSearched()) {
			$insert_update_args['on_search_form'] = w2dc_getValue($array, 'on_search_form');
			$insert_update_args['advanced_search_form'] = w2dc_getValue($array, 'advanced_search_form');
		} else {
			$insert_update_args['on_search_form'] = 0;
			$insert_update_args['advanced_search_form'] = 0;
			$insert_update_args['search_options'] = '';
		}
		
		return $insert_update_args;
	}
	
	public function search_configuration_page() {
		if (isset($_GET['action']) && $_GET['action'] == 'configure_search' && isset($_GET['field_id'])) {
			$field_id = $_GET['field_id'];
			if (isset($this->search_fields[$field_id])) {
				$search_field = $this->search_fields[$field_id];
				$search_field->searchConfigure();
			}
		}
	}
	
	public function render_categories_dropboxes() {
		global $w2dc_instance;
		
		if (get_option('w2dc_show_categories_search')) {
			$term_id = 0;
			//if (is_tax() && get_query_var('taxonomy') == W2DC_CATEGORIES_TAX) {
			if (get_query_var('category')) {
				$category_object = get_term_by('slug', get_query_var('category'), W2DC_CATEGORIES_TAX);
				$term_id = $category_object->term_id;
			} elseif (isset($_GET['search_category']) && is_numeric($_GET['search_category']))
				$term_id = $_GET['search_category'];
	
			w2dc_renderTemplate(array(W2DC_ESEARCH_TEMPLATES_PATH, 'categories_dropboxes.tpl.php'), array('term_id' => $term_id));
		}
	}
	
	public function retrieve_search_args($args, $defaults = array()) {
		global $w2dc_instance;

		$search_category = w2dc_getValue($_GET, 'search_category', w2dc_getValue($defaults, 'categories'));
		if ($search_category) {
			if ($categories = array_filter(explode(',', $search_category), 'trim')) {
				$field = 'term_id';
				foreach ($categories AS $category)
					if (!is_numeric($category))
						$field = 'slug';

				$args['tax_query'] = array(
						array(
							'taxonomy' => W2DC_CATEGORIES_TAX,
							'terms' => $categories,
							'field' => $field
				));
			}
		}

		$radius = w2dc_getValue($_GET, 'where_radius', w2dc_getValue($defaults, 'radius'));
		if ($radius && is_numeric($radius)) {
			$this->radius_value = $radius;

			$search_location = w2dc_getValue($_GET, 'search_location', w2dc_getValue($defaults, 'location_id'));
			$where_search = w2dc_getValue($_GET, 'where_search', w2dc_getValue($defaults, 'address'));
			if (($search_location && is_numeric($search_location)) || $where_search) {
				$chain = array();
				$parent_id = $search_location;
				while ($parent_id != 0) {
					if ($term = get_term($parent_id, W2DC_LOCATIONS_TAX)) {
						$chain[] = $term->name;
						$parent_id = $term->parent;
					} else
						$parent_id = 0;
				}
				$location_string = implode(', ', $chain);

				if ($where_search)
					$location_string = $where_search . ' ' . $location_string;

				$locationGeoname = new locationGeoname();
				if ($coords = $locationGeoname->geonames_request($location_string, 'coordinates')) {
					add_filter('w2dc_ordering_options', array($this, 'order_by_distance_html'), 10, 3);

					$this->x_coord = $coords[1];
					$this->y_coord = $coords[0];

					if (get_option('w2dc_miles_kilometers_in_search') == 'miles')
						$R = 3956; // earth's mean radius in miles
					else
						$R = 6367; // earth's mean radius in km

					$dLat = '((map_coords_1-'.$this->x_coord.')*PI()/180)';
					$dLong = '((map_coords_2-'.$this->y_coord.')*PI()/180)';
					$a = '(sin('.$dLat.'/2) * sin('.$dLat.'/2) + cos('.$this->x_coord.'*pi()/180) * cos(map_coords_1*pi()/180) * sin('.$dLong.'/2) * sin('.$dLong.'/2))';
					$c = '2*atan2(sqrt('.$a.'), sqrt(1-'.$a.'))';
					$sql = $R.'*'.$c; 

					global $wpdb;
					$results = $wpdb->get_results($wpdb->prepare(
						"SELECT DISTINCT
							post_id, " . $sql . " AS distance FROM {$wpdb->locations_relationships}
						HAVING
							distance <= %d
						ORDER BY
							distance
						", $this->radius_value), ARRAY_A);

					$post_ids = array();
					foreach ($results AS $row)
						$post_ids[] = $row['post_id'];
					$post_ids = array_unique($post_ids);

					if ($post_ids) {
						$args['post__in'] = $post_ids;
					} else
						// Do not show any listings
						$args['post__in'] = array(0);

					$args = $this->order_by_distance_args($args, $defaults);
				}
			}
		}

		//$this->load_search_fields();
		foreach ($this->search_fields AS $search_field)
			$search_field->validateSearch($args, $defaults);
		
		return $args;
	}
	
	public function order_by_distance_args($args, $defaults) {
		if ($this->radius_value && (!isset($defaults['order_by']) || !$defaults['order_by']))
			$defaults['order_by'] = 'distance';
		
		$order_by = w2dc_getValue($_GET, 'order_by', w2dc_getValue($defaults, 'order_by'));
		$order = w2dc_getValue($_GET, 'order', w2dc_getValue($defaults, 'order'));

		// When search by radius - order by distance by default instead of ordering by date
		if ($order_by == 'distance' && get_option('w2dc_orderby_distance')) {
			$args['orderby'] = 'post__in';
			unset($args['meta_key']);
			if ($order == 'DESC') {
				if (isset($args['post__in']) && is_array($args['post__in']))
					$args['post__in'] = array_reverse($args['post__in']);
			}

			// Do not affect levels weights when search by radius
			remove_filter('posts_join', 'join_levels');
			remove_filter('posts_orderby', 'orderby_levels', 1);
		}

		return $args;
	}
	
	public function order_by_distance_html($ordering, $base_url, $defaults = array()) {
		if ($this->radius_value && (!isset($_GET['order_by']) || !$_GET['order_by'] || $_GET['order_by'] == 'distance')) {
			if (isset($defaults['order_by']) && $defaults['order_by'])
				$ordering['links'][$defaults['order_by']] = '<a href="' . add_query_arg('order_by', $defaults['order_by'], $base_url) . '">' . $ordering['array'][$defaults['order_by']] . '</a>';

			$order_by = 'distance';
			$order = w2dc_getValue($_GET, 'order', 'ASC');
		} else {
			$order_by = w2dc_getValue($defaults, 'order_by');
			$order = w2dc_getValue($defaults, 'order');
		}

		$class = '';
		// When search by radius - order by distance by default instead of ordering by date
		if ($order_by == 'distance' && get_option('w2dc_orderby_distance')) {
			if (!$order || $order == 'ASC') {
				$class = 'ascending';
				$url = add_query_arg('order', 'DESC', add_query_arg('order_by', 'distance', $base_url));
			} elseif ($order == 'DESC') {
				$class = 'descending';
				$url = add_query_arg('order_by', 'distance', $base_url);
			}
		} else
			$url = add_query_arg('order_by', 'distance', $base_url);

		$ordering['links']['distance'] = '<a class="' . $class . '" href="' . $url . '">' . __('Distance', 'W2DC-ESEARCH') . '</a>';
		$ordering['array']['distance'] = __('Distance', 'W2DC-ESEARCH');

		return $ordering;
	}
	
	public function base_url_args($args) {
		if (isset($_GET['search_category']) && $_GET['search_category'] && is_numeric($_GET['search_category']))
			$args['search_category'] = $_GET['search_category'];
		if (isset($_GET['where_radius']) && $_GET['where_radius'] && is_numeric($_GET['where_radius']))
			$args['where_radius'] = $_GET['where_radius'];

		//$this->load_search_fields();
		foreach ($this->search_fields AS $search_field)
			$search_field->getBaseUrlArgs($args);
		
		return $args;
	}
	
	public function render_radius_search($random_id) {
		if (get_option('w2dc_show_radius_search')) {
			if (isset($_GET['where_radius']) && is_numeric($_GET['where_radius']))
				$value = $_GET['where_radius'];
			else
				$value = get_option('w2dc_radius_search_default');
	
			w2dc_renderTemplate(array(W2DC_ESEARCH_TEMPLATES_PATH, 'radius_slider.tpl.php'), array('value' => $value, 'random_id' => $random_id));
		}
	}
	
	public function render_content_fields($random_id) {
		$is_advanced_search_panel = false;
		foreach ($this->search_fields AS $search_field)
			if ($search_field->content_field->advanced_search_form) {
				$is_advanced_search_panel = true;
				break;
		}
		
		$use_advanced = 0;
		if (isset($_GET['use_advanced']) && ($_GET['use_advanced'] == 1))
			$use_advanced = 1;

		w2dc_renderTemplate(array(W2DC_ESEARCH_TEMPLATES_PATH, 'search_fields/fields_search_form.tpl.php'), array('search_fields' => $this->search_fields, 'is_advanced_search_panel' => $is_advanced_search_panel, 'use_advanced' => $use_advanced, 'random_id' => $random_id));
	}

	public function render_advanced_link($random_id) {
		$is_advanced_search_panel = false;
		foreach ($this->search_fields AS $search_field)
			if ($search_field->content_field->advanced_search_form) {
				$is_advanced_search_panel = true;
				break;
		}

		w2dc_renderTemplate(array(W2DC_ESEARCH_TEMPLATES_PATH, 'search_fields/advanced_search_link.tpl.php'), array('is_advanced_search_panel' => $is_advanced_search_panel, 'random_id' => $random_id));
	}
	
	public function enqueue_scripts_styles() {
		global $w2dc_instance;

		wp_enqueue_script('jquery-ui-slider');
		wp_enqueue_script('jquery-touch-punch');
		
		wp_localize_script(
				'jquery-ui-slider',
				'slider_params',
				array(
						'min' => get_option('w2dc_radius_search_min'),
						'max' => get_option('w2dc_radius_search_max')
				)
		);
		
		if ($this->radius_value && $this->x_coord && $this->y_coord)
			wp_localize_script(
					'google_maps_view',
					'radius_params',
					array('radius_value' => $this->radius_value,
							'map_coord_1' => $this->x_coord,
							'map_coord_2' => $this->y_coord,
							'dimension' => get_option('w2dc_miles_kilometers_in_search')
					)
			);
	}
}

function w2dc_install_esearch() {
	update_option('w2dc_show_category_count_in_search', 1);
	update_option('w2dc_miles_kilometers_in_search', 'miles');
	update_option('w2dc_installed_esearch', true);
	
	w2dc_upgrade_esearch('1.5.4');
	w2dc_upgrade_esearch('1.5.7');
	w2dc_upgrade_esearch('1.5.8');
}

function w2dc_upgrade_esearch($new_version) {
	if ($new_version == '1.5.4') {
		update_option('w2dc_radius_search_min', 0);
		update_option('w2dc_radius_search_max', 10);
	}
	if ($new_version == '1.5.7') {
		update_option('w2dc_show_categories_search', 1);
		update_option('w2dc_show_radius_search', 1);
		update_option('w2dc_radius_search_default', 0);
	}
	if ($new_version == '1.5.8') {
		update_option('w2dc_orderby_distance', 1);
	}
}


global $w2dc_esearch_instance;

$w2dc_esearch_instance = new w2dc_esearch_plugin();
$w2dc_esearch_instance->init();

?>
