<?php 

class w2dc_content_field_number_search extends w2dc_content_field_search {
	public $mode = 'exact_number';
	public $min_max_options = array();
	public $min_max_value = array('min' => '', 'max' => '');

	public function searchConfigure() {
		global $wpdb, $w2dc_instance;

		if (w2dc_getValue($_POST, 'submit') && wp_verify_nonce($_POST['w2dc_configure_content_fields_nonce'], W2DC_PATH)) {
			$validation = new form_validation();
			$validation->set_rules('mode', __('Search mode', 'W2DC-ESEARCH'), 'required|alpha_dash');
			$validation->set_rules('min_max_options[]', __('Min-Max options', 'W2DC-ESEARCH'), 'numeric');
			if ($validation->run()) {
				$result = $validation->result_array();
				if ($wpdb->update($wpdb->content_fields, array('search_options' => serialize(array('mode' => $result['mode'], 'min_max_options' => $result['min_max_options[]']))), array('id' => $this->content_field->id), null, array('%d')))
					w2dc_addMessage(__('Search field configuration was updated successfully!', 'W2DC-ESEARCH'));
				
				$w2dc_instance->content_fields_manager->showContentFieldsTable();
			} else {
				$this->mode = $validation->result_array('mode');
				$this->min_max_options = $validation->result_array('min_max_options[]');
				w2dc_addMessage($validation->error_string(), 'error');

				w2dc_renderTemplate(array(W2DC_ESEARCH_TEMPLATES_PATH, 'search_fields/fields/number_price_configuration.tpl.php'), array('search_field' => $this));
			}
		} else
			w2dc_renderTemplate(array(W2DC_ESEARCH_TEMPLATES_PATH, 'search_fields/fields/number_price_configuration.tpl.php'), array('search_field' => $this));
	}
	
	public function buildSearchOptions() {
		if (isset($this->content_field->search_options['mode']))
			$this->mode = $this->content_field->search_options['mode'];
		if (isset($this->content_field->search_options['min_max_options']))
			$this->min_max_options = $this->content_field->search_options['min_max_options'];
	}
	
	public function renderSearch($random_id) {
		if ($this->mode == 'exact_number')
			w2dc_renderTemplate(array(W2DC_ESEARCH_TEMPLATES_PATH, 'search_fields/fields/number_input_exactnumber.tpl.php'), array('search_field' => $this, 'random_id' => $random_id));
		elseif ($this->mode == 'min_max')
			w2dc_renderTemplate(array(W2DC_ESEARCH_TEMPLATES_PATH, 'search_fields/fields/number_input_minmax.tpl.php'), array('search_field' => $this, 'random_id' => $random_id));
	}
	
	public function validateSearch(&$args, $defaults = array()) {
		if ($this->mode == 'exact_number') {
			$field_index = 'field_' . $this->content_field->slug;
			$value = w2dc_getValue($_GET, $field_index, w2dc_getValue($defaults, $field_index));
			if ($value && is_numeric($value)) {
				$this->value = $value;
				$args['meta_query']['relation'] = 'AND';
				$args['meta_query'][] = array(
						'key' => '_content_field_' . $this->content_field->id,
						'value' => $this->value,
						'type' => 'NUMERIC'
				);
			}
		} elseif ($this->mode == 'min_max') {
			$field_index = 'field_' . $this->content_field->slug . '_min';
			$value = w2dc_getValue($_GET, $field_index, w2dc_getValue($defaults, $field_index));
			if ($value && is_numeric($value)) {
				$this->min_max_value['min'] = $value;
				$args['meta_query']['relation'] = 'AND';
				$args['meta_query'][] = array(
						'key' => '_content_field_' . $this->content_field->id,
						'value' => $this->min_max_value['min'],
						'type' => 'numeric',
						'compare' => '>='
				);
			}
			$field_index = 'field_' . $this->content_field->slug . '_max';
			$value = w2dc_getValue($_GET, $field_index, w2dc_getValue($defaults, $field_index));
			if ($value && is_numeric($value)) {
				$this->min_max_value['max'] = $value;
				$args['meta_query']['relation'] = 'AND';
				$args['meta_query'][] = array(
						'key' => '_content_field_' . $this->content_field->id,
						'value' => $this->min_max_value['max'],
						'type' => 'numeric',
						'compare' => '<='
				);
			}
		}
	}
	
	public function getBaseUrlArgs(&$args) {
		if ($this->mode == 'exact_number') {
			parent::getBaseUrlArgs($args);
		} elseif ($this->mode == 'min_max') {
			$field_index = 'field_' . $this->content_field->slug . '_min';
			if (isset($_GET[$field_index]) && is_numeric($_GET[$field_index]))
				$args[$field_index] = $_GET[$field_index];
			
			$field_index = 'field_' . $this->content_field->slug . '_max';
			if (isset($_GET[$field_index]) && is_numeric($_GET[$field_index]))
				$args[$field_index] = $_GET[$field_index];
		}
	}
}
?>