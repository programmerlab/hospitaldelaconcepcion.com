<?php 

class w2dc_content_field_string_search extends w2dc_content_field_search {
	public $input_size = 25;

	public function searchConfigure() {
		global $wpdb, $w2dc_instance;

		if (w2dc_getValue($_POST, 'submit') && wp_verify_nonce($_POST['w2dc_configure_content_fields_nonce'], W2DC_PATH)) {
			$validation = new form_validation();
			$validation->set_rules('input_size', __('Search input HTML field size', 'W2DC-ESEARCH'), 'required|is_natural_no_zero');
			if ($validation->run()) {
				$result = $validation->result_array();
				if ($wpdb->update($wpdb->content_fields, array('search_options' => serialize(array('input_size' => $result['input_size']))), array('id' => $this->content_field->id), null, array('%d')))
					w2dc_addMessage(__('Search field configuration was updated successfully!', 'W2DC-ESEARCH'));
				
				$w2dc_instance->content_fields_manager->showContentFieldsTable();
			} else {
				$this->input_size = $validation->result_array('input_size');
				w2dc_addMessage($validation->error_string(), 'error');

				w2dc_renderTemplate(array(W2DC_ESEARCH_TEMPLATES_PATH, 'search_fields/fields/string_textarea_configuration.tpl.php'), array('search_field' => $this));
			}
		} else
			w2dc_renderTemplate(array(W2DC_ESEARCH_TEMPLATES_PATH, 'search_fields/fields/string_textarea_configuration.tpl.php'), array('search_field' => $this));
	}
	
	public function buildSearchOptions() {
		if (isset($this->content_field->search_options['input_size']))
			$this->input_size = $this->content_field->search_options['input_size'];
	}
	
	public function renderSearch($random_id) {
		w2dc_renderTemplate(array(W2DC_ESEARCH_TEMPLATES_PATH, 'search_fields/fields/string_textarea_input.tpl.php'), array('search_field' => $this, 'random_id' => $random_id));
	}
	
	public function validateSearch(&$args, $defaults = array()) {
		$field_index = 'field_' . $this->content_field->slug;

		$value = w2dc_getValue($_GET, $field_index, w2dc_getValue($defaults, $field_index));
		if ($value) {
			$this->value = $value;
			$args['meta_query']['relation'] = 'AND';
			$args['meta_query'][] = array(
					'key' => '_content_field_' . $this->content_field->id,
					'value' => $this->value,
					'compare' => 'LIKE'
			);
		}
	}
}
?>