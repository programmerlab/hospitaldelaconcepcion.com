<?php w2dc_renderTemplate('admin_header.tpl.php'); ?>

<?php screen_icon('edit-pages'); ?>
<h2>
	<?php _e('Configure text string/textarea search field', 'W2DC-ESEARCH'); ?>
</h2>

<form method="POST" action="">
	<?php wp_nonce_field(W2DC_PATH, 'w2dc_configure_content_fields_nonce');?>
	<table class="form-table">
		<tbody>
			<tr>
				<th scope="row">
					<label><?php _e('Search input HTML field size', 'W2DC-ESEARCH'); ?><span class="red_asterisk">*</span></label>
				</th>
				<td>
					<input
						name="input_size"
						type="text"
						size="2"
						value="<?php echo esc_attr($search_field->input_size); ?>" />
				</td>
			</tr>
		</tbody>
	</table>
	
	<?php submit_button(__('Save changes', 'W2DC-ESEARCH')); ?>
</form>

<?php w2dc_renderTemplate('admin_footer.tpl.php'); ?>