<script>
	jQuery(document).ready(function($) {
		$("#w2dc_field_input_<?php echo $search_field->content_field->id; ?>_min_<?php echo $random_id; ?>").datepicker({
			showOn: "both",
			buttonImageOnly: true,
			buttonImage: "<?php echo W2DC_RESOURCES_URL?>images/calendar.png",
			showButtonPanel: true,
			dateFormat: '<?php echo $dateformat; ?>',
			onSelect: function(dateText) {
				var sDate = $("#w2dc_field_input_<?php echo $search_field->content_field->id; ?>_min_<?php echo $random_id; ?>").datepicker("getDate");
				if (sDate) {
					sDate.setMinutes(sDate.getMinutes() - sDate.getTimezoneOffset());
					tmstmp_str = $.datepicker.formatDate('@', sDate)/1000;
				} else 
					tmstmp_str = 0;
				$("#w2dc_field_input_<?php echo $search_field->content_field->id; ?>_max_<?php echo $random_id; ?>").datepicker('option', 'minDate', sDate);

				$("input[name=field_<?php echo $search_field->content_field->slug; ?>_min]").val(tmstmp_str);
			}
		});
		<?php
		if ($lang_code = w2dc_getDatePickerLangCode(get_locale())): ?>
		$("#w2dc_field_input_<?php echo $search_field->content_field->id; ?>_min_<?php echo $random_id; ?>").datepicker($.datepicker.regional[ "<?php echo $lang_code; ?>" ]);
		<?php endif; ?>

		$("#w2dc_field_input_<?php echo $search_field->content_field->id; ?>_max_<?php echo $random_id; ?>").datepicker({
			showOn: "both",
			buttonImageOnly: true,
			buttonImage: "<?php echo W2DC_RESOURCES_URL?>images/calendar.png",
			showButtonPanel: true,
			dateFormat: '<?php echo $dateformat; ?>',
			onSelect: function(dateText) {
				var sDate = $("#w2dc_field_input_<?php echo $search_field->content_field->id; ?>_max_<?php echo $random_id; ?>").datepicker("getDate");
				if (sDate) {
					sDate.setMinutes(sDate.getMinutes() - sDate.getTimezoneOffset());
					tmstmp_str = $.datepicker.formatDate('@', sDate)/1000;
				} else 
					tmstmp_str = 0;
				$("#w2dc_field_input_<?php echo $search_field->content_field->id; ?>_min_<?php echo $random_id; ?>").datepicker('option', 'maxDate', sDate);

				$("input[name=field_<?php echo $search_field->content_field->slug; ?>_max]").val(tmstmp_str);
			}
		});
		<?php
		if ($lang_code = w2dc_getDatePickerLangCode(get_locale())): ?>
		$("#w2dc_field_input_<?php echo $search_field->content_field->id; ?>_max_<?php echo $random_id; ?>").datepicker($.datepicker.regional[ "<?php echo $lang_code; ?>" ]);
		<?php endif; ?>

		<?php if ($search_field->min_max_value['max']): ?>
		$("#w2dc_field_input_<?php echo $search_field->content_field->id; ?>_max_<?php echo $random_id; ?>").datepicker('setDate', $.datepicker.parseDate('dd/mm/yy', '<?php echo date('d/m/y', $search_field->min_max_value['max']); ?>'));
		$("#w2dc_field_input_<?php echo $search_field->content_field->id; ?>_min_<?php echo $random_id; ?>").datepicker('option', 'maxDate', $("#w2dc_field_input_<?php echo $search_field->content_field->id; ?>_max_<?php echo $random_id; ?>").datepicker('getDate'));
		<?php endif; ?>
		$("#reset_date_max_<?php echo $random_id; ?>").click(function() {
			$.datepicker._clearDate('#w2dc_field_input_<?php echo $search_field->content_field->id; ?>_max_<?php echo $random_id; ?>');
		})

		<?php if ($search_field->min_max_value['min']): ?>
		$("#w2dc_field_input_<?php echo $search_field->content_field->id; ?>_min_<?php echo $random_id; ?>").datepicker('setDate', $.datepicker.parseDate('dd/mm/yy', '<?php echo date('d/m/y', $search_field->min_max_value['min']); ?>'));
		$("#w2dc_field_input_<?php echo $search_field->content_field->id; ?>_max_<?php echo $random_id; ?>").datepicker('option', 'minDate', $("#w2dc_field_input_<?php echo $search_field->content_field->id; ?>_min_<?php echo $random_id; ?>").datepicker('getDate'));
		<?php endif; ?>
		$("#reset_date_min_<?php echo $random_id; ?>").click(function() {
			$.datepicker._clearDate('#w2dc_field_input_<?php echo $search_field->content_field->id; ?>_min_<?php echo $random_id; ?>');
		})
	});
</script>
<div class="w2dc_field w2dc_field_search_block_<?php echo $random_id; ?> w2dc_field_search_block_<?php echo $search_field->content_field->id; ?>_<?php echo $random_id; ?> row">
	<label><?php echo $search_field->content_field->name; ?></label>
	<div class="col-md-6 form-group">
		<input type="text" id="w2dc_field_input_<?php echo $search_field->content_field->id; ?>_min_<?php echo $random_id; ?>" placeholder="<?php _e('Start date', 'W2DC-ESEARCH'); ?>" class="w2dc_field_search_input_datetime form-control" size="9" />
		<input type="hidden" name="field_<?php echo $search_field->content_field->slug; ?>_min" value="<?php echo esc_attr($search_field->min_max_value['min']); ?>"/>
		<input type="button" class="btn btn-primary form-control" id="reset_date_min_<?php echo $random_id; ?>" value="<?php _e('reset date', 'W2DC-ESEARCH')?>" />
	</div>
	<div class="col-md-6 form-group">
		<input type="text" id="w2dc_field_input_<?php echo $search_field->content_field->id; ?>_max_<?php echo $random_id; ?>" placeholder="<?php _e('End date', 'W2DC-ESEARCH'); ?>" class="w2dc_field_search_input_datetime form-control" size="9" />
		<input type="hidden" name="field_<?php echo $search_field->content_field->slug; ?>_max" value="<?php echo esc_attr($search_field->min_max_value['max']); ?>"/>
		<input type="button" class="btn btn-primary form-control" id="reset_date_max_<?php echo $random_id; ?>" value="<?php _e('reset date', 'W2DC-ESEARCH')?>" />
	</div>
</div>