<?php if (count($search_field->content_field->selection_items)): ?>
<div class="w2dc_field w2dc_field_search_block_<?php echo $random_id; ?> w2dc_field_search_block_<?php echo $search_field->content_field->id; ?>_<?php echo $random_id; ?> row">
	<label><?php echo $search_field->content_field->name; ?></label>
	<?php 
	foreach ($search_field->content_field->selection_items AS $key=>$item): ?>
	<label class="col-md-4">
		<input type="checkbox" name="field_<?php echo $search_field->content_field->slug; ?>[]" class="w2dc_field_search_input_select" value="<?php echo esc_attr($key); ?>" <?php if (in_array($key, $search_field->value)) echo 'checked'; ?> />&nbsp;&nbsp;<?php echo $item; ?>
	</label>
	<?php endforeach; ?>
</div>
<?php endif; ?>