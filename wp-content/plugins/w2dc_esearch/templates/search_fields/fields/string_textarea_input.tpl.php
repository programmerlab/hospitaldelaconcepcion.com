<div class="w2dc_field w2dc_field_search_block_<?php echo $random_id; ?> w2dc_field_search_block_<?php echo $search_field->content_field->id; ?>_<?php echo $random_id; ?> row">
	<label><?php echo $search_field->content_field->name; ?></label>
	<div class="col-md-8 form-group">
		<input type="text" name="field_<?php echo $search_field->content_field->slug; ?>" class="w2dc_field_search_input_string form-control" value="<?php echo esc_attr($search_field->value); ?>" size="<?php echo $search_field->input_size; ?>" />
	</div>
</div>