<?php w2dc_renderTemplate('admin_header.tpl.php'); ?>

<?php screen_icon('edit-pages'); ?>
<h2>
	<?php _e('Configure number/price search field', 'W2DC-ESEARCH'); ?>
</h2>

<script language="JavaScript" type="text/javascript">
	jQuery(document).ready(function($) {
		$("#add_selection_item").click(function() {
			$("#selection_items_wrapper").append('<div class="selection_item"><input name="min_max_options[]" type="text" size="9" value="" /><img class="delete_selection_item" src="<?php echo W2DC_RESOURCES_URL . 'images/delete.png'?>" title="<?php _e('Remove option', 'W2DC-ESEARCH')?>" /></div>');
		});
		jQuery(document).on("click", ".delete_selection_item", function() {
			$(this).parent().remove();
		});
	});
</script>

<form method="POST" action="">
	<?php wp_nonce_field(W2DC_PATH, 'w2dc_configure_content_fields_nonce');?>
	<table class="form-table">
		<tbody>
			<tr>
				<th scope="row">
					<label><?php _e('Search mode', 'W2DC-ESEARCH'); ?><span class="red_asterisk">*</span></label>
				</th>
				<td>
					<label>
						<input
							name="mode"
							type="radio"
							value="exact_number"
							<?php checked($search_field->mode, 'exact_number'); ?> />
						<?php _e('Enter exact number for search', 'W2DC-ESEARCH'); ?>
					</label>
					<br />
					<label>
						<input
							name="mode"
							type="radio"
							value="min_max"
							<?php checked($search_field->mode, 'min_max'); ?> />
						<?php _e('Select Min-Max options for search', 'W2DC-ESEARCH'); ?>
					</label>
				</td>
			</tr>
			<tr>
				<th scope="row">
					<label><?php _e('Min-Max options:', 'W2DC-ESEARCH'); ?>
				</th>
				<td>
					<div id="selection_items_wrapper">
						<?php if (count($search_field->min_max_options)): ?>
						<?php foreach ($search_field->min_max_options AS $item): ?>
						<div class="selection_item">
							<input
								name="min_max_options[]"
								type="text"
								size="9"
								value="<?php echo $item; ?>" />
							<img class="delete_selection_item" src="<?php echo W2DC_RESOURCES_URL . 'images/delete.png'?>" title="<?php _e('Remove min-max option', 'W2DC-ESEARCH')?>" />
						</div>
						<?php endforeach; ?>
						<?php else: ?>
						<div class="selection_item">
							<input
								name="min_max_options[]"
								type="text"
								size="9"
								value="" />
							<img class="delete_selection_item" src="<?php echo W2DC_RESOURCES_URL . 'images/delete.png'?>" title="<?php _e('Remove min-max option', 'W2DC-ESEARCH')?>" />
						</div>
						<?php endif; ?>
					</div>
				</td>
			</tr>
		</tbody>
	</table>
	<input type="button" id="add_selection_item" class="button button-primary" value="<?php _e('Add min-max option', 'W2DC-ESEARCH'); ?>" />
	
	<?php submit_button(__('Save changes', 'W2DC-ESEARCH')); ?>
</form>

<?php w2dc_renderTemplate('admin_footer.tpl.php'); ?>