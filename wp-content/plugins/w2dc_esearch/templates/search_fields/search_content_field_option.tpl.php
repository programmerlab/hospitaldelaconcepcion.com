<script language="JavaScript" type="text/javascript">
	jQuery(document).ready(function($) {
		<?php if (!$content_field->is_core_field): ?>
		$("#type").change(function() {
			if (
				<?php 
				foreach ($content_fields->fields_types_names AS $content_field_type=>$content_field_name){
					$field_class_name = 'w2dc_content_field_' . $content_field_type;
					if (class_exists($field_class_name)) {
						$_content_field = new $field_class_name;
						if (!$_content_field->canBeSearched()) {
				?>
				$(this).val() == '<?echo $content_field_type; ?>' ||
				<?php
						}
					}
				} ?>
			$(this).val() === '')
				$(".can_be_searched_block").hide();
			else
				$(".can_be_searched_block").show();
		});
		$("#on_search_form").click( function() {
			if ($(this).is(':checked'))
				$('input[name="advanced_search_form"]').removeAttr('disabled');
			else 
				$('input[name="advanced_search_form"]').attr('disabled', true);
		});
		<?php endif; ?>
	});
</script>
		
			<tr class="can_be_searched_block" <?php if (!$content_field->canBeSearched()): ?>style="display: none;"<?php endif; ?>>
				<th scope="row">
					<label><?php _e('Search by this field?', 'W2DC-ESEARCH'); ?></label>
				</th>
				<td>
					<input
						id="on_search_form"
						name="on_search_form"
						type="checkbox"
						value="1"
						<?php checked($content_field->on_search_form); ?> />
				</td>
			</tr>
			<tr class="can_be_searched_block" <?php if (!$content_field->canBeSearched()): ?>style="display: none;"<?php endif; ?>>
				<th scope="row">
					<label><?php _e('On advanced search panel?', 'W2DC-ESEARCH'); ?></label>
				</th>
				<td>
					<input
						name="advanced_search_form"
						type="checkbox"
						value="1"
						<?php checked($content_field->advanced_search_form); ?>
						<?php disabled(!$content_field->on_search_form)?> />
				</td>
			</tr>