<script>
	jQuery(document).ready(function($) {
		$('#where_radius_slider_<?php echo $random_id; ?>').slider({
			min: parseInt(slider_params.min),
			max: parseInt(slider_params.max),
			range: "min",
			value: $("#where_radius_<?php echo $random_id; ?>").val(),
			slide: function(event, ui) {
				$("#where_radius_label_<?php echo $random_id; ?>").html(ui.value);
				$("#where_radius_<?php echo $random_id; ?>").val(ui.value);
			}
		});
	});
</script>

<div class="col-md-12">
	<div style="margin-bottom: 5px; font-size: 12px;">
		<?php _e('Search in radius', 'W2DC-ESEARCH'); ?>
		<strong id="where_radius_label_<?php echo $random_id; ?>"><?php echo $value; ?></strong>
		<?php if (get_option('w2dc_miles_kilometers_in_search') == 'miles') _e('miles', 'W2DC-ESEARCH'); else _e('kilometers', 'W2DC-ESEARCH'); ?>
	</div>
	<div id="where_radius_slider_<?php echo $random_id; ?>"></div>
	<input type="hidden" name="where_radius" id="where_radius_<?php echo $random_id; ?>" value="<?php echo $value; ?>" />
</div>