<?php 

class w2dc_dashboard_controller extends w2dc_frontend_controller {

	public function __construct() {
		global $w2dc_instance;

		if (!is_user_logged_in())
			$this->template = array(W2DC_FSUBMIT_TEMPLATES_PATH, 'login_form.tpl.php');
		else {
			if (isset($_POST['referer']))
				$this->referer = $_POST['referer'];
			else
				$this->referer = wp_get_referer();
			if (isset($_POST['cancel']) && isset($_POST['referer']))
				wp_redirect($_POST['referer']);

			if (!$w2dc_instance->action) {
				if (get_query_var('page'))
					$paged = get_query_var('page');
				elseif (get_query_var('paged'))
					$paged = get_query_var('paged');
				else
					$paged = 1;
			} else
				$paged = -1;
			
			$args = array(
					'post_type' => W2DC_POST_TYPE,
					'author' => get_current_user_id(),
					'paged' => $paged,
					'posts_per_page' => 20,
					'post_status' => 'any'
			);
			$this->query = new WP_Query($args);
			wp_reset_postdata();
			
			$this->listings_count = $this->query->found_posts;
			
			$this->active_tab = 'listings';

			if (!$w2dc_instance->action) {
				$this->processQuery(false);

				$this->template = array(W2DC_FSUBMIT_TEMPLATES_PATH, 'dashboard.tpl.php');
				$this->subtemplate = array(W2DC_FSUBMIT_TEMPLATES_PATH, 'listings.tpl.php');
			} elseif ($w2dc_instance->action == 'edit_listing' && isset($_GET['listing_id']) && is_numeric($_GET['listing_id'])) {
				$listing_id = w2dc_getValue($_GET, 'listing_id');
				if ($listing_id && current_user_can('edit_post', $listing_id)) {
					$listing = $w2dc_instance->listings_manager->loadListing($listing_id);
					
					if (isset($_POST['submit'])) {
						$errors = array();

						if (!isset($_POST['post_title']) || !$_POST['post_title'] || $_POST['post_title'] == __('Auto Draft')) {
							$errors[] = __('Listing title field required', 'W2DC-FSUBMIT');
							$post_title = __('Auto Draft');
						} else
							$post_title = $_POST['post_title'];
						
						if ($listing->level->categories_number > 0 || $listing->level->unlimited_categories) {
							if ($post_categories_ids = $w2dc_instance->categories_manager->validateCategories($listing->level, $_POST, $errors)) {
								foreach ($post_categories_ids AS $key=>$id)
									$post_categories_ids[$key] = intval($id);
							}
							wp_set_object_terms($listing->post->ID, $post_categories_ids, W2DC_CATEGORIES_TAX);
						}
						
						$w2dc_instance->content_fields->saveValues($listing->post->ID, $post_categories_ids, $errors, $_POST);
						
						if ($listing->level->locations_number) {
							if ($validation_results = $w2dc_instance->locations_manager->validateLocations($errors)) {
								$w2dc_instance->locations_manager->saveLocations($listing->level, $listing->post->ID, $validation_results);
							}
						}
						
						if ($listing->level->images_number || $listing->level->videos_number) {
							if ($validation_results = $w2dc_instance->media_manager->validateAttachments($listing->level, $errors))
								$w2dc_instance->media_manager->saveAttachments($listing->level, $listing->post->ID, $validation_results);
						}

						$postarr = array(
								'ID' => $listing_id,
								'post_title' => $post_title,
								'post_content' => $_POST['post_content'],
								'post_excerpt' => $_POST['post_excerpt']
						);
						$result = wp_update_post($postarr, true);
						if (is_wp_error($result))
							$errors[] = $result->get_error_message();

						if ($errors) {
							foreach ($errors AS $error)
								w2dc_addMessage($error, 'error');
						} else {
							if (!$listing->level->eternal_active_period) {
								if (get_option('w2dc_change_expiration_date'))
									$w2dc_instance->listings_manager->changeExpirationDate();
								else {
									$expiration_date = w2dc_sumDates(time(), $listing->level->active_days, $listing->level->active_months, $listing->level->active_years);
									add_post_meta($listing->post->ID, '_expiration_date', $expiration_date);
								}
							}
							
							if (get_option('w2dc_fsubmit_edit_status') == 1) {
								$post_status = 'pending';
								$message = __('Listing was saved successfully! Now it\'s awaiting moderators approval.', 'W2DC-FSUBMIT');
							} elseif (get_option('w2dc_fsubmit_edit_status') == 2) {
								$post_status = 'draft';
								$message = __('Listing was saved successfully as draft! Contact site manager, please.', 'W2DC-FSUBMIT');
							} elseif (get_option('w2dc_fsubmit_edit_status') == 3) {
								$post_status = 'publish';
								$message = __('Listing was saved successfully!', 'W2DC-FSUBMIT');
							}
							$postarr = array(
									'ID' => $listing_id,
									'post_status' => $post_status
							);
							$result = wp_update_post($postarr, true);
							
							w2dc_addMessage($message);
							
							if (!$this->referer)
								$this->referer = w2dc_dashboardUrl();
							wp_redirect($this->referer);
							exit;
						}
					}

					$this->template = array(W2DC_FSUBMIT_TEMPLATES_PATH, 'dashboard.tpl.php');
					$this->subtemplate = array(W2DC_FSUBMIT_TEMPLATES_PATH, 'edit_listing.tpl.php');
					if ($listing->level->categories_number > 0 || $listing->level->unlimited_categories) {
						add_action('wp_enqueue_scripts', array($w2dc_instance->categories_manager, 'admin_enqueue_scripts_styles'));
					}
						
					if ($listing->level->locations_number > 0 && $listing->level->google_map) {
						add_action('wp_print_scripts', array($w2dc_instance->locations_manager, 'remove_google_maps_view'), 100);
						add_action('wp_enqueue_scripts', array($w2dc_instance->locations_manager, 'admin_enqueue_scripts_styles'));
					}
						
					if ($listing->level->images_number > 0 || $listing->level->videos_number > 0)
						add_action('wp_enqueue_scripts', array($w2dc_instance->media_manager, 'admin_enqueue_scripts_styles'));
				}
			} elseif ($w2dc_instance->action == 'raiseup_listing' && isset($_GET['listing_id']) && is_numeric($_GET['listing_id'])) {
				$listing_id = w2dc_getValue($_GET, 'listing_id');
				if ($listing_id && current_user_can('edit_post', $listing_id) && ($listing = $w2dc_instance->listings_manager->loadListing($listing_id)) && $listing->status == 'active') {
					$this->action = 'show';
					if (isset($_GET['raiseup_action']) && $_GET['raiseup_action'] == 'raiseup') {
						if ($listing->processRaiseUp())
							w2dc_addMessage(__('Listing was raised up successfully!', 'W2DC-FSUBMIT'));
						/* else
							w2dc_addMessage(__('An error has occurred and listing was not raised up', 'W2DC-FSUBMIT'), 'error'); */
						$this->action = $_GET['raiseup_action'];
						$this->referer = $_GET['referer'];
					}
					$this->template = array(W2DC_FSUBMIT_TEMPLATES_PATH, 'dashboard.tpl.php');
					$this->subtemplate = array(W2DC_FSUBMIT_TEMPLATES_PATH, 'raise_up.tpl.php');
				} else
					wp_die('You are not able to manage this listing', 'W2DC-FSUBMIT');
			} elseif ($w2dc_instance->action == 'renew_listing' && isset($_GET['listing_id']) && is_numeric($_GET['listing_id'])) {
				$listing_id = w2dc_getValue($_GET, 'listing_id');
				if ($listing_id && current_user_can('edit_post', $listing_id) && ($listing = $w2dc_instance->listings_manager->loadListing($listing_id))) {
					$this->action = 'show';
					if (isset($_GET['renew_action']) && $_GET['renew_action'] == 'renew') {
						if ($listing->processActivate())
							w2dc_addMessage(__('Listing was renewed successfully!', 'W2DC-FSUBMIT'));
						/* else
							w2dc_addMessage(__('An error has occurred and listing was not renewed', 'W2DC-FSUBMIT'), 'error'); */
						$this->action = $_GET['renew_action'];
						$this->referer = $_GET['referer'];
					}
					$this->template = array(W2DC_FSUBMIT_TEMPLATES_PATH, 'dashboard.tpl.php');
					$this->subtemplate = array(W2DC_FSUBMIT_TEMPLATES_PATH, 'renew.tpl.php');
				} else
					wp_die('You are not able to manage this listing', 'W2DC-FSUBMIT');
			} elseif ($w2dc_instance->action == 'delete_listing' && isset($_GET['listing_id']) && is_numeric($_GET['listing_id'])) {
				$listing_id = w2dc_getValue($_GET, 'listing_id');
				if ($listing_id && current_user_can('edit_post', $listing_id) && ($listing = $w2dc_instance->listings_manager->loadListing($listing_id))) {
					if (isset($_GET['delete_action']) && $_GET['delete_action'] == 'delete') {
						if (wp_delete_post($listing_id, true) !== FALSE) {
							$w2dc_instance->listings_manager->delete_listing_data($listing_id);

							w2dc_addMessage(__('Listing was deleted successfully!', 'W2DC-FSUBMIT'));
							wp_redirect(w2dc_dashboardUrl());
							exit;
						} else 
							w2dc_addMessage(__('An error has occurred and listing was not deleted', 'W2DC-FSUBMIT'), 'error');
					}
					$this->template = array(W2DC_FSUBMIT_TEMPLATES_PATH, 'dashboard.tpl.php');
					$this->subtemplate = array(W2DC_FSUBMIT_TEMPLATES_PATH, 'delete.tpl.php');
				} else
					wp_die('You are not able to manage this listing', 'W2DC-FSUBMIT');
			} elseif ($w2dc_instance->action == 'upgrade_listing' && isset($_GET['listing_id']) && is_numeric($_GET['listing_id'])) {
				$listing_id = w2dc_getValue($_GET, 'listing_id');
				if ($listing_id && current_user_can('edit_post', $listing_id) && ($listing = $w2dc_instance->listings_manager->loadListing($listing_id)) && $listing->status == 'active') {
					$this->action = 'show';
					if (isset($_GET['upgrade_action']) && $_GET['upgrade_action'] == 'upgrade') {
						$form_validation = new form_validation();
						$form_validation->set_rules('new_level_id', __('New level ID', 'W2DC-FSUBMIT'), 'required|integer');

						if ($form_validation->run()) {
							if ($listing->changeLevel($form_validation->result_array('new_level_id')))
								w2dc_addMessage(__('Listing level was changed successfully!', 'W2DC-FSUBMIT'));
							$this->action = $_GET['upgrade_action'];
						} else
							w2dc_addMessage(__('New level must be selected!', 'W2DC-FSUBMIT'), 'error');

						$this->referer = $_GET['referer'];
					}
					$this->template = array(W2DC_FSUBMIT_TEMPLATES_PATH, 'dashboard.tpl.php');
					$this->subtemplate = array(W2DC_FSUBMIT_TEMPLATES_PATH, 'upgrade.tpl.php');
				} else
					wp_die('You are not able to manage this listing', 'W2DC-FSUBMIT');
			} elseif ($w2dc_instance->action == 'profile') {
				if (get_option('w2dc_allow_edit_profile')) {
					$user_id = get_current_user_id();
					$current_user = wp_get_current_user();
	
					include_once ABSPATH . 'wp-admin/includes/user.php';
	
					if (isset($_POST['user_id'])) {
						if ($_POST['user_id'] == $user_id) {
							global $wpdb;
		
							if (!is_multisite()) {
								$errors = edit_user($user_id);
							} else {
								$user = get_userdata($user_id);
							
								// Update the email address in signups, if present.
								if ($user->user_login && isset($_POST['email']) && is_email($_POST['email']) && $wpdb->get_var($wpdb->prepare("SELECT user_login FROM {$wpdb->signups} WHERE user_login = %s", $user->user_login)))
									$wpdb->query($wpdb->prepare("UPDATE {$wpdb->signups} SET user_email = %s WHERE user_login = %s", $_POST['email'], $user->user_login));
							
								// We must delete the user from the current blog if WP added them after editing.
								$delete_role = false;
								$blog_prefix = $wpdb->get_blog_prefix();
								if ($user_id != $current_user->ID) {
									$cap = $wpdb->get_var("SELECT meta_value FROM {$wpdb->usermeta} WHERE user_id = '{$user_id}' AND meta_key = '{$blog_prefix}capabilities' AND meta_value = 'a:0:{}'");
									if (!is_network_admin() && null == $cap && $_POST['role'] == '') {
										$_POST['role'] = 'contributor';
										$delete_role = true;
									}
								}
								if (!isset($errors) || (isset($errors) && is_object($errors) && false == $errors->get_error_codes()))
									$errors = edit_user($user_id);
								if ( $delete_role ) // stops users being added to current blog when they are edited
									delete_user_meta($user_id, $blog_prefix . 'capabilities');
							}
							
							if (!is_wp_error($errors)) {
								w2dc_addMessage(__('Your profile was successfully updated!', 'W2DC-FSUBMIT'));
								wp_redirect($this->referer);
								exit;
							}
						} else
							wp_die('You are not able to manage profile', 'W2DC-FSUBMIT');
					}
	
					$this->user = get_user_to_edit($user_id);
	
					wp_enqueue_script('password-strength-meter');
					wp_enqueue_script('user-profile');
					
					$this->template = array(W2DC_FSUBMIT_TEMPLATES_PATH, 'dashboard.tpl.php');
					$this->subtemplate = array(W2DC_FSUBMIT_TEMPLATES_PATH, 'profile.tpl.php');
					$this->active_tab = 'profile';
				} else
					wp_die('You are not able to manage profile', 'W2DC-FSUBMIT');
			// adapted for WPML
			} elseif (function_exists('icl_object_id') && get_option('w2dc_enable_frontend_translations') && $w2dc_instance->action == 'add_translation' && isset($_GET['trid']) && isset($_GET['to_lang'])) {
				global $sitepress, $ICL_Pro_Translation;
				
				$master_post_id = $_GET['trid'];
				$lang_code = $_GET['to_lang'];

				require_once( ICL_PLUGIN_PATH . '/lib/icl_api.php' );
				require_once( ICL_PLUGIN_PATH . '/lib/xml2array.php' );
				require_once( ICL_PLUGIN_PATH . '/inc/translation-management/pro-translation.class.php' );
				require_once( ICL_PLUGIN_PATH . '/inc/translation-management/translation-management.class.php' );

				$iclTranslationManagement = include_once W2DC_PATH . 'wpml-workaround.php';

				$ICL_Pro_Translation      = new ICL_Pro_Translation();

				$post_type = get_post_type($master_post_id);
				if ($sitepress->is_translated_post_type($post_type)) {
					$sitepress->set_setting('sync_post_taxonomies', false);
					if (
					icl_object_id($w2dc_instance->dashboard_page_id, 'page', false, $lang_code) &&
					($new_listing_id = $iclTranslationManagement->make_duplicate($master_post_id, $lang_code)) &&
					($iclTranslationManagement->duplicate_taxonomies($master_post_id, $lang_code)) &&
					($iclTranslationManagement->duplicate_custom_fields($master_post_id, $lang_code))) {
						$iclTranslationManagement->reset_duplicate_flag($new_listing_id);
						w2dc_addMessage(__('Translation was successfully created!', 'W2DC-FSUBMIT'));
						wp_redirect(add_query_arg(array('w2dc_action' => 'edit_listing', 'listing_id' => $new_listing_id), get_permalink(icl_object_id($w2dc_instance->dashboard_page_id, 'page', true, $lang_code))));
					} else {
						w2dc_addMessage(__('Translation was not created!', 'W2DC-FSUBMIT'), 'error');
						wp_redirect(w2dc_dashboardUrl());
					}
					exit;
				}
			}
		}

		apply_filters('w2dc_frontend_controller_construct', $this);
	}

	public function display() {
		$output =  w2dc_renderTemplate($this->template, array('frontend_controller' => $this), true);
		wp_reset_postdata();

		return $output;
	}
}

?>