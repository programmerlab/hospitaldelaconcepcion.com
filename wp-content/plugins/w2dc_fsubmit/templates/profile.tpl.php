	<form action="" method="POST" role="form">
		<input type="hidden" name="referer" value="<?php echo $frontend_controller->referer; ?>" />
		<input type="hidden" name="rich_editing" value="<?php echo ($frontend_controller->user->rich_editing) ? 1 : 0; ?>" />
		<input type="hidden" name="admin_color" value="<?php echo ($frontend_controller->user->admin_color) ? $frontend_controller->user->admin_color : 'fresh'; ?>" />
		<input type="hidden" name="admin_bar_front" value="<?php echo ($frontend_controller->user->show_admin_bar_front) ? 1 : 0; ?>" />

		<div class="form-group">
			<p>
				<label for="user_login"><?php _e('Username', 'W2DC-FSUBMIT'); ?></label>
				<input type="text" name="user_login" class="form-control" value="<?php echo esc_attr($frontend_controller->user->user_login); ?>" disabled="disabled" /> <span class="description"><?php _e('Usernames cannot be changed.', 'W2DC-FSUBMIT'); ?></span>
			</p>
			<p>
				<label for="first_name"><?php _e('First Name', 'W2DC-FSUBMIT') ?></label>
				<input type="text" name="first_name" class="form-control" value="<?php echo esc_attr($frontend_controller->user->first_name); ?>" />
			</p>
			<p>
				<label for="last_name"><?php _e('Last Name', 'W2DC-FSUBMIT') ?></label>
				<input type="text" name="last_name" class="form-control" value="<?php echo esc_attr($frontend_controller->user->last_name); ?>" />
			</p>
			<p>
				<label for="nickname"><?php _e('Nickname', 'W2DC-FSUBMIT') ?> <span class="description"><?php _e('(required)', 'W2DC-FSUBMIT'); ?></span></label>
				<input type="text" name="nickname" class="form-control" value="<?php echo esc_attr($frontend_controller->user->nickname); ?>" />
			</p>
			<p>
				<label for="display_name"><?php _e('Display to Public as', 'W2DC-FSUBMIT') ?></label>
				<select name="display_name" class="form-control">
				<?php
					$public_display = array();
					$public_display['display_username']  = $frontend_controller->user->user_login;
					$public_display['display_nickname']  = $frontend_controller->user->nickname;
					if (!empty($profileuser->first_name))
						$public_display['display_firstname'] = $frontend_controller->user->first_name;

					if (!empty($profileuser->last_name))
						$public_display['display_lastname'] = $frontend_controller->user->last_name;

					if (!empty($profileuser->first_name) && !empty($profileuser->last_name)) {
						$public_display['display_firstlast'] = $frontend_controller->user->first_name . ' ' . $frontend_controller->user->last_name;
						$public_display['display_lastfirst'] = $frontend_controller->user->last_name . ' ' . $frontend_controller->user->first_name;
					}

					if (!in_array($frontend_controller->user->display_name, $public_display)) // Only add this if it isn't duplicated elsewhere
						$public_display = array('display_displayname' => $frontend_controller->user->display_name) + $public_display;

					$public_display = array_map('trim', $public_display);
					$public_display = array_unique($public_display);
					foreach ($public_display as $id => $item) {
				?>
					<option id="<?php echo $id; ?>" value="<?php echo esc_attr($item); ?>"<?php selected($frontend_controller->user->display_name, $item); ?>><?php echo $item; ?></option>
				<?php
					}
				?>
				</select>
			</p>
			<p>
				<label for="email"><?php _e('E-mail', 'W2DC-FSUBMIT'); ?> <span class="description"><?php _e('(required)', 'W2DC-FSUBMIT'); ?></span></label>
				<input type="text" name="email" class="form-control" value="<?php echo esc_attr($frontend_controller->user->user_email) ?>" />
			</p>
			<p>
				<label for="pass1"><?php _e('New Password', 'W2DC-FSUBMIT'); ?></label>
				<input type="password" name="pass1" id="pass1" class="form-control" size="16" value="" autocomplete="off" />
				<span class="description"><?php _e('If you would like to change the password type a new one. Otherwise leave this blank.', 'W2DC-FSUBMIT'); ?></span>
			</p>
			<p>
				<label for="pass2"><?php _e('Repeat New Password', 'W2DC-FSUBMIT'); ?></label>
				<input name="pass2" type="password" id="pass2" class="form-control" size="16" value="" autocomplete="off" />
				<span class="description" for="pass2"><?php _e('Type your new password again.', 'W2DC-FSUBMIT'); ?></span>
				<div id="pass-strength-result"><?php _e('Strength indicator', 'W2DC-FSUBMIT'); ?></div>
				<span class="description indicator-hint"><?php _e('Hint: The password should be at least seven characters long. To make it stronger, use upper and lower case letters, numbers and symbols like ! " ? $ % ^ &amp; ).', 'W2DC-FSUBMIT'); ?></span>
			</p>
		</div>

		<input type="hidden" name="user_id" id="user_id" value="<?php echo esc_attr($frontend_controller->user->ID); ?>" />
		<?php require_once(ABSPATH . 'wp-admin/includes/template.php'); ?>
		<?php submit_button(__('Save changes', 'W2DC-FSUBMIT'), 'btn btn-primary'); ?>
	</form>