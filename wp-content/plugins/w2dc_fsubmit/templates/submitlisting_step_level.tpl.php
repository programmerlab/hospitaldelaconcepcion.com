<div class="w2dc_content">
	<?php if ((count($w2dc_instance->levels->levels_array) > 1) || (get_option('w2dc_fsubmit_login_mode') == 1 && !is_user_logged_in())): ?>
	<div class="submit_section_adv">
		<?php $step = 1; ?>

		<?php if (count($w2dc_instance->levels->levels_array) > 1): ?>
		<div class="adv_step adv_step_active">
			<div class="adv_circle adv_circle_active"><?php _e('Step', 'W2DC-FSUBMIT'); ?> <?php echo $step++; ?></div>
			<?php _e('Choose level', 'W2DC-FSUBMIT'); ?>
		</div>
		<div class="adv_line adv_line_active"></div>
		<?php endif; ?>

		<?php if (get_option('w2dc_fsubmit_login_mode') == 1 && !is_user_logged_in()): ?>
		<div class="adv_step">
			<div class="adv_circle"><?php _e('Step', 'W2DC-FSUBMIT'); ?> <?php echo $step++; ?></div>
			<?php _e('Login', 'W2DC-FSUBMIT'); ?>
		</div>
		<div class="adv_line"></div>
		<?php endif; ?>
		
		<div class="adv_step">
			<div class="adv_circle"><?php _e('Step', 'W2DC-FSUBMIT'); ?> <?php echo $step++; ?></div>
			<?php _e('Create listing', 'W2DC-FSUBMIT'); ?>
		</div>
		
		<?php $step = apply_filters('w2dc_create_listings_steps_html', $step); ?>
		
		<div class="clear_float"></div>
	</div>
	<?php endif; ?>
	
	<div class="submit_section_adv">
		<?php $max_columns_in_row = 4;?>
		<?php $levels_counter = count($w2dc_instance->levels->levels_array); ?>
		<?php if ($levels_counter > $max_columns_in_row) $levels_counter = $max_columns_in_row; ?>
		<?php $cols_width = floor(12/$levels_counter); ?>

		<?php $counter = 0; ?>
		<?php $tcounter = 0; ?>
		<?php foreach ($w2dc_instance->levels->levels_array AS $level): ?>
		<?php $tcounter++; ?>
		<?php if ($counter == 0): ?>
		<div class="row">
		<?php endif; ?>

			<div class="col-sm-<?php echo $cols_width; ?>">
				<div class="panel panel-default text-center">
					<div class="panel-heading <?php if ($level->featured): ?>w2dc_featured<?php endif; ?>">
						<h3>
							<?php echo $level->name; ?>
						</h3>
						<?php if ($level->description): ?><div class="hint_icon"></div><div class="hint_msg"><?php echo nl2br($level->description); ?></div><?php endif; ?>
					</div>
					<ul class="list-group">
						<?php do_action('w2dc_submitlisting_levels_rows', $level, '<li class="list-group-item">', '</li>'); ?>
						<li class="list-group-item">
							<?php echo $level->getActivePeriodString(); ?>
						</li>
						<li class="list-group-item">
							<?php _e('Sticky', 'W2DC-FSUBMIT'); ?>&nbsp;
							<?php if ($level->sticky): ?>
							<img src="<?php echo W2DC_RESOURCES_URL; ?>images/accept.png" />
							<?php else: ?>
							<img src="<?php echo W2DC_RESOURCES_URL; ?>images/delete.png" />
							<?php endif; ?>
						</li>
						<li class="list-group-item">
							<?php _e('Featured', 'W2DC-FSUBMIT'); ?>&nbsp;
							<?php if ($level->featured): ?>
							<img src="<?php echo W2DC_RESOURCES_URL; ?>images/accept.png" />
							<?php else: ?>
							<img src="<?php echo W2DC_RESOURCES_URL; ?>images/delete.png" />
							<?php endif; ?>
						</li>
						<li class="list-group-item">
							<?php _e('Categories number', 'W2DC-FSUBMIT'); ?>:
							<?php if (!$level->unlimited_categories) echo $level->categories_number; else _e('Unlimited', 'W2DC-FSUBMIT'); ?>
						</li>
						<li class="list-group-item">
							<?php _e('Google map', 'W2DC-FSUBMIT'); ?>&nbsp;
							<?php if ($level->google_map): ?>
							<img src="<?php echo W2DC_RESOURCES_URL; ?>images/accept.png" />
							<?php else: ?>
							<img src="<?php echo W2DC_RESOURCES_URL; ?>images/delete.png" />
							<?php endif; ?>
						</li>
						<li class="list-group-item">
							<?php _e('Images number', 'W2DC-FSUBMIT'); ?>:
							<?php echo $level->images_number; ?>
						</li>
						<li class="list-group-item">
							<?php _e('Videos number', 'W2DC-FSUBMIT'); ?>:
							<?php echo $level->videos_number; ?>
						</li>
						<li class="list-group-item">
							<a href="<?php echo w2dc_submitUrl(array('level' => $level->id)); ?>" class="btn <?php echo ($level->featured)? 'btn-info' : 'btn-primary'; ?>"><?php _e('Submit', 'W2DC-FSUBMIT'); ?></a>
						</li>
					</ul>
				</div>          
			</div>

		<?php $counter++; ?>
		<?php if ($counter == $max_columns_in_row || $tcounter == $levels_counter): ?>
		</div>
		<?php endif; ?>
		<?php if ($counter == $max_columns_in_row) $counter = 0; ?>
		<?php endforeach; ?>
	</div>
</div>